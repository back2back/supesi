<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class GepointsToStreetAddress extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('trips');
        $table->addColumn('readable_start', 'string', [
            'default' => 'not set',
            'null' => false,
            'limit' => 255,
        ]);
        $table->addColumn('readable_end', 'string', [
            'default' => 'not set',
            'null' => false,
            'limit' => 255,
        ]);

        $table->update();
    }
}
