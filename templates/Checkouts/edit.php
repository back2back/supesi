<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Checkout $checkout
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $checkout->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $checkout->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Checkouts'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="checkouts form content">
            <?= $this->Form->create($checkout) ?>
            <fieldset>
                <legend><?= __('Edit Checkout') ?></legend>
                <?php
                    echo $this->Form->control('session_id');
                    echo $this->Form->control('user_id', ['options' => $users]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
