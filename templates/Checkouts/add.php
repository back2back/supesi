<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Checkout $checkout
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Checkouts'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="checkouts form content">
            <?= $this->Form->create($checkout) ?>
            <fieldset>
                <legend><?= __('Add Checkout') ?></legend>
                <?php
                    echo $this->Form->control('email');
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
