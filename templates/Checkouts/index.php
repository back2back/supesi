<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Checkout[]|\Cake\Collection\CollectionInterface $checkouts
 */
?>
<div class="checkouts index content">
    <?= $this->Html->link(__('New Checkout'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Checkouts') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('session_id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($checkouts as $checkout): ?>
                <tr>
                    <td><?= $this->Number->format($checkout->id) ?></td>
                    <td><?= h($checkout->session_id) ?></td>
                    <td><?= $checkout->has('user') ? $this->Html->link($checkout->user->id, ['controller' => 'Users', 'action' => 'view', $checkout->user->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $checkout->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $checkout->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $checkout->id], ['confirm' => __('Are you sure you want to delete # {0}?', $checkout->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
