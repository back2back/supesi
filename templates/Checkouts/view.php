<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Checkout $checkout
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Checkout'), ['action' => 'edit', $checkout->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Checkout'), ['action' => 'delete', $checkout->id], ['confirm' => __('Are you sure you want to delete # {0}?', $checkout->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Checkouts'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Checkout'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="checkouts view content">
            <h3><?= h($checkout->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Session Id') ?></th>
                    <td><?= h($checkout->session_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $checkout->has('user') ? $this->Html->link($checkout->user->id, ['controller' => 'Users', 'action' => 'view', $checkout->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($checkout->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
