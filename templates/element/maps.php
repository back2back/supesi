<?php 

use Cake\Core\Configure;

$firebaseConfig = Configure::read('FirebaseConfig');
$jsonArray = json_encode($firebaseConfig);
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Google Maps with Firestore Data</title>
    <style>
      #map {
        
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <!-- Add the Google Maps JavaScript API script -->

    <script type="text/javascript">
         var jsArray = <?php echo $jsonArray; ?>;

        // Create a script element
        var scriptElement = document.createElement("script");

        // Set the src attribute dynamically
        scriptElement.src = "https://maps.googleapis.com/maps/api/js?key=" + jsArray.mapsKey ;

        // Optionally, set other attributes like type
        //scriptElement.type = "text/javascript";

        // Append the script element to the document head or body
        document.head.appendChild(scriptElement);
    </script>
    
    <!-- Add the Firebase JavaScript SDKs -->
    <script src="https://www.gstatic.com/firebasejs/10.12.2/firebase-app-compat.js"></script>
    <script src="https://www.gstatic.com/firebasejs/10.12.2/firebase-firestore-compat.js"></script>
  </head>
  <body>
    <div id="map"></div>

    <script>

        var jsArray = <?php echo $jsonArray; ?>;
        
      // Your Firebase configuration
        const firebaseConfig = {
        apiKey: jsArray.apiKey,
        authDomain: jsArray.authDomain,
        databaseURL: jsArray.databaseURL,
        projectId: jsArray.projectId,
        storageBucket: jsArray.storageBucket,
        messagingSenderId: jsArray.messagingSenderId,
        appId: jsArray.appId,
        measurementId: jsArray.measurementId
        };

      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);

      // Initialize Firestore
      const db = firebase.firestore();

      // Initialize and add the map
      function initMap() {
        // The location of the initial center of the map
        const center = { lat: -1.09878, lng: 37.0078224 };
        // The map, centered at the initial center
        const map = new google.maps.Map(document.getElementById("map"), {
          zoom: 12,
          center: center
        });

        



        // Retrieve data from Firestore and add markers
        db.collection(jsArray.collection).get().then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const data = doc.data();
            
            const marker = new google.maps.Marker({
              position: { lat: data.lat, lng: data.lng },
              map: map,
              title: data.user.phone
            });
            const infowindow = new google.maps.InfoWindow({
              content: data.user.phone
            });

            marker.addListener("click", () => {
              infowindow.open({
                anchor: marker,
                map,
              });
            });
          });
        });
      }

      // Load the map
      window.onload = initMap;
    </script>
  </body>
</html>
