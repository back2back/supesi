<?php 

use Cake\Core\Configure;

$firebaseConfig = Configure::read('FirebaseConfig');
$jsonArray = json_encode($firebaseConfig);

//$this->log(print_r($jwt, true));

$domain = $fullBaseUrl.'/api/order-entries/delivery.json';
$myOrders = $fullBaseUrl.'/order-entries';
?>
<style>
    #trip {
        height: 500px;
        width: 100%;
    }
    .controls {
        margin-top: 10px;
        display: flex;
        flex-direction: column;
    }
    .input-group {
        display: flex;
        margin-bottom: 10px;
    }
    .input-group input {
        flex: 1;
        margin-right: 10px;
    }
</style>
<div class="controls">
    <div class="input-group">
        <label for = "Start">Start</label>
        <input type="text" class="location" placeholder="Enter start point" id="Start" />
        <button type="button" class="remove-btn" aria-label="Remove location input" style="display: none;">Remove</button>
    </div>
    <button id="add-location-btn" aria-label="Add new location input">Add Location</button>
</div>
<div id="trip"></div>
<p id="distance"></p>
<p id="amount"></p>
<pre id="waypoints-data" style="display: none;"></pre>
<pre id="input-values"></pre>
<p>
    <button type="button" class="secondary" aria-label="Request delivery" onclick = "sendDeliveryRequest()">Confirm</button>
</p>
<script type="text/javascript">
         var jsArray = <?php echo $jsonArray; ?>;

        // Create a script element
        var scriptElement = document.createElement("script");

        // Set the src attribute dynamically
        scriptElement.src = "https://maps.googleapis.com/maps/api/js?key=" + jsArray.mapsKey + "&libraries=places" ;

        // Optionally, set other attributes like type
        //scriptElement.type = "text/javascript";

        // Append the script element to the document head or body
        document.head.appendChild(scriptElement);
</script>

<script>
        let map, directionsService, directionsRenderer;
        const locations = [];

        function initMap() {
            map = new google.maps.Map(document.getElementById('trip'), {
                center: { lat: -1.295992, lng: 36.824989 },
                zoom: 12
            });

            directionsService = new google.maps.DirectionsService();
            directionsRenderer = new google.maps.DirectionsRenderer();
            directionsRenderer.setMap(map);

            addAutocomplete(document.querySelector('.location'));
            document.getElementById('add-location-btn').addEventListener('click', addLocationInput);
        }

        function addAutocomplete(input) {
            const autocomplete = new google.maps.places.Autocomplete(input, {componentRestrictions : {country : 'KE'}});
            autocomplete.addListener('place_changed', () => {
                const place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
                calculateAndDisplayRoute();
            });
            locations.push({ input, autocomplete });
        }

        function addLocationInput() {
            const controlsDiv = document.querySelector('.controls');
            const inputGroup = document.createElement('div');
            inputGroup.className = 'input-group';

            const label = document.createElement('label');
            label.for = 'waypoint';
            label.textContent = "Add waypoint";
            inputGroup.appendChild(label);
            const input = document.createElement('input');
            input.type = 'text';
            input.className = 'location';
            input.placeholder = `Enter location ${locations.length + 1}`;
            inputGroup.appendChild(input);

            const removeBtn = document.createElement('button');
            removeBtn.type = 'button';
            removeBtn.className = 'remove-btn';
            removeBtn.innerText = 'Remove';
            removeBtn.setAttribute('aria-label', `Remove location input ${locations.length + 1}`);
            removeBtn.addEventListener('click', () => removeLocationInput(inputGroup, input));
            inputGroup.appendChild(removeBtn);

            controlsDiv.insertBefore(inputGroup, document.getElementById('add-location-btn'));
            addAutocomplete(input);
        }

        function removeLocationInput(inputGroup, input) {
            const index = locations.findIndex(loc => loc.input === input);
            if (index > -1) {
                locations.splice(index, 1);
            }
            inputGroup.remove();
            calculateAndDisplayRoute();
            
        }

        function calculateAndDisplayRoute() {
            if (locations.length < 2) {
                directionsRenderer.set('directions', null);
                document.getElementById('distance').innerText = '';
                document.getElementById('waypoints-data').innerText = '';
                return;
            }
            

            const validLocations = locations.filter(location => location.autocomplete.getPlace());
            if (validLocations.length < 2) {
                directionsRenderer.set('directions', null);
                document.getElementById('distance').innerText = '';
                document.getElementById('waypoints-data').innerText = '';
                return;
            }
            
            const waypoints = validLocations.slice(1, -1).map(location => {
            return {
                location: location.autocomplete.getPlace().geometry.location,
                stopover: true
            };
        });
            
            directionsService.route(
                {
                    origin: validLocations[0].autocomplete.getPlace().geometry.location,
                    destination: validLocations[validLocations.length - 1].autocomplete.getPlace().geometry.location,
                    waypoints: waypoints,
                    travelMode: google.maps.TravelMode.DRIVING
                    //add optimization maybe
                    //optimizeWaypoints: true
                   
                },
                (response, status) => {
                    if (status === google.maps.DirectionsStatus.OK) {
                        directionsRenderer.setDirections(response);
                        const route = response.routes[0];
                        const legs = route.legs;
                        
                        const totalDistance = legs.reduce((sum, leg) => sum + leg.distance.value, 0);
                        const distanceKms = (totalDistance / 1000).toFixed(2);
                        
                        const someAmount = (130) + distanceKms * 15;
                       
                        document.getElementById('distance').innerText = `Distance (kms): ${distanceKms}`;
                        document.getElementById('amount').innerText = `Amount (kes) ${someAmount}`;

                        const waypointData = legs.map((leg, index) => {
                            return {
                                start_location: {
                                    lat: leg.start_location.lat(),
                                    lng: leg.start_location.lng()
                                },
                                end_location: {
                                    lat: leg.end_location.lat(),
                                    lng: leg.end_location.lng()
                                },
                                distance: leg.distance.value,
                                duration: leg.duration.text,
                                start_address: leg.start_address,
                                end_address: leg.end_address
                            };
                        });
                        //document.getElementById('waypoints-data').innerText = JSON.stringify(waypointData, null, 2);
                        displayInputValues(waypointData);
                        
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                }
            );
        }

        function displayInputValues(mm) {
            const inputValues = locations.slice(1).map(location => location.input.value);
            //document.getElementById('input-values').innerText = JSON.stringify(inputValues, null, 2);
            const sth = addReadableAddresses(mm, inputValues)
            const foobar = JSON.stringify(sth, null, 2);

            document.getElementById('waypoints-data').innerText = foobar;

            
        }
        function addReadableAddresses(locations, addresses) {
            for (let i = 0; i < locations.length; i++) {
                locations[i].readable_start_address = addresses[i];
                locations[i].readable_end_address = addresses[i + 1];
            }
            return locations;
        }

        function sendDeliveryRequest(){
            dataAddresses = document.getElementById('waypoints-data').innerText;
            parsed = JSON.parse(dataAddresses);
            
            const jwtToken = "<?php echo $jwt;?>";
            axios
            .post("<?php echo $domain;?>", parsed, { headers: {"Authorization" : `Bearer ${jwtToken}`} })
            .then(response => {
               
                redirect = "<?php echo $myOrders;?>";
                location.replace(redirect)
            })
            .catch(function (error) {
                console.log(error);
            })

        }

        window.onload = initMap;

        
    </script>