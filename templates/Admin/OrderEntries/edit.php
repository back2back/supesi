<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderEntry $orderEntry
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $orderEntry->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $orderEntry->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Order Entries'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="orderEntries form content">
            <?= $this->Form->create($orderEntry) ?>
            <fieldset>
                <legend><?= __('Edit Order Entry') ?></legend>
                <?php
                    echo $this->Form->control('status', ['options' => $status]);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
