<?php
use Cake\I18n\FrozenTime;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderEntry[]|\Cake\Collection\CollectionInterface $orderEntries
 */
?>
<div class="orderEntries index content">
    <?= $this->Html->link(__('New Order Entry'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Order Entries') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('tenant_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($orderEntries as $orderEntry): ?>
                <tr>
                    <td><?= $this->Number->format($orderEntry->id) ?></td>
                    <td><?= $orderEntry->has('user') ? $this->Html->link($orderEntry->user->id, ['controller' => 'Users', 'action' => 'view', $orderEntry->user->id]) : '' ?></td>
                    <td><?= $orderEntry->has('tenant') ? $this->Html->link($orderEntry->tenant->name, ['controller' => 'Tenants', 'action' => 'view', $orderEntry->tenant->id]) : '' ?></td>
                    <td><?php 
                    
                    echo ($orderEntry->created)->timeAgoInWords()  ?></td>
                    
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $orderEntry->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $orderEntry->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $orderEntry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderEntry->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
