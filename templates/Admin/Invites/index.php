<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invite[]|\Cake\Collection\CollectionInterface $invites
 */
?>
<div class="invites index content">
    <?= $this->Html->link(__('New Invite'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Invites') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('token') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('is_used') ?></th>
                    <th><?= $this->Paginator->sort('invitee') ?></th>
                    <th><?= $this->Paginator->sort('tenant_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($invites as $invite): ?>
                <tr>
                    <td><?= $this->Number->format($invite->id) ?></td>
                    <td><?= h($invite->token) ?></td>
                    <td><?= $invite->has('user') ? $this->Html->link($invite->user->id, ['controller' => 'Users', 'action' => 'view', $invite->user->id]) : '' ?></td>
                    <td><?= $this->Number->format($invite->is_used) ?></td>
                    <td><?= h($invite->invitee) ?></td>
                    <td><?= $invite->has('tenant') ? $this->Html->link($invite->tenant->name, ['controller' => 'Tenants', 'action' => 'view', $invite->tenant->id]) : '' ?></td>
                    <td><?= h($invite->created) ?></td>
                    <td><?= h($invite->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $invite->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $invite->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $invite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invite->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
