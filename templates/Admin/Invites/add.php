<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invite $invite
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Invites'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="invites form content">
            <?= $this->Form->create($invite) ?>
            <fieldset>
                <legend><?= __('Add Invite') ?></legend>
                <?php
                    echo $this->Form->control('token');
                    echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('is_used');
                    echo $this->Form->control('invitee');
                    echo $this->Form->control('tenant_id', ['options' => $tenants]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
