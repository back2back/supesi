<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken $resetToken
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $resetToken->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $resetToken->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Reset Tokens'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="resetTokens form content">
            <?= $this->Form->create($resetToken) ?>
            <fieldset>
                <legend><?= __('Edit Reset Token') ?></legend>
                <?php
                    echo $this->Form->control('token');
                    echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('is_valid');
                    echo $this->Form->control('expires', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
