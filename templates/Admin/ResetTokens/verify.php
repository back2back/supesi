<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken $resetToken
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Reset Token'), ['action' => 'edit', $resetToken->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Reset Token'), ['action' => 'delete', $resetToken->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resetToken->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Reset Tokens'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Reset Token'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="resetTokens view content">
            <h3><?= h($resetToken->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Token') ?></th>
                    <td><?= h($resetToken->token) ?></td>
                </tr>
               
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($resetToken->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Valid') ?></th>
                    <td><?= $this->Number->format($resetToken->is_valid) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($resetToken->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($resetToken->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Expires') ?></th>
                    <td><?= h($resetToken->expires) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
