<?php

?>
<div class="users index content">
    <h3>Dashboard</h3>
    <ul>
    <li>
        <?= $this->Html->link(__('Create rider'), ['controller'=> 'Users', 'action' => 'rider'])?></li>
        <li><?= $this->Html->link(__('Users'), ['controller'=> 'Users', 'action' => 'index'])?></li>
        <li><?= $this->Html->link(__('Order Entries'), ['controller'=> 'OrderEntries', 'action' => 'index'])?></li>
        <li><?= $this->Html->link(__('Unassigned trips'), ['controller'=> 'Trips', 'action' => 'index'])?></li>

    </ul>
    <p>
    	<ul>
    		<li><?= $this->Html->link(__('Stores'), ['controller'=> 'Stores', 'action' => 'index'])?></li>
    	</ul>
    </p>

     <p>
    	<?= $this->element('maps')?>
    </p>
    
</div>
