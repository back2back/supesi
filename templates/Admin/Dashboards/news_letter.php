<div class="grid-x">
	<?= $this->Form->create() ?>
	<div class="cell large-12">
		
		<?=$this->Form->control('subject', ['size' => '60', 'required' => true]);?>
		<?=$this->Form->control('body',['type'=> 'textarea', 'required' => true]);?>
		<?= $this->Form->button(__('Send newsletter')) ?>
		<?= $this->Form->end() ?>
	</div>
</div>

