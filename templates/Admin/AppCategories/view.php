<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AppCategory $appCategory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit App Category'), ['action' => 'edit', $appCategory->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete App Category'), ['action' => 'delete', $appCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $appCategory->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List App Categories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New App Category'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="appCategories view content">
            <h3><?= h($appCategory->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($appCategory->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slug') ?></th>
                    <td><?= h($appCategory->slug) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $appCategory->has('user') ? $this->Html->link($appCategory->user->id, ['controller' => 'Users', 'action' => 'view', $appCategory->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($appCategory->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($appCategory->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($appCategory->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Stores') ?></h4>
                <?php if (!empty($appCategory->stores)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Tenant Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Slug') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Store Image Id') ?></th>
                            <th><?= __('Lat') ?></th>
                            <th><?= __('Lng') ?></th>
                            <th><?= __('Opening') ?></th>
                            <th><?= __('Closing') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('App Category Id') ?></th>
                            <th><?= __('Is Online') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($appCategory->stores as $stores) : ?>
                        <tr>
                            <td><?= h($stores->id) ?></td>
                            <td><?= h($stores->name) ?></td>
                            <td><?= h($stores->tenant_id) ?></td>
                            <td><?= h($stores->user_id) ?></td>
                            <td><?= h($stores->slug) ?></td>
                            <td><?= h($stores->created) ?></td>
                            <td><?= h($stores->modified) ?></td>
                            <td><?= h($stores->store_image_id) ?></td>
                            <td><?= h($stores->lat) ?></td>
                            <td><?= h($stores->lng) ?></td>
                            <td><?= h($stores->opening) ?></td>
                            <td><?= h($stores->closing) ?></td>
                            <td><?= h($stores->description) ?></td>
                            <td><?= h($stores->app_category_id) ?></td>
                            <td><?= h($stores->is_online) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Stores', 'action' => 'view', $stores->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Stores', 'action' => 'edit', $stores->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stores', 'action' => 'delete', $stores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stores->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
