<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AppCategory[]|\Cake\Collection\CollectionInterface $appCategories
 */
?>
<div class="appCategories index content">
    <?= $this->Html->link(__('New App Category'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('App Categories') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('slug') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($appCategories as $appCategory): ?>
                <tr>
                    <td><?= $this->Number->format($appCategory->id) ?></td>
                    <td><?= h($appCategory->name) ?></td>
                    <td><?= h($appCategory->slug) ?></td>
                    <td><?= $appCategory->has('user') ? $this->Html->link($appCategory->user->id, ['controller' => 'Users', 'action' => 'view', $appCategory->user->id]) : '' ?></td>
                    <td><?= h($appCategory->created) ?></td>
                    <td><?= h($appCategory->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $appCategory->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $appCategory->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $appCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $appCategory->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
