<?php

use Cake\Core\Configure;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Store $store
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Store'), ['action' => 'edit', $store->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Store'), ['action' => 'delete', $store->id], ['confirm' => __('Are you sure you want to delete # {0}?', $store->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Stores'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Store'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="stores view content">
            <h3><?= h($store->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($store->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tenant') ?></th>
                    <td><?= $store->has('tenant') ? $this->Html->link($store->tenant->name, ['controller' => 'Tenants', 'action' => 'view', $store->tenant->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $store->has('user') ? $this->Html->link($store->user->id, ['controller' => 'Users', 'action' => 'view', $store->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Slug') ?></th>
                    <td><?= h($store->slug) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($store->description) ?></td>
                </tr>
                
                <tr>
                    <th><?= __('Lat') ?></th>
                    <td><?= $this->Number->format($store->lat) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lng') ?></th>
                    <td><?= $this->Number->format($store->lng) ?></td>
                </tr>
                <tr>
                    <th><?= __('status') ?></th>
                    <td><?= h($store->is_online) ?></td>
                </tr>
                
                <tr>
                    <th><?= __('Opening') ?></th>
                    <td><?= h($store->opening) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing') ?></th>
                    <td><?= h($store->closing) ?></td>
                </tr>
            </table>
            <div class="related">
                <p>
                    <?php

                    $imageBucket = Configure::read('FileStorage.cdn');
                     echo $this->Html->image($imageBucket.'/'.$image->dir.'/'.$image->photo);?>
                </p>
            </div>
            <div class="related">
                <h5><?= __('Store products')?></h5>
                <p>
                    <table>
                        <tr>
                            <th><?= __('product id')?></th>
                            <th><?= __('Is available')?></th>
                            
                        </tr>
                        <?php foreach ($inStore as $data) : ?>
                        <tr>
                            <td><?= $data->product->title?></td>
                            <td><?= $data->is_available?></td>
                        </tr>
                    <?php endforeach; ?>
                    </table>
                </p>
            </div>
            
        </div>
    </div>
</div>
