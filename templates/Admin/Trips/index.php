<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Trip[]|\Cake\Collection\CollectionInterface $trips
 */
?>
<div class="trips index content">
    
    <h3><?= __('Trips') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    
                    
                    <th><?= $this->Paginator->sort('amount') ?></th>
                    <th><?= $this->Paginator->sort('order_entry_id') ?></th>
                    <th><?= $this->Paginator->sort('comment') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($trips as $trip): ?>
                <tr>
                    <td><?= $this->Number->format($trip->id) ?></td>
                    
                    
                    <td><?= $this->Number->format($trip->nice_amount) ?></td>
                    <td><?= $trip->has('order_entry') ? $this->Html->link($trip->order_entry->id, ['controller' => 'OrderEntries', 'action' => 'view', $trip->order_entry->id]) : '' ?></td>
                    <td><?= h($trip->comment) ?></td>
                    <td><?= h($trip->created->timeAgoInWords()) ?></td>
                   
                    <td class="actions">
                        <?= $this->Html->link(__('View '), ['action' => 'view', $trip->id]) ?>
                        
                        
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
   
</div>
