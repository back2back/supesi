<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Trip $trip
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Trip'), ['action' => 'edit', $trip->id], ['class' => 'side-nav-item']) ?>
            
            <?= $this->Html->link(__('List Trips'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Trip'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Finish Trip'), ['action' => 'finish', $trip->id], ['confirm' => __('Are you sure you want to finish # {0}?', $trip->id), 'class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="trips view content">
            <h3><?= h($trip->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $trip->has('user') ? $this->Html->link($trip->user->id, ['controller' => 'Users', 'action' => 'view', $trip->user->id]) : 'No rider' ?></td>
                </tr>
                <tr>
                    <th><?= __('Order Entry') ?></th>
                    <td><?= $trip->has('order_entry') ? $this->Html->link($trip->order_entry->id, ['controller' => 'OrderEntries', 'action' => 'view', $trip->order_entry->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($trip->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($trip->id) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status') ?></th>
                    <td><?php
                    if ($trip->status == 1) {
                        echo __('Trip is complete');
                    }
                    if ($trip->status == 0 && $trip->user_id == 0) {
                        echo __('Waiting to be assigned');
                    }
                     ?></td>
                </tr>
                
                    
                </tr>
                    <th><?= __('Amount') ?></th>
                    <td><?= $this->Number->format($trip->nice_amount) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($trip->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Start Location') ?></th>
                    <td><?= h($trip->readable_start) ?></td>
                </tr>
                <tr>
                    <th><?= __('End Location') ?></th>
                    <td><?= h($trip->readable_end) ?></td>
                </tr>
                
            </table>
            <div id="map">
                
            </div>
            <div id="rider-container">
                
            </div>
            
        </div>
    </div>

</div>
<?=
     $this->Html->script('fire',['options' => ['block' => true]]) ?>
<script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-firestore.js"></script>

<script>
var firebaseConfig = {
    apiKey: "AIzaSyAbnoJVYz1uaqfJqG3KX0tqry1AMds46ws",
    authDomain: "supesi.firebaseapp.com",
    databaseURL: "https://supesi.firebaseio.com",
    projectId: "supesi",
    storageBucket: "supesi.appspot.com",
    messagingSenderId: "837539953201",
    appId: "1:837539953201:web:bd550861e64a287f493310",
    measurementId: "G-YKZH5QP7M7"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  

  var firestore = firebase.firestore();

  firestore.collection("location").get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().lat}`);
    });
});



    function initMap() {
      // The location of Uluru
      var shop = {lat: <?php echo $trip->start_lat?>, lng: <?php echo $trip->start_lng ?>};
      // The map, centered at shop
      var map = new google.maps.Map(
          document.getElementById('map'), {zoom: 12, center: shop});

      var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

      // The marker, positioned at shop
      var marker = new google.maps.Marker({
        position: shop,
        map: map,
        icon: image
    });

      
      firestore.collection("location").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
               //riders

               var foo = {lat: doc.data().lat, lng: doc.data().lng}

               var riders = new google.maps.Marker({
                position: foo, 
                map: map, 
                });
                var king = `${doc.data().user.id}`;

               var datas = `<a href="<?= $domain. $trip->id.'/'.'${king}'?>">Assign </a>`;
               
               

               var infowindow = new google.maps.InfoWindow({
                    content: datas
                  });

               riders.addListener('click', function() {
                infowindow.open(map, riders);
              });

            });
        });
}
</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6XSYMWmOrhDyjx2jc3HvZ2GOa7iJOeB0&callback=initMap">
</script>
