<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreImage[]|\Cake\Collection\CollectionInterface $storeImages
 */
?>
<div class="storeImages index content">
    <?= $this->Html->link(__('New Store Image'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Store Images') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('store_id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('photo') ?></th>
                    <th><?= $this->Paginator->sort('dir') ?></th>
                    <th><?= $this->Paginator->sort('size') ?></th>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($storeImages as $storeImage): ?>
                <tr>
                    <td><?= $this->Number->format($storeImage->id) ?></td>
                    <td><?= $this->Number->format($storeImage->store_id) ?></td>
                    <td><?= $storeImage->has('user') ? $this->Html->link($storeImage->user->id, ['controller' => 'Users', 'action' => 'view', $storeImage->user->id]) : '' ?></td>
                    <td><?= h($storeImage->photo) ?></td>
                    <td><?= h($storeImage->dir) ?></td>
                    <td><?= $this->Number->format($storeImage->size) ?></td>
                    <td><?= h($storeImage->type) ?></td>
                    <td><?= h($storeImage->created) ?></td>
                    <td><?= h($storeImage->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $storeImage->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $storeImage->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $storeImage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeImage->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
