<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreImage $storeImage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Store Image'), ['action' => 'edit', $storeImage->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Store Image'), ['action' => 'delete', $storeImage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeImage->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Store Images'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Store Image'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeImages view content">
            <h3><?= h($storeImage->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $storeImage->has('user') ? $this->Html->link($storeImage->user->id, ['controller' => 'Users', 'action' => 'view', $storeImage->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Photo') ?></th>
                    <td><?= h($storeImage->photo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dir') ?></th>
                    <td><?= h($storeImage->dir) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($storeImage->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($storeImage->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Store Id') ?></th>
                    <td><?= $this->Number->format($storeImage->store_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Size') ?></th>
                    <td><?= $this->Number->format($storeImage->size) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($storeImage->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($storeImage->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Stores') ?></h4>
                <?php if (!empty($storeImage->stores)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Tenant Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Slug') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Store Image Id') ?></th>
                            <th><?= __('Lat') ?></th>
                            <th><?= __('Lng') ?></th>
                            <th><?= __('Opening') ?></th>
                            <th><?= __('Closing') ?></th>
                            <th><?= __('Description') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($storeImage->stores as $stores) : ?>
                        <tr>
                            <td><?= h($stores->id) ?></td>
                            <td><?= h($stores->name) ?></td>
                            <td><?= h($stores->tenant_id) ?></td>
                            <td><?= h($stores->user_id) ?></td>
                            <td><?= h($stores->slug) ?></td>
                            <td><?= h($stores->created) ?></td>
                            <td><?= h($stores->modified) ?></td>
                            <td><?= h($stores->store_image_id) ?></td>
                            <td><?= h($stores->lat) ?></td>
                            <td><?= h($stores->lng) ?></td>
                            <td><?= h($stores->opening) ?></td>
                            <td><?= h($stores->closing) ?></td>
                            <td><?= h($stores->description) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Stores', 'action' => 'view', $stores->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Stores', 'action' => 'edit', $stores->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stores', 'action' => 'delete', $stores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stores->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
