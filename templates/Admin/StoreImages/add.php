<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreImage $storeImage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Store Images'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeImages form content">
            <?= $this->Form->create($storeImage, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Add Store Image') ?></legend>
                <?php
                    echo $this->Form->control('user_id', ['options' => $users]);
                    
                    echo $this->Form->control('photo', ['type' => 'file']);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
