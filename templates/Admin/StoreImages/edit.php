<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreImage $storeImage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $storeImage->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $storeImage->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Store Images'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeImages form content">
            <?= $this->Form->create($storeImage) ?>
            <fieldset>
                <legend><?= __('Edit Store Image') ?></legend>
                <?php
                    echo $this->Form->control('store_id');
                    echo $this->Form->control('user_id', ['options' => $users]);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
