<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rider $rider
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Rider'), ['action' => 'edit', $rider->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Rider'), ['action' => 'delete', $rider->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rider->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Riders'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Rider'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="riders view content">
            <h3><?= h($rider->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td >

                        <?= $rider->has('user') ? $this->Html->link($rider->user->id, ['controller' => 'Users', 'action' => 'view', $rider->user->id]) : '' ?>
                            
                        </td>
                    <td id="my">
                        <?= $rider->user->id?>
                    </td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($rider->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= $this->Number->format($rider->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created By') ?></th>
                    <td><?= $this->Number->format($rider->created_by) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($rider->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($rider->modified) ?></td>
                </tr>
            </table>
            <div id="app">
                <?= $this->Html->link(__('Link to firebase'))?>
            </div>
        </div>
    </div>
</div>

    <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js"></script>

<script>
   
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyAbnoJVYz1uaqfJqG3KX0tqry1AMds46ws",
    authDomain: "supesi.firebaseapp.com",
    databaseURL: "https://supesi.firebaseio.com",
    projectId: "supesi",
    storageBucket: "supesi.appspot.com",
    messagingSenderId: "837539953201",
    appId: "1:837539953201:web:bd550861e64a287f493310",
    measurementId: "G-YKZH5QP7M7"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  var firestore = firebase.firestore();

    const docRef = firestore.doc("supesi/location");

    const userId = document.querySelector("#my");
    const saveButton = document.querySelector("#app");
    saveButton.addEventListener("click", function(){
        const textToSave = userId.value;
        console.log("some textToSave " + textToSave);

        docRef.set({
            user_id: userId
        }).then(function(){
            console.log("success");
        }).catch( function(error){
            console.log("error" , error);
        })
    });

</script>

<script>
    var csrfToken = <?= json_encode($this->request->getAttribute('csrfToken')) ?>;
    console.log(csrfToken);

    /*var app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue!'
      }
    });*/
</script>
<script >
    










</script>
