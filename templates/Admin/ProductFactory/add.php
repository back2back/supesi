<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductFactory $productFactory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Product Factory'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productFactory form content">
            <?= $this->Form->create($productFactory) ?>
            <fieldset>
                <legend><?= __('Add Product Factory') ?></legend>
                <?php
                    echo $this->Form->control('title');
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
