<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductFactory $productFactory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productFactory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productFactory->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Product Factory'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productFactory form content">
            <?= $this->Form->create($productFactory) ?>
            <fieldset>
                <legend><?= __('Edit Product Factory') ?></legend>
                <?php
                    echo $this->Form->control('title');
                    echo $this->Form->control('slug');
                    echo $this->Form->control('user_id', ['options' => $users]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
