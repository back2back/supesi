<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductFactory[]|\Cake\Collection\CollectionInterface $productFactory
 */
?>
<div class="productFactory index content">
    <?= $this->Html->link(__('New Product Factory'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Product Factory') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('slug') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($productFactory as $productFactory): ?>
                <tr>
                    <td><?= $this->Number->format($productFactory->id) ?></td>
                    <td><?= h($productFactory->name) ?></td>
                    <td><?= h($productFactory->slug) ?></td>
                    <td><?= $productFactory->has('user') ? $this->Html->link($productFactory->user->id, ['controller' => 'Users', 'action' => 'view', $productFactory->user->id]) : '' ?></td>
                    <td><?= h($productFactory->created) ?></td>
                    <td><?= h($productFactory->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $productFactory->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productFactory->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productFactory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productFactory->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
