<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductFactory $productFactory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Product Factory'), ['action' => 'edit', $productFactory->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Product Factory'), ['action' => 'delete', $productFactory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productFactory->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Product Factory'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Product Factory'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productFactory view content">
            <h3><?= h($productFactory->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($productFactory->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slug') ?></th>
                    <td><?= h($productFactory->slug) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $productFactory->has('user') ? $this->Html->link($productFactory->user->id, ['controller' => 'Users', 'action' => 'view', $productFactory->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($productFactory->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($productFactory->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($productFactory->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
