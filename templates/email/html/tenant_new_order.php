<?php

?>
<p>
	<?php  echo __('Congratulations ')?>
</p>
<p>
	<?php echo __('You have received a new order. Please login to your account and process it. The details are below')?>
</p>
<p>
	<?php foreach ($cartItems as $data): ?>
		<ul>
			<li> <?= __('Name: ')?><?= $data->product->title?></li>
			<li><?= __('Quantity: ')?><?= $data->qty ?></li>
		</ul>
	<?php endforeach; ?>		
	</p>