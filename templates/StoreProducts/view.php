<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreProduct $storeProduct
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Store Product'), ['action' => 'edit', $storeProduct->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Store Product'), ['action' => 'delete', $storeProduct->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeProduct->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Store Products'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Store Product'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeProducts view content">
            <h3><?= h($storeProduct->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Product') ?></th>
                    <td><?= $storeProduct->has('product') ? $this->Html->link($storeProduct->product->title, ['controller' => 'Products', 'action' => 'view', $storeProduct->product->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Store') ?></th>
                    <td><?= $storeProduct->has('store') ? $this->Html->link($storeProduct->store->name, ['controller' => 'Stores', 'action' => 'view', $storeProduct->store->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $storeProduct->has('user') ? $this->Html->link($storeProduct->user->id, ['controller' => 'Users', 'action' => 'view', $storeProduct->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($storeProduct->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($storeProduct->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($storeProduct->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
