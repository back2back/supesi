<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreProduct[]|\Cake\Collection\CollectionInterface $storeProducts
 */
?>
<div class="storeProducts index content">
    
    <h3><?= __('Store Products') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('product_id') ?></th>
                    <th><?= $this->Paginator->sort('store_id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th> <?= __('Product available?')?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($storeProducts as $storeProduct): ?>
                <tr>
                    <td><?= $this->Number->format($storeProduct->id) ?></td>
                    <td><?= $storeProduct->has('product') ? $this->Html->link($storeProduct->product->title, ['controller' => 'Products', 'action' => 'view', $storeProduct->product->id]) : '' ?></td>
                    <td><?= $storeProduct->has('store') ? $this->Html->link($storeProduct->store->name, ['controller' => 'Stores', 'action' => 'view', $storeProduct->store->id]) : '' ?></td>
                    <td><?= $storeProduct->has('user') ? $this->Html->link($storeProduct->user->id, ['controller' => 'Users', 'action' => 'view', $storeProduct->user->id]) : '' ?></td>
                    <td>
                        <?php 
                        if ($storeProduct->is_available == 1) {
                            echo __('Online');
                        }else{
                            echo __('Offline');
                        }
                        ?>
                    </td>
                    <td class="actions">
                        
                        <?= $this->Html->link(__('Toggle online/ offline'), ['action' => 'edit', $storeProduct->id]) ?>
                        
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
