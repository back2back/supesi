<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreProduct $storeProduct
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $storeProduct->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $storeProduct->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Store Products'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeProducts form content">
            <?= $this->Form->create($storeProduct) ?>
            <fieldset>
                <legend><?= __('Choose availability') ?></legend>
                <?php
                    echo $this->Form->control('is_available', ['options' => $isOnline]);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
