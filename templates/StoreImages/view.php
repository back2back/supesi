<?php
use Cake\Core\Configure;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreImage $storeImage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= __('What users see when they load your store')?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeImages view content">
            <h3><?= h($storeImage->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $storeImage->has('user') ? $this->Html->link($storeImage->user->id, ['controller' => 'Users', 'action' => 'view', $storeImage->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Dir') ?></th>
                    <td><?= h($storeImage->dir) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($storeImage->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($storeImage->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Store Id') ?></th>
                    <td><?= $this->Number->format($storeImage->store_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Size') ?></th>
                    <td><?= $this->Number->format($storeImage->size) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($storeImage->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($storeImage->modified) ?></td>
                </tr>
            </table>
            <div>
                <?php 
                    echo $this->Html->image(Configure::read('FileStorage.cdn').'/'.$storeImage->dir.'/'.$storeImage->photo);
                ?>
            </div>
        </div>
    </div>
</div>
