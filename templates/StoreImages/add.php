<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreImage $storeImage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <?= $store->name ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeImages form content">
            <?= $this->Form->create($storeImage, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Add Store banner image. ') ?></legend>
                
                <p><?= __('Image must be png/jpg/jpeg format')?></p>
                <p><?= __('Image must be 1280 px by 720 px')?></p>
                <?php
                   
                    echo $this->Form->control('photo', ['type' => 'file']);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
