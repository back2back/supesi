<?php

use Cake\Core\Configure;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Store $store
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            

            <?= $this->Html->link(__('List Stores'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Store Products'), ['controller'=>'StoreProducts', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="stores view content">
            <h3><?= h($store->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($store->name) ?></td>
                </tr>
                
                    <th><?= __('Created') ?></th>
                    <td><?= h($store->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($store->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Opening') ?></th>
                    <td><?= h($store->opening) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing') ?></th>
                    <td><?= h($store->closing) ?></td>
                </tr>
            </table>
            <div>
                <?php if($image->first() === null): ?>
                    <div>
                        <?= $this->Html->link(__('Add store banner'), [
                            'controller' => 'StoreImages',
                            'action' => 'add',$store->id
                        ])?>
                    </div>
                <?php else:?>
                    <div>
                        <p>
                            <?php 
                        echo $this->Html->image(Configure::read('FileStorage.cdn').'/'.$image->first()->dir.'/'.$image->first()->photo);
                        ?>
                            
                        </p>
                    </div>
                <?php endif; ?>
            </div>
            
            
        </div>
    </div>
</div>
