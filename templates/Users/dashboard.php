<?php

use Cake\I18n\FrozenTime;
use Cake\I18n\FrozenDate;
use Cake\Core\Configure;
use Cake\Log\Log;

$time =  FrozenTime::now();
$startOfMonth = $time->startOfMonth();

$domain = $fullBaseUrl.'/api/order-entries/interval/2.json';
$stocks = $fullBaseUrl.'/api/stocks.json';

//add means to get the jwtToken

?>
<div class="users index content">
	<p><?= $this->Html->link(__('Request Delivery'),['controller' => 'OrderEntries', 'action' => 'delivery'])?></p>
	<p><?= $this->Html->link(__('Manage products'),['controller' => 'Products'])?></p>
	<p><?= $this->Html->link(__('Check stocks'),['controller' => 'Stocks'])?></p>
	<p><?= $this->Html->link(__('View balance'),['controller' => 'Accounts'])?></p>
	<p><?= $this->Html->link(__('Stores'),['controller' => 'Stores'])?></p>
	<p><?= $this->Html->link(__('Manage teams'),['controller' => 'Teams'])?></p>
	<p><?= $this->Html->link(__('Customer manage'),['controller' => 'Customers'])?></p>
	<p><?= $this->Html->link(__('Manage Invoices'),['controller' => 'Invoices'])?></p>
</div>

</br>
<div id="app" class="users index content">
	<div class="grid-x">
		<div class="cell large-12">
			<p>	
				Reporting period:  <?= __($startOfMonth->nice())?> to : <?=__($time->nice())?>

			</p>

			
		</div>
	</div>
	<div class="grid-x">
		<div class="cell large-6">
			<p>
				<div class="card sales" style="width: 300px;">
				  
				  <div class="card-section" v-if = "info">
				    <h4>Sales</h4>
				    <p>{{info.total}} </p>
				  </div>
				</div>
			</p>
		</div>
		<div class="cell large-6">
			<p>
				<div class="card profits" style="width: 300px;">
				  
				  <div class="card-section" v-if = "info">
				    <h4>Profits</h4>
				    <p>{{info.profit}}</p>
				  </div>
				</div>
			</p>
		</div>
	</div>

	<div class="grid-x">
		<div class="cell large-6">
			<p>
				<div class="card orders" style="width: 300px;">
				  
				  <div class="card-section" v-if = "info">
				    <h4>Orders</h4>
				    <p>{{info.orderEntries.length}}

				    </p>
				  </div>
				</div>
			</p>
		</div>
		<div class="cell large-6">
			<p>
				<div class="card stocks" style="width: 300px;">
				  
				  <div class="card-section" v-if = "stock">
				    <h4>Stocks</h4>
				    <p> All products {{stock.stocks.length}} </p>
				  </div>
				</div>
			</p>
		</div>
	</div>
	
</div>
<script>

var app = new Vue({
  el: '#app',
  data() {
    return {
      info : '',
      stock: ''
    }
  },
  mounted(){
	const jwtToken = "<?php echo $jwt;?>";
	
    axios
      .get("<?php echo $domain;?>", { headers: {"Authorization" : `Bearer ${jwtToken}`} })
      .then(response => {
		this.info = response.data
	})
	  .catch(error => {
        console.error('Error fetching info:', error.response ? error.response.data : error.message);
      });

    axios
      .get("<?php echo $stocks;?>",  { headers: {"Authorization" : `Bearer ${jwtToken}`} })
      .then(response => (this.stock = response.data))
  

  }
})
</script>