<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <p> <?php echo __('Create your Business account. This name cannot be changed later') ?></p>
            <p>
                <?php 
                echo __('By Creating an account you agree to our terms located at');
                ?>
            </p>
            <p>
                <?= $this->Html->link(__('Terms'), ['controller' => 'Pages', 'action' => 'terms'])?>
            </p>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Add User') ?></legend>
                <?php
                    echo $this->Form->control('f_name', ['label' => 'First name']);
                    echo $this->Form->control('l_name', ['label' => 'Last name']);
                    echo $this->Form->control('phone', ['placeholder' => '07xxx']);
                    echo $this->Form->control('email');
                    echo $this->Form->control('password');
                    
                    echo $this->Form->control('Tenant.name', ['label' => __('Business name')]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div>
            <?= $this->Html->link(__('Or login'), ['controller' => 'Users', 'action' => 'login'])?>
        </div>
    </div>

</div>

