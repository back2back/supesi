<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rider[]|\Cake\Collection\CollectionInterface $riders
 */
?>
<div class="riders index content">
    <?= $this->Html->link(__('New Rider'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Riders') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th><?= $this->Paginator->sort('created_by') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($riders as $rider): ?>
                <tr>
                    <td><?= $this->Number->format($rider->id) ?></td>
                    <td><?= $rider->has('user') ? $this->Html->link($rider->user->id, ['controller' => 'Users', 'action' => 'view', $rider->user->id]) : '' ?></td>
                    <td><?= $this->Number->format($rider->status) ?></td>
                    <td><?= $this->Number->format($rider->created_by) ?></td>
                    <td><?= h($rider->created) ?></td>
                    <td><?= h($rider->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $rider->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rider->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rider->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rider->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
