<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductImage $productImage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <p><?= $product->title?></p>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productImages form content">
            <?= $this->Form->create($productImage,['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Add Product Image. Consider downloading HI-RES quality from the internet.') ?></legend>
                
                <p><?= __('Image must be png/jpg/jpeg format')?></p>
                <p><?= __('Image must be 512 px by 512 px')?></p>
                <?php
                
                    echo $this->Form->control('name', ['type' => 'file']);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
