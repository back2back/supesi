<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreCategory $storeCategory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Store Category'), ['action' => 'edit', $storeCategory->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Store Category'), ['action' => 'delete', $storeCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeCategory->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Store Categories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Store Category'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="storeCategories view content">
            <h3><?= h($storeCategory->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Store') ?></th>
                    <td><?= $storeCategory->has('store') ? $this->Html->link($storeCategory->store->name, ['controller' => 'Stores', 'action' => 'view', $storeCategory->store->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($storeCategory->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slug') ?></th>
                    <td><?= h($storeCategory->slug) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $storeCategory->has('user') ? $this->Html->link($storeCategory->user->id, ['controller' => 'Users', 'action' => 'view', $storeCategory->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($storeCategory->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($storeCategory->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($storeCategory->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
