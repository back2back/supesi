<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StoreCategory[]|\Cake\Collection\CollectionInterface $storeCategories
 */
?>
<div class="storeCategories index content">
    <?= $this->Html->link(__('New Store Category'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Store Categories') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('store_id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('slug') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($storeCategories as $storeCategory): ?>
                <tr>
                    <td><?= $this->Number->format($storeCategory->id) ?></td>
                    <td><?= $storeCategory->has('store') ? $this->Html->link($storeCategory->store->name, ['controller' => 'Stores', 'action' => 'view', $storeCategory->store->id]) : '' ?></td>
                    <td><?= h($storeCategory->name) ?></td>
                    <td><?= h($storeCategory->slug) ?></td>
                    <td><?= $storeCategory->has('user') ? $this->Html->link($storeCategory->user->id, ['controller' => 'Users', 'action' => 'view', $storeCategory->user->id]) : '' ?></td>
                    <td><?= h($storeCategory->created) ?></td>
                    <td><?= h($storeCategory->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $storeCategory->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $storeCategory->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $storeCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeCategory->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
