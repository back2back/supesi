<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invite $invite
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Invites'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="invites form content">
            <?= $this->Form->create($invite) ?>
            <fieldset>
                <legend><?= __('Add Team member') ?></legend>
                <?php
                    
                    echo $this->Form->control('f_name', ['label' => 'First name', 'required' => true]);
                    echo $this->Form->control('l_name', ['label' => 'Last name', 'required' => true]);
                    echo $this->Form->control('phone', ['placeholder' => '07xxx', 'required' => true]);
                    echo $this->Form->control('invitee',  ['label'=> 'Valid email','placeholder' => __('email@example.com'), 'value' => '']);
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
