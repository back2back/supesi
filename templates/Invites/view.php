<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invite $invite
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Invite'), ['action' => 'edit', $invite->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Invite'), ['action' => 'delete', $invite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invite->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Invites'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Invite'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="invites view content">
            <h3><?= h($invite->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Token') ?></th>
                    <td><?= h($invite->token) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $invite->has('user') ? $this->Html->link($invite->user->id, ['controller' => 'Users', 'action' => 'view', $invite->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Invitee') ?></th>
                    <td><?= h($invite->invitee) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tenant') ?></th>
                    <td><?= $invite->has('tenant') ? $this->Html->link($invite->tenant->name, ['controller' => 'Tenants', 'action' => 'view', $invite->tenant->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($invite->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Used') ?></th>
                    <td><?= $this->Number->format($invite->is_used) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($invite->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($invite->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
