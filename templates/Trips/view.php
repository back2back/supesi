<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Trip $trip
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Trip'), ['action' => 'edit', $trip->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Trip'), ['action' => 'delete', $trip->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trip->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Trips'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Trip'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="trips view content">
            <h3><?= h($trip->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $trip->has('user') ? $this->Html->link($trip->user->id, ['controller' => 'Users', 'action' => 'view', $trip->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Order Entry') ?></th>
                    <td><?= $trip->has('order_entry') ? $this->Html->link($trip->order_entry->id, ['controller' => 'OrderEntries', 'action' => 'view', $trip->order_entry->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($trip->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($trip->id) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status') ?></th>
                    <td><?= 
                    if ($trip->status == 1) {
                        echo __('Trip is complete');
                    }
                    if ($trip->status == 0) {
                        echo __('Waiting to be assigned');
                    }
                     ?></td>
                </tr>
                
                    <th><?= __('Amount') ?></th>
                    <td><?= $this->Number->format($trip->amount) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($trip->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($trip->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
