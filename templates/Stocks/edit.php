<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Stock $stock
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            
            <?= $this->Html->link(__('List Stocks'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class=" form content">
            <?= $stock->niceCounted?>
            <?= $this->Form->create($stock) ?>
            <fieldset>
                <legend><?= __('Items in Stock') ?></legend>
                <?php
                    
                    echo $this->Form->control('restock', [
                        'label'=> 'Enter new value. Value will be added to current stock ',

                    ]);
                    echo $this->Form->control('threshold', ['label' => 
                        'What value to trigger a low stock alert', 'value' => $stock->niceThreshold])
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
