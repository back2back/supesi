<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
 */
?>

<div class="products index content">
    <?= $this->Html->link(__('New Product'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Products') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    
                    
                    <th><?= $this->Paginator->sort('title') ?></th>
                    <th><?= $this->Paginator->sort('Price') ?></th>
                    <th><?= $this->Paginator->sort('Quantity') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($guestCart as $guestCheckout): ?>
                <tr>
                    
                   
                    <td><?= h($guestCheckout->product->title) ?></td>
                   
                    <td><?= h($guestCheckout->product->nicePriceOut) ?></td>
                    <td><?= $guestCheckout->quantity ?></td>
                    <td class="actions">
                        
                        <?= $this->Html->link(__('update'), ['action' => 'edit', $guestCheckout->id]) ?>

                       
                        
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>

        </table>
    </div>
    <?= $this->Html->link(__('Continue Shopping'), ['action' => 'index'], ['class' => 'button float-right']) ?>
            <?= $this->Html->link(__('Proceed to pay'), ['controller' => 'Users','action' => 'loginCustomer'], ['class' => 'button float-right']) ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
