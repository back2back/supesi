<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Products'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="products form content">
            <?= $this->Form->create($product) ?>
            <fieldset>
                <legend><?= __('Add Product') ?></legend>
                <?php
                    
                    echo $this->Form->control('title');
                    echo $this->Form->control('description');
                    echo $this->Form->control('price_out', ['label' => __('Sell at'), 'required' => true]);
                     echo $this->Form->control('price_in', [
                        'label' => __('Buying cost'),
                        'placeholder' => 'Might help you analyze sales',
                        'value' => false,
                    ]);
                      echo $this->Form->control('stock_count', [
                        'label' => __('Available stock'),
                        'placeholder' => 'Might help you analyze stocks',

                    ]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
