<?php

use Cake\Core\Configure;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id], ['class' => 'side-nav-item']) ?>
            
            <?= $this->Html->link(__('List Products'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Product'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>

    <div class="column-responsive column-80">
        <div class="products view content">



            <h3><?= h($product->title) ?></h3>
            <!-- 
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $product->has('user') ? $this->Html->link($product->user->id, ['controller' => 'Users', 'action' => 'view', $product->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Tenant') ?></th>
                    <td><?= $product->has('tenant') ? $this->Html->link($product->tenant->name, ['controller' => 'Tenants', 'action' => 'view', $product->tenant->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Title') ?></th>
                    <td><?= h($product->title) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($product->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($product->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($product->modified) ?></td>
                </tr>
            </table> 
            -->
            <div class="text">
                <strong><?= __('Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($product->description)); ?>
                </blockquote>
            </div>
            <div>
                <?php if($image->first() === null): ?>
                    <div>
                        <?= $this->Html->link(__('Add image'), [
                            'controller' => 'ProductImages',
                            'action' => 'add',$product->id
                        ])?>
                    </div>
                <?php else:?>
                    <div>
                        <?php 
                        echo $this->Html->image(Configure::read('FileStorage.cdn').'/'.$image->first()->dir.'/'.$image->first()->name);
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            
            
        </div>
        
    </div>
</div>
