<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AppCategory $appCategory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit App Category'), ['action' => 'edit', $appCategory->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete App Category'), ['action' => 'delete', $appCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $appCategory->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List App Categories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New App Category'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="appCategories view content">
            <h3><?= h($appCategory->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($appCategory->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slug') ?></th>
                    <td><?= h($appCategory->slug) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $appCategory->has('user') ? $this->Html->link($appCategory->user->id, ['controller' => 'Users', 'action' => 'view', $appCategory->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($appCategory->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($appCategory->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($appCategory->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
