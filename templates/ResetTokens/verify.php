<?php
$this->layout = 'anonymous';
?>

<div class="row">
    
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create() ?>
            <fieldset>
                <legend><?= __('Edit User') ?></legend>
                <?php
                    
                    echo $this->Form->control('password');
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>