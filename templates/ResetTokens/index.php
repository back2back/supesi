<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken[]|\Cake\Collection\CollectionInterface $resetTokens
 */
?>
<div class="resetTokens index content">
    <?= $this->Html->link(__('New Reset Token'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Reset Tokens') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('token') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('is_valid') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('expires') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resetTokens as $resetToken): ?>
                <tr>
                    <td><?= $this->Number->format($resetToken->id) ?></td>
                    <td><?= h($resetToken->token) ?></td>
                    <td><?= $resetToken->has('user') ? $this->Html->link($resetToken->user->id, ['controller' => 'Users', 'action' => 'view', $resetToken->user->id]) : '' ?></td>
                    <td><?= $this->Number->format($resetToken->is_valid) ?></td>
                    <td><?= h($resetToken->created) ?></td>
                    <td><?= h($resetToken->modified) ?></td>
                    <td><?= h($resetToken->expires) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $resetToken->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $resetToken->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $resetToken->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resetToken->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
