<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken $resetToken
 */
?>
<div class="row">
    
    <div class="column-responsive column-80">
        <div class="resetTokens form content">
            <?= $this->Form->create($resetToken) ?>
            <fieldset>
                <legend><?= __('Reset password') ?></legend>
                <?php
                    echo $this->Form->control('email');
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
