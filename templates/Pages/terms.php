<?php
$this->layout = 'anonymous';
?>
<h2>Terms &amp; Conditions General</h2> <p>
                    By downloading or using the app, these terms will
                    automatically apply to you – you should make sure therefore
                    that you read them carefully before using the app. You’re
                    not allowed to copy, or modify the app, any part of the app,
                    or our trademarks in any way. You’re not allowed to attempt
                    to extract the source code of the app, and you also
                    shouldn’t try to translate the app into other languages, or
                    make derivative versions. The app itself, and all the trade
                    marks, copyright, database rights and other intellectual
                    property rights related to it, still belong to
                    oneapp technologies.
                  </p> <p>
                    oneapp technologies is committed to ensuring that the app
                    is as useful and efficient as possible. For that reason, we
                    reserve the right to make changes to the app or to charge
                    for its services, at any time and for any reason. We will
                    never charge you for the app or its services without making
                    it very clear to you exactly what you’re paying for.
                  </p> <p>
                    The Fastee app stores and processes personal data
                    that you have provided to us, in order to provide
                    our Service. It’s your responsibility to keep your
                    phone and access to the app secure. We therefore recommend
                    that you do not jailbreak or root your phone, which is the
                    process of removing software restrictions and limitations
                    imposed by the official operating system of your device. It
                    could make your phone vulnerable to
                    malware/viruses/malicious programs, compromise your phone’s
                    security features and it could mean that the
                    Fastee app won’t work properly or at all.
                  </p> <p>
                    The app does use third party services that declare their own Terms and Conditions.
                  </p> <div><p>
                      Link to Terms and Conditions of third party service providers
                      used by the app
                    </p> <ul><li><a href="https://policies.google.com/terms" target="_blank">Google Play Services</a></li><!----><li><a href="https://firebase.google.com/terms/analytics" target="_blank">Google Analytics for Firebase</a></li><li><a href="https://firebase.google.com/terms/crashlytics" target="_blank">Firebase Crashlytics</a></li><li><a href="https://www.facebook.com/legal/terms/plain_text_terms" target="_blank">Facebook</a></li><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----></ul></div> <p>
                    You should be aware that there are certain things that
                    oneapp technologies will not take responsibility for.
                    Certain functions of the app will require the app to have an
                    active internet connection. The connection can be Wi-Fi, or
                    provided by your mobile network provider, but
                    oneapp technologies cannot take responsibility for the
                    app not working at full functionality if you don’t have
                    access to Wi-Fi, and you don’t have any of your data
                    allowance left.
                  </p> <p></p> <p>
                    If you’re using the app outside of an area with Wi-Fi, you
                    should remember that your terms of the agreement with your
                    mobile network provider will still apply. As a result, you
                    may be charged by your mobile provider for the cost of data
                    for the duration of the connection while accessing the app,
                    or other third party charges. In using the app, you’re
                    accepting responsibility for any such charges, including
                    roaming data charges if you use the app outside of your home
                    territory (i.e. region or country) without turning off data
                    roaming. If you are not the bill payer for the device on
                    which you’re using the app, please be aware that we assume
                    that you have received permission from the bill payer for
                    using the app.
                  </p> <p>
                    Along the same lines, oneapp technologies cannot always
                    take responsibility for the way you use the app i.e. You
                    need to make sure that your device stays charged – if it
                    runs out of battery and you can’t turn it on to avail the
                    Service, oneapp technologies cannot accept
                    responsibility.
                  </p> <p>
                    With respect to oneapp technologies’s responsibility for
                    your use of the app, when you’re using the app, it’s
                    important to bear in mind that although we endeavour to
                    ensure that it is updated and correct at all times, we do
                    rely on third parties to provide information to us so that
                    we can make it available to you.
                    oneapp technologies accepts no liability for any loss,
                    direct or indirect, you experience as a result of relying
                    wholly on this functionality of the app.
                  </p> <p>
                    At some point, we may wish to update the app. The app is
                    currently available on Android – the requirements for
                    system (and for any additional systems we
                    decide to extend the availability of the app to) may change,
                    and you’ll need to download the updates if you want to keep
                    using the app. oneapp technologies does not promise that
                    it will always update the app so that it is relevant to you
                    and/or works with the Android version that you have
                    installed on your device. However, you promise to always
                    accept updates to the application when offered to you, We
                    may also wish to stop providing the app, and may terminate
                    use of it at any time without giving notice of termination
                    to you. Unless we tell you otherwise, upon any termination,
                    (a) the rights and licenses granted to you in these terms
                    will end; (b) you must stop using the app, and (if needed)
                    delete it from your device.
                  </p> <p><strong>Changes to This Terms and Conditions</strong></p> <p>
                    We may update our Terms and Conditions
                    from time to time. Thus, you are advised to review this page
                    periodically for any changes. We will
                    notify you of any changes by posting the new Terms and
                    Conditions on this page. These changes are effective
                    immediately after they are posted on this page.
                  </p> <p><strong>Contact Us</strong></p> <p>
                    If you have any questions or suggestions about
                    our Terms and Conditions, do not hesitate to
                    contact us at @supesiapp on twitter or email us admin[at]Fastee.com.
                  </p> <p>
                    This Terms and Conditions page was generated by
                    <a href="https://app-privacy-policy-generator.firebaseapp.com/" target="_blank">App Privacy Policy Generator</a></p>
<h2>
    For partners
</h2>
<p>
    The following terms and conditions (the “Terms”) govern your access and/or use of any of the applications, websites, content, products, software and services (the “Services”) made available by Oneapp Technologies Limited and its subsidiaries, representatives, affiliates, officers and directors (collectively “Oneapp” or the "APP") to you the partner (collectively "PARTNER"). 
</p>
<p>
BY ENTERING THIS AGREEMENT, YOU EXPRESSLY ACKNOWLEDGE THAT YOU HAVE READ AND UNDERSTAND ALL OF THE TERMS AND HAVE TAKEN TIME TO CONSIDER THE CONSEQUENCES.
</p>
<p>
    By accessing, using or receiving any Services made available by Oneapp, you expressly acknowledge and confirm your agreement to be bound by these Terms. If you do not agree to these Terms, you may not access or use the Services. Oneapp may cease offering or deny access to the Services or any portion thereof, to you at any time for any reason. 

Oneapp reserves the right to amend the Terms or its policies relating to the Services at any time, without notice to you. Any such amendment will be effective upon posting of the updated Terms on the Services, and you are solely responsible for regularly reviewing the same. Your continued access or use of the Services after any such changes shall constitute your agreement to be bound by such Terms as amended.
</p>

<h3>
    The Services
</h3>
<ul>
    <li>The aim of the business terms is to make the products and/or services offered by the
    PARTNER available through the APP.</li>
</ul>
<h3>
    Duration of terms
</h3>
<ul>
    <li>This agreement shall remain in terms for as long the partner is accessing any of the SERVICES. In case either party wants to 
        terminate this agreement they shall write to the contacts given on file. Any monetary balances shall be settled in 3 days.
    </li>
</ul>

<h3>
    Finance
</h3>
<ul>
    <li>
        Oneapp will pay the PARTNER for all applicable sales generated from using the APP
    </li>
    <li>
        The COMMISSION for using the APP shall be 20% of the sale price. Oneapp at its sole discretion may change this percentage at any time.
    </li>
    <li>
        Oneapp shall pay the PARTNER for all the sales generated through the APP,
minus the COMMISSION and any amounts owed by the PARTNER either directly
or indirectly in connection with the use of the APP (hereinafter, the “Amount
Payable”) as provided below: i) For the total amount of sales generated minus
returns, on FRIDAY of every WEEK , Oneapp shall pay the
Amount Payable within a maximum of three (3) business days from the day the withdrawal request is received.
    </li>
    <li>
        In the event of incomplete delivery of any product and/or service for reasons
        attributable to the PARTNER, the PARTNER will be required to bear the full cost
        (including tax) corresponding to the price of the incomplete product and/or service.
        
    </li>
</ul>