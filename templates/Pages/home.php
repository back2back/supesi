<?php
$this->layout = 'anonymous';
?>


<main class="main">
   
   <div class="grid-x">
	  <div class="cell medium-6 large-4">
	  	<p>
	  		Get
	  		<h1 id="app">
	       		{{whois[0]}} 
	    	</h1>
	    	Delivered in 1 hour!
	  	</p>
	  </div>
  	  <div class="cell medium-6 large-8">
  	   <?= $this->Html->image('dish.jpg')?>
  	  </div>
	</div>
	<div class="grid-x">
		<div class="cell">
			<p>
				<h1>Get the app</h1>
				<a href='https://play.google.com/store/apps/details?id=co.supesi.shopping&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' class="google-play" /></a>
			</p>
		</div>
	</div>
</main>
<script>
var app = new Vue({
  el: '#app',
  data() {
    return {
      whois: ['Food', 'Drinks', 'Veggies', '& more']
    }
  },
  mounted(){
    window.setInterval(()=>{
      this.pollPerson();
    }, 2000);

  },
  methods: {
    pollPerson(){
      const first = this.whois.shift();
      this.whois = this.whois.concat(first);
    }
  }
})
</script>
