<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderEntry $orderEntry
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Order Entry'), ['action' => 'edit', $orderEntry->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Order Entry'), ['action' => 'delete', $orderEntry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderEntry->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Order Entries'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Order Entry'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="orderEntries view content">
            <h3><?= h($orderEntry->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $orderEntry->has('user') ? $this->Html->link($orderEntry->user->id, ['controller' => 'Users', 'action' => 'view', $orderEntry->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Tenant') ?></th>
                    <td><?= $orderEntry->has('tenant') ? $this->Html->link($orderEntry->tenant->name, ['controller' => 'Tenants', 'action' => 'view', $orderEntry->tenant->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($orderEntry->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($orderEntry->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($orderEntry->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Orders') ?></h4>
                <?php if (!empty($orderEntry->orders)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Sold') ?></th>
                            <th><?= __('Margin') ?></th>
                            <th><?= __('Qty') ?></th>
                            <th><?= __('Order Entry Id') ?></th>
                            <th><?= __('Product Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($orderEntry->orders as $orders) : ?>
                        <tr>
                            <td><?= h($orders->id) ?></td>
                            <td><?= h($orders->created) ?></td>
                            <td><?= h($orders->modified) ?></td>
                            <td><?= h($orders->sold) ?></td>
                            <td><?= h($orders->margin) ?></td>
                            <td><?= h($orders->qty) ?></td>
                            <td><?= h($orders->order_entry_id) ?></td>
                            <td><?= h($orders->product_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
