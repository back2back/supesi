<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StockRetake $stockRetake
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Stock Retake'), ['action' => 'edit', $stockRetake->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Stock Retake'), ['action' => 'delete', $stockRetake->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stockRetake->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Stock Retakes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Stock Retake'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="stockRetakes view content">
            <h3><?= h($stockRetake->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Stock') ?></th>
                    <td><?= $stockRetake->has('stock') ? $this->Html->link($stockRetake->stock->id, ['controller' => 'Stocks', 'action' => 'view', $stockRetake->stock->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $stockRetake->has('user') ? $this->Html->link($stockRetake->user->id, ['controller' => 'Users', 'action' => 'view', $stockRetake->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($stockRetake->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($stockRetake->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Qty') ?></th>
                    <td><?= $this->Number->format($stockRetake->qty) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reason') ?></th>
                    <td><?= $this->Number->format($stockRetake->reason) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($stockRetake->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($stockRetake->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
