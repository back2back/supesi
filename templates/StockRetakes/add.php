<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StockRetake $stockRetake
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Stock Retakes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="stockRetakes form content">
            <?= $this->Form->create($stockRetake) ?>
            <fieldset>
                <legend><?= __('Add Stock Retake') ?></legend>
                <?php
                    
                    echo $this->Form->control('qty');
                    
                    echo $this->Form->control('reason', ['options' => $foo]);
                    echo $this->Form->control('comment');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
