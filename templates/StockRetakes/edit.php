<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StockRetake $stockRetake
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $stockRetake->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $stockRetake->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Stock Retakes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="stockRetakes form content">
            <?= $this->Form->create($stockRetake) ?>
            <fieldset>
                <legend><?= __('Edit Stock Retake') ?></legend>
                <?php
                    echo $this->Form->control('stock_id', ['options' => $stocks]);
                    echo $this->Form->control('qty');
                    echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('reason');
                    echo $this->Form->control('comment');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
