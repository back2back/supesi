<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StockRetake[]|\Cake\Collection\CollectionInterface $stockRetakes
 */
?>
<div class="stockRetakes index content">
    <?= $this->Html->link(__('New Stock Retake'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Stock Retakes') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('stock_id') ?></th>
                    <th><?= $this->Paginator->sort('qty') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('reason') ?></th>
                    <th><?= $this->Paginator->sort('comment') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($stockRetakes as $stockRetake): ?>
                <tr>
                    <td><?= $this->Number->format($stockRetake->id) ?></td>
                    <td><?= $stockRetake->has('stock') ? $this->Html->link($stockRetake->stock->id, ['controller' => 'Stocks', 'action' => 'view', $stockRetake->stock->id]) : '' ?></td>
                    <td><?= $this->Number->format($stockRetake->qty) ?></td>
                    <td><?= $stockRetake->has('user') ? $this->Html->link($stockRetake->user->id, ['controller' => 'Users', 'action' => 'view', $stockRetake->user->id]) : '' ?></td>
                    <td><?= $this->Number->format($stockRetake->reason) ?></td>
                    <td><?= h($stockRetake->comment) ?></td>
                    <td><?= h($stockRetake->created) ?></td>
                    <td><?= h($stockRetake->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $stockRetake->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $stockRetake->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $stockRetake->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stockRetake->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
