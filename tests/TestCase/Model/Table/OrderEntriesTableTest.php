<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderEntriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderEntriesTable Test Case
 */
class OrderEntriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderEntriesTable
     */
    protected $OrderEntries;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.OrderEntries',
        'app.Users',
        'app.Tenants',
        'app.Orders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderEntries') ? [] : ['className' => OrderEntriesTable::class];
        $this->OrderEntries = TableRegistry::getTableLocator()->get('OrderEntries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OrderEntries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
