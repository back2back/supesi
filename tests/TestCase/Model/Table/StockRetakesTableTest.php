<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StockRetakesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StockRetakesTable Test Case
 */
class StockRetakesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StockRetakesTable
     */
    protected $StockRetakes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.StockRetakes',
        'app.Stocks',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StockRetakes') ? [] : ['className' => StockRetakesTable::class];
        $this->StockRetakes = TableRegistry::getTableLocator()->get('StockRetakes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->StockRetakes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
