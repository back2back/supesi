<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GuestCartsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GuestCartsTable Test Case
 */
class GuestCartsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GuestCartsTable
     */
    protected $GuestCarts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.GuestCarts',
        //'app.Sessions',
        'app.Products',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GuestCarts') ? [] : ['className' => GuestCartsTable::class];
        $this->GuestCarts = TableRegistry::getTableLocator()->get('GuestCarts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GuestCarts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
