<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StoreProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StoreProductsTable Test Case
 */
class StoreProductsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StoreProductsTable
     */
    protected $StoreProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.StoreProducts',
        'app.Products',
        'app.Stores',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StoreProducts') ? [] : ['className' => StoreProductsTable::class];
        $this->StoreProducts = TableRegistry::getTableLocator()->get('StoreProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->StoreProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
