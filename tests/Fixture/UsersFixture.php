<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            //a tenant
            [
                
                'email' => 'foo@foo.com',
                'password' => 'Lorem ipsum dolor sit amet',
                'tenant_id' => 1,
                'token' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-02-04 11:06:35',
                'modified' => '2020-02-04 11:06:35',
                'is_admin' => 0,
                'role' => 0,
                'f_name' => 'First',
                'l_name' => 'Last',
                'phone' => '+254716666666'
            ],

            //admin

            [
                
                'email' => 'foo@foo.com',
                'password' => 'Lorem ipsum dolor sit amet',
                'tenant_id' => 0,
                'token' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-02-04 11:06:35',
                'modified' => '2020-02-04 11:06:35',
                'is_admin' => 1,
                'role' => 0,
                'f_name' => 'First',
                'l_name' => 'Last',
                'phone' => '+254716666309'
            ],
            // normal user
            [
                
                'email' => 'foo@foo.com',
                'password' => 'Lorem ipsum dolor sit amet',
                'tenant_id' => 0,
                'token' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-02-04 11:06:35',
                'modified' => '2020-02-04 11:06:35',
                'is_admin' => 0,
                'role' => 0,
                'f_name' => 'First',
                'l_name' => 'Last',
                'phone' => '+254716666309'
            ],

            // rider
            [
                
                'email' => 'foo@foo.com',
                'password' => 'Lorem ipsum dolor sit amet',
                'tenant_id' => 0,
                'token' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-02-04 11:06:35',
                'modified' => '2020-02-04 11:06:35',
                'is_admin' => 0,
                'role' => 1,
                'f_name' => 'First',
                'l_name' => 'Last',
                'phone' => '+254716666309'

            ],
        ];
        parent::init();
    }
}
