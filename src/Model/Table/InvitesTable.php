<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\LogTrait;

/**
 * Invites Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 *
 * @method \App\Model\Entity\Invite get($primaryKey, $options = [])
 * @method \App\Model\Entity\Invite newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Invite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Invite|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invite saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Invite[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Invite findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvitesTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('invites');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        /*$validator
            ->scalar('token')
            ->maxLength('token', 20)
            ->requirePresence('token', 'create')
            ->notEmptyString('token')
            ->add('token', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);*/

        $validator
            ->integer('is_used')
            ->allowEmptyString('is_used');

        $validator
            ->email('invitee')
            ->maxLength('invitee', 255)
            ->notEmptyString('invitee');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['token']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'));
        $rules->add($rules->isUnique(['invitee']));

        return $rules;
    }
    public function beforeSave($event, $entity, $options)
    {
        if ($entity->isNew()) {

            //ensure email is unique
            $email = $entity->invitee;
            $user = $this->Users->find()->where(['Users.email' => $email])->first();
            
            if ($user != null) {
                return false;
            }
            
            //length should be shorter than chars
            $length = 15;

            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz@#$&*";
            $size = strlen( $chars );

            $str = '';
            for( $i = 0; $i < $length; $i++ ) {  
                $str .= $chars[ rand( 0, $size - 1 ) ];  
                
            }
            $entity->token = $str;
            //todo
            //to implement logic of sending the encoded string to the email provided
            $this->log(base64_encode($str));

            

            return true;
        }
    }

    public function afterSave($event, $entity, $options){
        //length should be shorter than chars
            $length = 15;

            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz@#$&*";
            $size = strlen( $chars );

            $str = '';
            for( $i = 0; $i < $length; $i++ ) {  
                $str .= $chars[ rand( 0, $size - 1 ) ];  
                
            }
            if ($entity->is_used == 0) {
                $this->quickTeamAdd($entity, $str);
            }
            
        
    }

    public function quickTeamAdd($invite, $password){
        $user = $this->Users->newEmptyEntity();
        //put user entities here
            $user->email = $invite->invitee;
            $user->tenant_id = $invite->tenant_id;
            $user->password = $password;
            $user->f_name = $invite->f_name;
            $user->l_name = $invite->l_name;
            $user->phone = $invite->phone;
            $this->log((string) print_r($invite, true));

        if ($this->Users->save($user)) {
            //mark as used
            $foobar = $this->get($invite->id);
            $foobar->is_used = 1;
            $this->save($foobar);
            return true;
        }
        return false;
    }
    public function findToken(Query $query, array $options)
    {
        $invite = $options['invite'];
        $decodedToken = base64_decode($invite);
        //also check if token is used
        return $query->where(['token' => $decodedToken]);
    }

    /**
    * @param invite entity
    * @param user created password
    */
    public function teamMemberAdd($invite, $password){
        $user = $this->Users->newEmptyEntity();
        //put user entities here
            $user->email = $invite->invitee;
            $user->tenant_id = $invite->tenant_id;
            $user->password = $password;

        if ($this->Users->save($user)) {
            //mark as used
            $foobar = $this->get($invite->id);
            $foobar->is_used = 1;
            $this->save($foobar);
            return true;
        }
        return false;
        

    }

    
}
