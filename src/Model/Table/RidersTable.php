<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Riders Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Rider newEmptyEntity()
 * @method \App\Model\Entity\Rider newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Rider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rider get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rider findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Rider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rider[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rider|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rider saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rider[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Rider[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Rider[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Rider[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RidersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('riders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        $validator
            ->integer('created_by')
            ->notEmptyString('created_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
    public function beforeSave($event, $entity, $options){
        if ($entity->isNew()) {
            $entity->is_firebase = 0;
        }
    }
     public function afterSave($event, $entity, $options){
        //create an account
        if ($entity->isNew()) {
            $accountsTable = TableRegistry::getTableLocator()->get('Accounts');
            $account = $accountsTable->newEmptyEntity();
            $account->user_id = $entity->user_id;
            $accountsTable->save($account);
        }
     }

     public function updateStatus($rider,$status){
        switch ($status) {
            //going offline
            case 0:
            $rider->status = 0;
            $this->save($rider);
                return true;
                break;
            //going online
            case 1:
            $rider->status = 1;
            $this->save($rider);
                return true;
                break;
            
            default:
                return false;
                break;
        }
     }
   
    
}
