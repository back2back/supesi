<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use App\Utility\TripCalculator;
use Cake\Log\LogTrait;
use Cake\Collection\Collection;
use Cake\Collection\CollectionInterface;
use Cake\Datasource\ConnectionManager;

/**
 * Stores Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\StoreImagesTable&\Cake\ORM\Association\BelongsTo $StoreImages
 * @property \App\Model\Table\StoreCategoriesTable&\Cake\ORM\Association\HasMany $StoreCategories
 * @property \App\Model\Table\StoreImagesTable&\Cake\ORM\Association\HasMany $StoreImages
 * @property \App\Model\Table\StoreProductsTable&\Cake\ORM\Association\HasMany $StoreProducts
 *
 * @method \App\Model\Entity\Store newEmptyEntity()
 * @method \App\Model\Entity\Store newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Store[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Store get($primaryKey, $options = [])
 * @method \App\Model\Entity\Store findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Store patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Store[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Store|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Store saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Store[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Store[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Store[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Store[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StoresTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stores');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('StoreImages', [
            'foreignKey' => 'store_image_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('StoreCategories', [
            'foreignKey' => 'store_id',
        ]);
        /*$this->hasMany('StoreImages', [
            'foreignKey' => 'store_id',
        ]);*/
        $this->hasMany('StoreProducts', [
            'foreignKey' => 'store_id',
        ]);
        $this->belongsTo('AppCategories', [
            'foreignKey' => 'app_category_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');


        

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['store_image_id'], 'StoreImages'));

        return $rules;
    }
    public function beforeSave($event, $entity, $options){

        
        if ($entity->isNew()) {
            //check if tenant has another store

            /*if (condition) {
                // take prev slug - append name
                $entity->slug = 
            }else{
                //else create slug from name
                $entity->slug = Text::slug (strtolower($entity->name));
            }*/
            
            
            //if (!isset($entity->lat)) {
                $entity->lat = 0.00;
                $entity->lng = 0.00;
                $entity->opening = 0;
                $entity->closing = 0;
                $entity->description = __('No description');
                $entity->app_category_id = 0;
                $entity->is_online = 0;

            //}
        }
        
    }

    public function getDelivery(array $options){
        $userLat = $options['lat'];
        $userLng = $options['lng'];
        $storeId = $options['storeId'];

        $foo = $this->find()
            ->where(['id' => $storeId])
            ->limit(1);
        

        $bar = $foo->extract(function($value){
            return [
                $value->lat,
                $value->lng
            ];
        });
        
            
        $someArray = $bar->toList();
        $storeLat = ($someArray[0][0]);
        $storeLng = $someArray[0][1];
        
        
         $fare = new TripCalculator();
         $booz = $fare->getFare($storeLat, $storeLng, $userLat, $userLng);
         

        //return array($foo, $booz[2]);
         return $booz[2];
        
    }

    public function findStoreCategory(Query $query, array $options){

        $mapper = function ($store, $key, $mapReduce){
            if ($store->app_category_id > 0) {
                $this->log('look');
                $appCategory = $this->AppCategories
                                ->find()
                                ->where(['id' => $store->app_category_id])
                                ->distinct(['id'], true)
                                ->select(['id','name','slug']);
                                
                                //->all();


                $mapReduce->emitIntermediate($appCategory, 'appCategories');
            }
            
            
        };

        $reducer = function($stores, $appCategory, $mapReduce){
            $mapReduce->emit($stores, $appCategory);
        };

        $query = $this->find()->mapReduce($mapper, $reducer);
        /*$query = $this->find()->matching('AppCategories', function($q){
            return $q
            ->group(['AppCategories'])
            ->where(['AppCategories.id' => 1]);
        });*/

        return $query;
    }

    public function getNearMe( array $options){
        $lat = $options['lat'];
        $lng = $options['lng'];

        $data = "
        SELECT
              id, (
                6371 * acos (
                  cos ( radians($lat) )
                  * cos( radians( lat ) )
                  * cos( radians( lng ) - radians($lng) )
                  + sin ( radians($lat) )
                  * sin( radians( lat ) )
                )
              ) AS distance
            FROM stores
            WHERE is_online = 1
            HAVING distance < 10     
        ";

        $connection = ConnectionManager::get('default');
        $q = $connection->execute($data)->fetchAll('assoc');
        

        return $q;
    }

    public function defaultTenantStore(int $tenantId){
        $row =  $this->find()
            ->where(['tenant_id' => $tenantId])
            ->first();
        return $row->id;
    }
}
