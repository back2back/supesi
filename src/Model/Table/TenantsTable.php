<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Cake\I18n\FrozenDate;
use Cake\Log\LogTrait;
use Cake\Utility\Text;

/**
 * Tenants Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\HasMany $Products
 * @property \App\Model\Table\StocksTable&\Cake\ORM\Association\HasMany $Stocks
 * @property \App\Model\Table\TeamsTable&\Cake\ORM\Association\HasMany $Teams
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Tenant get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tenant newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tenant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tenant|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tenant saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tenant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tenant[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tenant findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TenantsTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tenants');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Products', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Stocks', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Teams', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasOne('Subscriptions', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasOne('Stores', [
            'foreignKey' => 'tenant_id',
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    public function beforeSave($event, $entity, $options)
    {
        if ($entity->name ==='') {
            return false;
        }
        if ($entity->isNew()) {
            $name = strtolower($entity->name);

            $entity->name = Text::slug($name);
            # all tenants belong to a country
            $countriesTable = TableRegistry::getTableLocator()->get('Countries');

           /* //countries may be absent
            $data = $countriesTable->find('country', ['data' => 'blank'])
                ->limit(1)
                ->all();

            $this->log((string) $data);*/
           
            $data = $countriesTable->find()->firstOrFail();

            $entity->country_id = $data->id;
        }
        return true;
    }

    public function afterSave($event, $entity, $options)
    {
        if ($entity->isNew()) {
            //get my tenant id
            //owner of a tenant is the admin in the teams
            //this will alter the users table
            $tenantId = $entity->id;
            

            $user = $this->Users->get($entity->user_id);
            $user->tenant_id = $tenantId;
            
            $this->Users->save($user);

            //teams
            $team = $this->Teams->newEmptyEntity();
            $team->user_id = $entity->user_id;
            $team->org_role = 1;
            $team->tenant_id = $tenantId;
            $this->Teams->save($team);

            //subscriptions
            $subEntity = $this->Subscriptions->newEmptyEntity();
            $subEntity->tenant_id = $tenantId;
            $trial = new FrozenTime($entity->created);
            $trial = $trial->addDays(14);
            $this->log((string) $trial);
            $subEntity->expires_on = $trial;

            //todo
            //the amount is 2000 for now. For Kenya.
            $subEntity->amt = 2000;
            //todo
            //assume the new creator is the only seat available
            $subEntity->qty = 1;

            $this->Subscriptions->save($subEntity);

            //create the default store
            $storeEntity = $this->Stores->newEmptyEntity();
            $storeEntity->tenant_id = $tenantId;
            $storeEntity->name = $entity->name;
            $storeEntity->slug = $entity->name;
            $storeEntity->user_id = $entity->user_id;
            $this->Stores->save($storeEntity);

            //create default account
            $accountsTable = TableRegistry::getTableLocator()->get('Accounts');
            $account = $accountsTable->newEmptyEntity();
            $account->user_id = $entity->user_id;
            $accountsTable->save($account);


        }
        
    }

    public function findName(Query $query, array $options){
        $tenantName = $options['tenantName'];
        return $query->where(['name'=> $tenantName]);
    }
}
