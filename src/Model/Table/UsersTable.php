<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;

use Firebase\JWT\JWT;
use Cake\Utility\Security;
use Cake\I18n\FrozenTime;
use Cake\Http\Client;

/**
 * Users Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\HasMany $Products
 * @property \App\Model\Table\StocksTable&\Cake\ORM\Association\HasMany $Stocks
 * @property \App\Model\Table\TeamsTable&\Cake\ORM\Association\HasMany $Teams
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\HasMany $Tenants
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Products', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        $this->hasMany('Stocks', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Teams', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->requirePresence('phone')
            ->alphaNumeric('phone')
            ->minLength('phone', 10)
            ->lengthBetween('phone', [0, 10])

            ->notEmptyString('phone');

        $validator
            ->requirePresence('f_name', 'create')
            ->notEmptyString('f_name');

        $validator
            ->requirePresence('l_name', 'create')
            ->notEmptyString('l_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['phone']));
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'));

        return $rules;
    }

    /**
    * No client user can be saved without a valid tenant id. Admins should be added via mysql
    *
    */
    public function beforeSave($event, $entity, $options)
    {
        
        
        
        if ($entity->isNew()) {

            $phone = $entity->phone;
            $formatted = substr($phone, 1);

           
            //assume all numbers are kenyan
            $entity->phone = '+254'.$formatted;

            if (!isset($entity->role)) {
                $entity->role = 0;
            }
            //an entity with a tenant id has been invited
            if (isset($entity->tenant_id)) {
                return true;
            }
            //check if tenant name is available
            if (isset($entity->tenantName)) {
                
                # code...
                //user creating a business account
                $query = $this->Tenants->find('name', ['tenantName' => $entity->tenantName]);
                if ($query->first()) {
                    # tenant name is already taken
                
                    return false;
                }
            }
            
            return true;
        }
        
       
        
    }

    public function afterSave($event, $entity, $options)
    {
        
        
        if ($entity->isNew()) {
            //get my user id
            $userId = $entity->id;
            //$tenantName = $entity->tenantName;
            

            
            //if tenant id present means im owner
            if ($entity->tenant_id > 0) {

                //teams
                $team = $this->Teams->newEmptyEntity();
                $team->user_id = $userId;
                $team->org_role = 0;
                $team->tenant_id = $entity->tenant_id;
                $this->Teams->save($team);

                //create a token
                $entity->token = $this->jwt($entity->id, $entity->tenant_id);
            
                
                //return;
            }
            if (isset($entity->tenantName)) {
                 $tenant = $this->Tenants->newEmptyEntity();
                $tenant->name = $entity->tenantName;
                $tenant->user_id = $userId;
                $this->Tenants->save($tenant);
               
            }

            if (isset($entity->role) && $entity->role === 1) {
                # creating a rider
                $ridersTable = TableRegistry::getTableLocator()->get('Riders');
                $rider = $ridersTable->newEmptyEntity();
                $rider->user_id = $entity->id;
                $rider->created_by = $entity->created_by;
                $ridersTable->save($rider);
            }
        
        }
 
        
        
    }

    

    
    public function jwt($userId, $tenantId){
        //this function should have an expiry date
        //should we update the jwt everytime the user opens the app?
        if (is_null($userId) || $userId ==='') {
            
        }
        $key = Security::getSalt();

        if ($tenantId > 0) {
            $payload = array(
                "sub" => $userId,
                "exp" => time() + 604800, 
                "tenant_id" => $tenantId
            );
        }else{
            $payload = array(
            "sub" => $userId,
            "exp" => time() + 604800, 
            
            );
        }
        
        
        
        $jwt = JWT::encode($payload,$key);
        
        return $jwt;
    }

}
