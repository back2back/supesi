<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;
use App\Utility\TripCalculator;
use Cake\Datasource\ConnectionManager;
use Cake\Collection\Collection;
use Cake\ORM\Exception\PersistenceFailedException;
use App\Utility\TripCostRounder;

/**
 * Trips Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\OrderEntriesTable&\Cake\ORM\Association\BelongsTo $OrderEntries
 *
 * @method \App\Model\Entity\Trip newEmptyEntity()
 * @method \App\Model\Entity\Trip newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Trip[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Trip get($primaryKey, $options = [])
 * @method \App\Model\Entity\Trip findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Trip patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Trip[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Trip|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Trip saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Trip[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Trip[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Trip[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Trip[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TripsTable extends Table
{
    use LogTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('trips');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('OrderEntries', [
            'foreignKey' => 'order_entry_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        $validator
            ->decimal('start_lat')
            ->requirePresence('start_lat', 'create')
            ->notEmptyString('start_lat');

        $validator
            ->decimal('start_lng')
            ->requirePresence('start_lng', 'create')
            ->notEmptyString('start_lng');

        $validator
            ->decimal('end_lat')
            ->requirePresence('end_lat', 'create')
            ->notEmptyString('end_lat');

        $validator
            ->decimal('end_lng')
            ->requirePresence('end_lng', 'create')
            ->notEmptyString('end_lng');

        $validator
            ->integer('amount')
            ->notEmptyString('amount');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->requirePresence('comment', 'create')
            ->notEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        //removed rule because initially no user
        //$rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['order_entry_id'], 'OrderEntries'));

        //removed this rule to cater for multi trips. TODO
        //$rules->add($rules->isUnique(['order_entry_id']));

        return $rules;
    }

 
    public function afterSave($event, $entity, $options){
        // after an edit check all orders and add to respective accounts
        if (!$entity->isNew()) {
            //check status be aware of deliveryOnly and store pick up!!!
            
            //if trip complete debit store owner and rider accounts
            /* if ($entity->status == 1) {
                $this->debitAccounts($entity);
                
            } */
            
            if ($entity->status == 1) {
                $this->debitAccountsDeliveryOnly($entity);
                
            } 

            // if trip has been assigned a user, notify order entry status
            if ($entity->user_id > 0) {
                $this->notifyOrderEntry($entity);
            }
        }

    }

    protected function notifyOrderEntry($entity){
        $orderEntryEntity = $this->OrderEntries->get($entity->order_entry_id);
        $orderEntryEntity->status = 4;
        $this->OrderEntries->save($orderEntryEntity);
    }

    public function debitAccounts($entity){
        $conn = ConnectionManager::get('default');
        $conn->begin();
        //get Rider id
        $riderId = $entity->user_id;
        //get rider amount
        $riderAmount = $entity->amount;

        //get storeOwner id tenant id
        $orderEntryEntity = $this->OrderEntries->get($entity->order_entry_id);
        $tenantAmount = $orderEntryEntity->amount;

        $tenantId = $orderEntryEntity->tenant_id;
        $tenantsTable = TableRegistry::getTableLocator()->get('Tenants');
        $tenant = $tenantsTable->get($tenantId);
        $storeOwnerId = $tenant->user_id;

        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $riderAccount = $accountsTable->find()->where(['Accounts.user_id' => $riderId])->first();
        $prevRiderAmount = $riderAccount->amount;
        $currRiderAmount = $prevRiderAmount + $riderAmount;
        $riderAccount->amount = $currRiderAmount;
        
        $tenantAccount = $accountsTable->find()->where(['Accounts.user_id' => $storeOwnerId])->first();
        $prevTenantAmount = $tenantAccount->amount;
        $currTenantAmount = $prevTenantAmount + $tenantAmount;
        $tenantAccount->amount = $currTenantAmount;
       


        if ($accountsTable->save($riderAccount) 
            && $accountsTable->save($tenantAccount)) {
            $conn->commit();
            
        }else{
            $conn->rollback();
            
        }
    }
    public function readyTrip($orderEntryEntity){

        $fare = new TripCalculator();

        $stores = TableRegistry::getTableLocator()->get('Stores');
        $storeData = $stores->get($orderEntryEntity->store_id);
        // trips are placed with no users
        $trip = $this->newEmptyEntity();
        $trip->user_id = 0;
        $trip->status = 0;
        //eg store location
        $trip->start_lat = $storeData->lat;
        $trip->start_lng = $storeData->lng;
        $trip->end_lat = $orderEntryEntity->end_lat;
        $trip->end_lng = $orderEntryEntity->end_lng;

        $fare = $fare->getFare(
            $trip->start_lat,
            $trip->start_lng,
            $trip->end_lat,
            $trip->end_lng
            );

        $fareStatus = $fare[0];
        if ($fareStatus) {
            //place the trip calculate distance and amount later
            $trip->distance = $fare[1];
            $trip->amount = $fare[2];
            $trip->order_entry_id = $orderEntryEntity->id;
            $trip->comment = __('no notes');

            $this->save($trip);
        }
                
    }
    public function multiDelivery($orderEntryEntityId, $tripData){
       
        //save trip data
        $toSaveTrip = new Collection($tripData);
        
        $toSaveTrip->each(function ($value, $key) use ($orderEntryEntityId){
            try {
                
                $orderEntryEntity = $this->OrderEntries->get($orderEntryEntityId);

                $tripRow = $this->newEmptyEntity();
                // note that a trip belongs to assigned rider NOT the requestor so must be zero initially
                //$tripRow->user_id = $orderEntryEntity->user_id;
                $tripRow->user_id = 0;
                $tripRow->status = 0;
                $tripRow->start_lat = $value['start_location']['lat'];
                $tripRow->start_lng = $value['start_location']['lng'];

                $tripRow->end_lat = $value['end_location']['lat'];
                $tripRow->end_lng = $value['end_location']['lng'];

                $tripRow->readable_start = $value['readable_start_address'];
                $tripRow->readable_end = $value['readable_end_address'];

                $bbAmount = (TripCalculator::RATE_PER_M * $value['distance']);
                $tripRow->amount = $bbAmount;
                $tripRow->order_entry_id = $orderEntryEntityId;
                $tripRow->comment = "Multi trip";
                $tripRow->distance = $value['distance'];

                $this->save($tripRow);
                
                
            } catch (PersistenceFailedException $e) {
                $this->log(print_r($e->getEntity(), true));
               
            }
        });
        $distances = $toSaveTrip->extract('distance');
        $niceArray = $distances->toList();
       
        $sum = $distances->reduce(function ($accumulator, $value) {
            return $accumulator + $value;
        }, 0);
        
        $toPatch = $this->OrderEntries->get($orderEntryEntityId);
        $totalAmount = (TripCalculator::MIN_FARE) + ($sum * TripCalculator::RATE_PER_M );
        //get individual trips
        //get min fare
       
        // HACKY !!!!
       
        
        $foo = new TripCostRounder();
        $toPatch->amount = $foo->roundOff($totalAmount);
        $this->OrderEntries->save($toPatch);
        return true;
    }

    public function debitAccountsDeliveryOnly($entity){
        $conn = ConnectionManager::get('default');
        $conn->begin();
        //get Rider id
        $riderId = $entity->user_id;
        //get rider amount
        $riderAmount = $entity->amount;

        

        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $riderAccount = $accountsTable->find()->where(['Accounts.user_id' => $riderId])->first();
        $prevRiderAmount = $riderAccount->amount;
        $currRiderAmount = $prevRiderAmount + $riderAmount;
        $riderAccount->amount = $currRiderAmount;
        
        
       


        if ($accountsTable->save($riderAccount)) {
            $conn->commit();
            
        }else{
            $conn->rollback();
            
        } 
    }
}
