<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\LogTrait;
use Cake\Core\Configure;

/**
 * StockRetakes Model
 *
 * @property \App\Model\Table\StocksTable&\Cake\ORM\Association\BelongsTo $Stocks
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\StockRetake newEmptyEntity()
 * @method \App\Model\Entity\StockRetake newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\StockRetake[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StockRetake get($primaryKey, $options = [])
 * @method \App\Model\Entity\StockRetake findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\StockRetake patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StockRetake[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\StockRetake|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StockRetake saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StockRetake[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StockRetake[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\StockRetake[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StockRetake[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StockRetakesTable extends Table
{
    use LogTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stock_retakes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Stocks', [
            'foreignKey' => 'stock_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('qty')
            ->notEmptyString('qty');

        $validator
            ->integer('reason')
            ->notEmptyString('reason');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->requirePresence('comment', 'create')
            ->notEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['stock_id'], 'Stocks'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
    
    public function beforeSave($event, $entity, $options){
        return true;

    }
    public function afterSave($event, $entity, $options){
        $stock = $this->Stocks->get($entity->stock_id);
        $stock->track = 1;

        //  adjust stocks table accordingly
        switch ($entity->reason) {
            case 0:
                # receive stock
                $oldStock = $stock->niceCounted;
                
                $current = $oldStock + $entity->qty;
                
                $stock->counted = $current;
                $this->Stocks->save($stock);
                break;
                
            case 1:
                # recount
                $stock->counted = $entity->qty;
                $this->Stocks->save($stock);
                break;

            case 2:
                # theft
                $oldStock = $stock->niceCounted;
                $current = $oldStock - $entity->qty;
                $stock->counted = $current;
                $this->Stocks->save($stock);
                break;
            
            
        }

    }
}
