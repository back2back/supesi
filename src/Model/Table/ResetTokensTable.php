<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\LogTrait;
use Cake\Utility\Text;
use Cake\I18n\FrozenTime;
use Cake\Event\Event;
use App\Event\EmailListener;

/**
 * ResetTokens Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ResetToken newEmptyEntity()
 * @method \App\Model\Entity\ResetToken newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken get($primaryKey, $options = [])
 * @method \App\Model\Entity\ResetToken findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ResetToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ResetToken saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ResetTokensTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('reset_tokens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        

        $validator
            ->allowEmptyString('is_valid');

        $validator
            ->dateTime('expires')
            ->allowEmptyDateTime('expires');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
    public function beforeSave($event, $entity, $options)
    {
        //$this->log((string) $entity->email);
        if ($entity->isNew()) {
            $barz = $this->Users->findByEmail($entity->email)->first();

            
            $longToken = Text::uuid();
            $entity->token = $longToken;

            $timeNow = FrozenTime::now();
            $future = $timeNow->addDays(2);
            $entity->expires = $future;

            $entity->user_id = $barz->id;

            return true;
        }
       
    }
    public function afterSave($event, $entity, $options)
    {
        //fire email event
        if ($entity->isNew()) {
            //$userEntity = $this->Users->get($entity->user_id);

            $userEntity = $this->Users->findByEmail($entity->email)->first();

           

            $event = new Event('Model.Email.ResetToken', $this, [
                        'token' => $entity->token,
                        'reset' => $userEntity]);
                    $this->getEventManager()->dispatch($event);

        }
    }

    public function findToken(Query $query, array $options){
        $resetToken = $options['token'];
        
        return $query->find('all')
            ->where(['ResetTokens.token' => $resetToken, 'ResetTokens.is_valid' => 1])
            ->contain(['Users'])
            ->limit(1);
            //->firstOrFail();
    }
    public function updateToken($data, $updated){
        
        //is valid = 0
        //load users table update

        $query = $this->find()
                ->where(['token' => $data->token]);

        $rowData = $query->first();
        $rowData->is_valid = 0;

        
        $userData = $this->Users->get($data->user_id);
        $userData->password = $updated;

        //debug($updated);
        if ($this->save($rowData) && $this->Users->save($userData)) {


            
            # code...
            return true;
        }
        return false;
        

    }
}
