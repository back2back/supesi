<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Cake\I18n\FrozenDate;
use Cake\Log\LogTrait;
use Cake\Collection\Collection;
use Cake\Event\Event;
use App\Utility\TripCostRounder;


/**
 * OrderEntries Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\OrderEntry get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderEntry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderEntry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderEntry|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderEntry saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderEntry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderEntry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderEntry findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderEntriesTable extends Table
{
    //default status 0 - offline stores
    // status will change depending on what's going on with the request
    // 1 - from online, waiting for store action
    // 2 - accepted
    // 3 - ready for dispatch
    // 4 - en route
    // 5 - complete
    // 6 - canceled

    use LogTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('order_entries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'order_entry_id',
        ]);
        $this->belongsTo('Stores', [
            'foreignKey' => 'store_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Trips', [
            'foreignKey' => 'order_entry_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'));

        return $rules;
    }
    public function afterSave($event, $entity, $options)
    {
    
        //this code conflicts with data from online multi delivery. So will uncomment for now
        /* if ($entity->status ===3) {
            $this->createTrip($entity);
        } */
        
    }
    

    public function orderStatus($entity){
        //watch for states
        $status = $entity->status;
        //possibly notify parties
        switch ($status) {
            case 1:
                $event = new Event('Model.OrderEntries.online', $this, [
                            //'product' => $product,
                            'orderEntry' => $entity]);
                        $this->getEventManager()->dispatch($event);
                        $this->log('order entry status 1');
                break;

            
            default:
                # do nothing...
            $this->log('Warning order entry has no action set');
                break;
        }
        
    }

    public function orderListener($orderEntryId){
        //this function might be called multiple times
        $entity = $this->get($orderEntryId);
        $oldAmount = $entity->amount ;
        $currentAmount = 0;

        $query = $this->Orders->find()->where(['Orders.order_entry_id' => $orderEntryId]);
        foreach($query as $data){
            $currentAmount += $data->total;
        }

        $entity->amount = $currentAmount;
        $this->save($entity);

        //do any notifications after we have total
        $this->orderStatus($entity);
    
    }

    public function createTrip($entity){
        //go to trips table with this entity data
        $tripsTable = TableRegistry::getTableLocator()->get('Trips');
        $tripsTable->readyTrip($entity);
    }
    
    public function interval($interval){
        //1 = today 
        //2 = this month
        //3 = last month

        $time =  FrozenTime::now();
        
        switch ($interval) {
            case 1:
                $startOfDay = $time->startOfDay();
                $endOfDay = $time->endOfDay();
                $query = $this->find('all')
                    ->where(['OrderEntries.created >' => $startOfDay, 'OrderEntries.created <' => $endOfDay])
                    ->contain('Orders');
                return $query;
                break;

            case 2:
                $startOfMonth = $time->startOfMonth();
                $endMonth = $time->endOfMonth();

                
                $query = $this->find('all')
                    ->where(['OrderEntries.created >' => $startOfMonth, 'OrderEntries.created <' => $endMonth])
                    ->contain('Orders');

                return $query;
                break;
            
            default:
                $this->forever();
                break;
        }       
    }

    public function highMovingProduct($query){
        $collection = collection($query);
        $orders = $collection->extract('orders');
       
        //$collectionB = collection($orders);
        $collectionB = $orders->each(function ($value, $key) {
            //$this->log((string) print_r($value,  true));
            foreach($value as $order){
                return $order;
            }
            //$this->log((string) print_r($order->id,  true));
        });
        return $collectionB->unfold()->toList();
       
    }

    public function orderValue($query){
        $collection = new Collection($query);
       
        $data = $collection->extract('orders'); 

        $mmm = $data->map(function($value, $key){
            $totalz = 0;
            //array of orders
            $sm = new Collection($value);
            $so = $sm->map(function($v, $k )use ($totalz){
                
                //return  $totalz+=$v->total;
                return $v;
            });
           
            return $so->toArray();
        });
       
        
        return $mmm;
        
        /*$new = $collectionB->each(function ($value, $key) {
            return  $key.':'.$value;
        });
        $new = $new->toList();
        $this->log((string) print_r($new, true));

        
        return $new;*/
    }
    protected function forever(){
        $query = $this->find('all');
        return $query;
    }
}
