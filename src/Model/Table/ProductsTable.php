<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\LogTrait;
use Authentication\AuthenticationService;
use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;

/**
 * Products Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\StocksTable&\Cake\ORM\Association\HasMany $Stocks
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'product_id',
        ]);
        $this->hasOne('Stocks', [
            'foreignKey' => 'product_id',
        ]);

        $this->hasMany('GuestCarts', [
            'foreignKey' => 'product_id',
        ]);
        $this->belongsTo('ProductFactory', [
            'foreignKey' => 'product_factory_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductImages', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        //todo title is not validated if not empty
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'));

        $rules->add($rules->isUnique(['tenant_id','title']));

        return $rules;
    }

    public function beforeSave($event, $entity, $options){
        if ($entity->isNew()) {
            //check if from factory
            if (!$entity->from_factory) {
                $validator = new Validator();
                $validator
                ->scalar('title')
                ->maxLength('title', 255)
                ->requirePresence('title')
                ->notEmptyString('title');
            }
        }
        $entity->price_in = $entity->nice_price_in;
        $entity->price_out = $entity->nice_price_out;
        
        return true; 
    }
    public function afterSave($event, $entity, $options)
    {
        //create stocks on new products

        //todo
        //restocking 
        if ($entity->isNew()) {
            $productId = $entity->id;

            $this->toStore($entity->id, $entity->user_id);

            $stock = $this->Stocks->newEmptyEntity();
            $stock->product_id = $productId;

            $stock->tenant_id = $entity->tenant_id;
            $stock->user_id = $entity->user_id;

            if ($entity->stock_count) {
                
                $stock->counted = $entity->stock_count;
            }
            
            
            if($this->Stocks->save($stock)){
                return true;
            }else{
                return false;
            }
            
        }
    }

    public function toStore($productId, $userId){
        //get my store
        //by default user have only one store
        $storesTable = TableRegistry::getTableLocator()->get('Stores');
        $firstStore = $storesTable->find()->where(['Stores.user_id' => $userId])->first();

        // register product in store_products
        $storeProducts = TableRegistry::getTableLocator()->get('StoreProducts');
        $storeProductEntity = $storeProducts->newEmptyEntity();
        $storeProductEntity->product_id = $productId;
        $storeProductEntity->user_id = $userId;
        $storeProductEntity->store_id = $firstStore->id;

        $storeProducts->save($storeProductEntity);
    }

    public function findcheckFactory(Query $query, array $options)
    {
        
        //return $query->contain(['ProductFactory']);
        //$iterator = $query->all();

        

       /* $simpleCollection = new Collection($query);

        $new = $simpleCollection->map(function ($value, $key) {
            $daga = $value['from_factory'];
            $this->log((string) 'val '.$daga);
            if ($daga === 1) {
                # get factory
                return $this->ProductFactory->get($value['product_factory_id']);
            }
        });

        $result = $new->toArray();
        //$this->log((string) print_r($result, true));*/

        /*return $query
            ->group(['product_factory_id'])
            ->contain(['ProductFactory'])
            ->where(['from_factory' => 1]);*/

        $query->formatResults(function ($results){
            return $results->map(function($row){
                $row['product_factory'] = $this->ProductFactory->findById(5)->first();
                return $row;
            });
        });

        return $query;
    }

    

    /*public function afterDelete($event, $entity, $options){
        $this->log((string) $entity->id);
    }*/
}
