<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\LogTrait;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use App\Utility\ShortRandomString;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;
use Josegonzalez\Upload\File\Transformer\DefaultTransformer;
use Cake\Core\Configure;

//use Josegonzalez\Upload\File\Path\Filename\DefaultTrait;

/**
 * ProductImages Model
 *
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\ProductImage newEmptyEntity()
 * @method \App\Model\Entity\ProductImage newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ProductImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProductImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProductImage findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ProductImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProductImage[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProductImage|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductImage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductImage[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ProductImage[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ProductImage[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ProductImage[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductImagesTable extends Table
{
    use LogTrait;
    protected $adapter;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('product_images');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $client = new S3Client([
            'credentials' => [
                'key'    => Configure::read('FileStorage.key'),
                'secret' => Configure::read('FileStorage.secret'),
            ],
            'region' => Configure::read('FileStorage.region'),
            'version' => 'latest',
            'endpoint' => Configure::read('FileStorage.endpoint'),
        ]);

        $adapter = new \League\Flysystem\AwsS3v3\AwsS3Adapter(
            $client,
            Configure::read('FileStorage.bucket-name')
            
        );
        $this->adapter = $adapter;

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'name' => [
                'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    'dir' => 'dir', // defaults to `dir`
                    'size' => 'size', // defaults to `size`
                    'type' => 'type', // defaults to `type`
                ],
                'filesystem' => [
                    'adapter' => $adapter,
                ],
                'path' => 'webroot{DS}files{DS}{model}',
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {

                    $pathParts = pathinfo($data->getClientFilename());
                    $short = new ShortRandomString();
                    $short = $short->random(5);
                    $filename = $short . '.' . strtolower($pathParts['extension']);

                    return $filename;

                 },
                 
                 
                
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        //$path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false

                
            ],
        ]);

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->setProvider('upload', \Josegonzalez\Upload\Validation\DefaultValidation::class);
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->add('name', 'fileType', [
                'rule' => ['extension', ['jpg','png','jpeg'] ],
                'message' => 'This file type is incorrect']);

        $validator->add('name', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 512],
            'message' => 'This image should at least be 512px high',
            'provider' => 'upload'
        ]);

        $validator->add('name', 'fileAboveMaxHeight', [
            'rule' => ['isBelowMaxHeight', 514],
            'message' => 'This image should at most be 512px high',
            'provider' => 'upload'
        ]);

        $validator->add('name', 'fileAboveMaxWidth', [
            'rule' => ['isBelowMaxWidth', 514],
            'message' => 'This image should at most be 512px wide',
            'provider' => 'upload'
        ]);

        $validator->add('name', 'fileAboveMinWidth', [
            'rule' => ['isAboveMinWidth', 512],
            'message' => 'This image should at least be 512px wide',
            'provider' => 'upload'
        ]);
        $validator->add('name', 'fileBelowMaxSize', [
            'rule' => ['isBelowMaxSize', 5000000],
            'message' => 'This file is too large',
            'provider' => 'upload'
        ]);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
    public function beforeDelete($event, $entity, $options)
    {
        

        $filesystem = new Filesystem($this->adapter);

        $check = $filesystem->has($entity->dir.'/'.$entity->name);
        

        $response = $filesystem->delete($entity->dir.'/'.$entity->name);

        return $response;
    }
}
