<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\Log\LogTrait;
use Cake\ORM\TableRegistry;

/**
 * Countries Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Country get($primaryKey, $options = [])
 * @method \App\Model\Entity\Country newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Country[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Country|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Country saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Country patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Country[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Country findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CountriesTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('countries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('alpha3')
            ->maxLength('alpha3', 3)
            ->requirePresence('alpha3', 'create')
            ->notEmptyString('alpha3')
            ->add('alpha3', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 64)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('timezone')
            ->maxLength('timezone', 64)
            ->requirePresence('timezone', 'create')
            ->notEmptyString('timezone');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['alpha3']));
        $rules->add($rules->isUnique(['name']));
        

        return $rules;
    }

    //no countries. Get default

    public function findDefault(){
        $data = Configure::read('Countries');
        

        $country = $data;

        $alpha3 = $country['alpha3'];
        $name = $country['name'];
        $userId = $country['user_id'];
        $timezone = $country['timezone'];
        $currCode = $country['curr_code'];


        $countryEntity = $this->newEmptyEntity();
        $countryEntity->alpha3 = $alpha3;
        $countryEntity->name = $name;
        $countryEntity->user_id = $userId;
        $countryEntity->timezone = $timezone;
        $countryEntity->curr_code = $currCode;

        if($this->save($countryEntity)){
            return true;
        }
        return false;

      
    }

    public function findCountry(Query $query, array $options){
        $query = $this->find();
        $total = $query->count();

        if ($total == 0) {
            $this->findDefault();
        }
        return $query;
    }
}
