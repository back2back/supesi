<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;
use Cake\Event\Event;
use App\Event\EmailListener;

/**
 * Checkouts Model
 *
 * @property \App\Model\Table\SessionsTable&\Cake\ORM\Association\BelongsTo $Sessions
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Checkout newEmptyEntity()
 * @method \App\Model\Entity\Checkout newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Checkout[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Checkout get($primaryKey, $options = [])
 * @method \App\Model\Entity\Checkout findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Checkout patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Checkout[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Checkout|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Checkout saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Checkout[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Checkout[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Checkout[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Checkout[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CheckoutsTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('checkouts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->addBehavior('Timestamp');

        $this->belongsTo('Sessions', [
            'foreignKey' => 'session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        //$rules->add($rules->existsIn(['session_id'], 'Sessions'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function cleanUp($identity, $session){
        //find session data
        $tableGuestCart = TableRegistry::getTableLocator()->get('GuestCarts');

        $query = $tableGuestCart->find('all')
            ->contain(['Products'])
            //->group(['session_id'])
            ->where(['session_id' => $session]);


        return $query;
    }

    public function afterSave($event, $entity, $options){
        // notify tenant
        $customerId = $entity->user_id;
        $sessionId = $entity->session_id;

        //a unique session can have many items just fetch one row.
        $tableGuestCart = TableRegistry::getTableLocator()->get('GuestCarts');
        $query = $tableGuestCart->find('all')
            ->contain(['Products'])
            ->where(['session_id' => $sessionId])
            ->limit(1);

        

        foreach($query as $data){
            $productId = $data->product->id;
           

            $event = new Event('Model.Checkout.alertTenant', $this, [
                        //'product' => $product,
                        'checkout' => $data]);
                    $this->getEventManager()->dispatch($event);
        }

       

    }
}
