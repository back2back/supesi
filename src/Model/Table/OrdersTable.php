<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Cake\Event\Event;

/**
 * Orders Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('OrderEntries', [
            'foreignKey' => 'order_entry_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }

    public function beforeSave($event, $entity, $options){
        
        return true;
    }

    
    public function afterSave($event, $entity, $options)
    {
        
        //decrease the stock count
        $productId = $entity->product_id;

        //quantity in normal form ie not 1000th
        $quantity = $entity->qty;

        //check product in stocks
        $stocksTable = TableRegistry::getTableLocator()->get('Stocks');
        $query = $stocksTable->find('product', ['product' => $this->Products->get($productId)]);
        $row = $query->first();
        //check if we tracking stock
        if ($row) {      
            $counted = $row->niceCounted;
            if ($counted > 0) {

                $this->log('fooz');

                //subtract the qty
                $updated = $counted - $quantity;
                $row->counted = $updated; 
                $stocksTable->save($row);
            }
            
        }
    }

    

    public function orderEntryTransaction(array $products, $identity, $storeId  ){
        
        $tenantId= $identity->getOriginalData()->tenant_id;
        $userId = $identity->getOriginalData()->id;
        //create an order entry
        $orderEntry = $this->OrderEntries->newEmptyEntity();

        $io =array();
        
        foreach($products as $myData){
                 
                 
                 $foo['product_id'] = $myData['product_id'];
                 $foo['qty'] = $myData['qty'];
                 

                 $productEntity = $this->Products->get($myData['product_id']);
                 $foo['sold'] = $productEntity->price_out;
                 $foo['margin'] = ($productEntity->price_out) - ($productEntity->price_in);
                 $foo['Product'] = $productEntity;
                 
                 //do not set user id or tenant id. already set in order entry table
                 array_push($io, $foo);
        }
        //check if user is a tenant for the product
        foreach($io as $zaga){
            $pr = $zaga['Product'];
            

            if ($tenantId != $pr->tenant_id) {
                 return false;
             } 
        }
            



        $orderEntry->tenant_id = $tenantId;
        $orderEntry->user_id = $userId;
        $orderEntry->store_id = $storeId;

        if (array_key_exists('prepared_by', $products['0'])) {
            $orderEntry->prepared_by = $products['0']['prepared_by'];
        }else{
            $orderEntry->prepared_by = $userId;
        }

        if (array_key_exists('payment_type', $products['0'])) {
            $orderEntry->payment_type = $products['0']['payment_type'];
            
        }else{
            $orderEntry->payment_type = 0;
            
        }

        // check if customer id attached

        if (array_key_exists('customer_id', $products['0'])) {
            $orderEntry->customer_id = $products['0']['customer_id'];
        }
        
        $orderEntry->end_lat = 0.0;
        $orderEntry->end_lng = 0.0;

        $conn = ConnectionManager::get('default');
        $conn->begin();

        //save the orderEntry first, later the orders below
        if($this->OrderEntries->save($orderEntry)){
            
            //get the order entry id
            $orderEntryId = $orderEntry->id;
            
            $val = array();

            //saving the orders
            foreach($products as $myData){
                 $foo['product_id'] = $myData['product_id'];
                 $foo['qty'] = $myData['qty'];
                 $foo['order_entry_id'] = $orderEntryId;

                 $productEntity = $this->Products->get($myData['product_id']);
                 $foo['sold'] = $productEntity->price_out;
                 $foo['margin'] = ($productEntity->price_out) - ($productEntity->price_in);
                 
                 //do not set user id or tenant id. already set in order entry table
                 array_push($val, $foo);
            }

            
            
            $pp = $this->newEntities($val);
           
            if ($this->saveMany($pp)) {
                
                $conn->commit();
                 return array(true, $products);
            }else{
                $conn->rollback();
                
                return false;
            }
        }

    }

    public function worldOnlineTransact(array $products, $identity , $storeId ){
        
        // world users can create orders
        $userId = $identity->getOriginalData()->id;
        //create an order entry
        $orderEntry = $this->OrderEntries->newEmptyEntity();
        
        
        $orderEntry->store_id = $storeId;
        $orderEntry->status = 1;
        $orderEntry->end_lat = $products['0']['end_lat'];
        $orderEntry->end_lng = $products['0']['end_lng'];


       

       

        //we need a tenant to save so get tenant data from store
        $storesTable = TableRegistry::getTableLocator()->get('Stores');
        $storeData = $storesTable->find()->where(['Stores.id' => $storeId])->first();

        $orderEntry->tenant_id = $storeData->tenant_id;
        
        $io =array();
        
        foreach($products as $myData){
                 
                 
                 $foo['product_id'] = $myData['product_id'];
                 $foo['qty'] = $myData['qty'];
                 

                 $productEntity = $this->Products->get($myData['product_id']);
                 $foo['sold'] = $productEntity->price_out;
                 $foo['margin'] = ($productEntity->price_out) - ($productEntity->price_in);
                 $foo['Product'] = $productEntity;
                 
                 //do not set user id or tenant id. already set in order entry table
                 array_push($io, $foo);
        }
        
        $orderEntry->user_id = $userId;

        $conn = ConnectionManager::get('default');
        $conn->begin();

        //save the orderEntry first, later the orders below
        if($this->OrderEntries->save($orderEntry)){
            
            //get the order entry id
            $orderEntryId = $orderEntry->id;
            
            $val = array();

            //saving the orders
            foreach($products as $myData){
                 $foo['product_id'] = $myData['product_id'];
                 $foo['qty'] = $myData['qty'];
                 $foo['order_entry_id'] = $orderEntryId;

                 $productEntity = $this->Products->get($myData['product_id']);
                 $foo['sold'] = $productEntity->price_out;
                 $foo['margin'] = ($productEntity->price_out) - ($productEntity->price_in);
                 
                 //do not set user id or tenant id. already set in order entry table
                 array_push($val, $foo);
            }

            
            
            $pp = $this->newEntities($val);
            
            if ($this->saveMany($pp)) {
                $this->OrderEntries->orderListener($orderEntryId);
                $conn->commit();
                
                 return array(true, $products, $orderEntry);
            }else{
                $conn->rollback();
                
                return false;
            }

           
        }

    }

    

}
