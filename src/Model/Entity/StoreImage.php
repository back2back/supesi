<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StoreImage Entity
 *
 * @property int $id
 * @property int $store_id
 * @property int $user_id
 * @property string $dir
 * @property int $size
 * @property string $type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Store[] $stores
 * @property \App\Model\Entity\User $user
 */
class StoreImage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'store_id' => true,
        'user_id' => true,
        'photo' => true,
        'dir' => true,
        'size' => true,
        'type' => true,
        'created' => true,
        'modified' => true,
        'stores' => true,
        'user' => true,
    ];
}
