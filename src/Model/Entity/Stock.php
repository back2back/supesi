<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Stock Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $tenant_id
 * @property int $product_id
 * @property int $counted
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Product $product
 */
class Stock extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'tenant_id' => true,
        'product_id' => true,
        //'counted' => true,
        //'threshold' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'tenant' => true,
        'product' => true,
    ];

    protected $_virtual = [
        'nice_counted',
        'nice_threshold',
    ];

    protected $_hidden = [
        'counted',
        'threshold',
    ];

    //stock counts should be multiplied by 1000 least divisional unit
    protected function _setCounted($counted){
        return $counted*1000;
    }

    //stock counts to be read and divided by 1000
    protected function _getNiceCounted(){
        
        return $this->counted/1000;
        
    }

    protected function _setThreshold($threshold){
        return $threshold * 1000;
    }
    protected function _getNiceThreshold(){
        return $this->threshold/1000;
    }
    


}
