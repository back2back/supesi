<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $tenant_id
 * @property int $product_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Product $product
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'tenant_id' => true,
        'product_id' => true,
        'created' => true,
        'modified' => true,
        'margin' => true,
        'order_entry_id' => true,
        'qty' => true,
        'sold' => true,
        //'store_id' => true,
        
    ];

    protected $_hidden = [
        'margin',
        'sold',
        'created',
        'modified'
        
    ];

    protected $_virtual = [
        'nice_sold',
        'nice_margin',
        'total',
    ];

    protected function _getNiceSold(){
        return $this->sold/100;
    }
    protected function _getNiceMargin(){
        return $this->margin/100;
    }

    protected function _getTotal(){
        return $this->nice_sold * $this->qty;
    }
}
