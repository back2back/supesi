<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity
 *
 * @property int $id
 * @property int $order_entry_id
 * @property \Cake\I18n\FrozenTime $due_date
 * @property string $message
 * @property int $status
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\OrderEntry $order_entry
 * @property \App\Model\Entity\User $user
 */
class Invoice extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_entry_id' => true,
        'due_date' => true,
        'message' => true,
        'status' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'order_entry' => true,
        'user' => true,
    ];
}
