<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $tenant_id
 * @property string $title
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Stock[] $stocks
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'tenant_id' => true,
        'title' => true,
        'description' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'tenant' => true,
        'orders' => true,
        'stock_count' => true,
        'price_in' => true,
        'price_out' => true,
        'product_factory_id' => true,
        'from_factory' => true,
        //'slug' => true,
        
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'price_in',
        'price_out',
        'created',
        'modified'
        
    ];

    protected $_virtual = [
        'nice_price_out',
        'nice_price_in',
    ];

     //use 100 since its the lowest denomination (cents)
    protected function _setPriceIn($priceIn){
        return $priceIn*100;
    }

    
    protected function _setPriceOut($priceOut){
        return $priceOut*100;
    }

    protected function _getNicePriceIn(){
        return $this->price_in/100;
    }
    protected function _getNicePriceOut(){
        return $this->price_out/100;
    }

}
