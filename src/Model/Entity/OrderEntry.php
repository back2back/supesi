<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Log\LogTrait;
use App\Utility\TripCostRounder;

/**
 * OrderEntry Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $tenant_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Order[] $orders
 */
class OrderEntry extends Entity
{
    use LogTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'tenant_id' => true,
        'created' => true,
        'modified' => true,
        'store_id' => true,
        'status' => true,
        'prepared_by' =>true,
        'payment_type' => true,
        'end_lat' => true,
        'end_lng' => true,
        'amount' => true,
        'user' => true,
        'tenant' => true,
        'orders' => true,
        

    ];

    protected $_virtual = [
        'nice_amount',
    ];

    protected function _setAmount($amount){
        return $amount*100;
    }
    protected function _getNiceAmount(){
        return $this->amount/100;
    }
}
