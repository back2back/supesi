<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Log\LogTrait;
use App\Utility\TripCostRounder;

/**
 * Trip Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $status
 * @property string $start_lat
 * @property string $start_lng
 * @property string $end_lat
 * @property string $end_lng
 * @property int $amount
 * @property int $order_entry_id
 * @property string $comment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\OrderEntry $order_entry
 */
class Trip extends Entity
{
    use LogTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'status' => true,
        'start_lat' => true,
        'start_lng' => true,
        'end_lat' => true,
        'end_lng' => true,
        //'amount' => true,
        'order_entry_id' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'order_entry' => true,
        'readable_start' => true,
        'readable_end' => true
    ];
    protected $_virtual = [
        'nice_amount',
    ];

    protected function _setAmount($amount){
        $rounded = new TripCostRounder();
        $amount = $rounded->roundOff($amount);
           
        return $amount*100;
    }
    protected function _getNiceAmount(){
        return $this->amount/100;
    }
}
