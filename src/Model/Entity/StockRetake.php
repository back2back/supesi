<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StockRetake Entity
 *
 * @property int $id
 * @property int $stock_id
 * @property float $qty
 * @property int $user_id
 * @property int $reason 0 - receive stock 1 - recount 2 Theft
 * @property string $comment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Stock $stock
 * @property \App\Model\Entity\User $user
 */
class StockRetake extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'stock_id' => true,
        'qty' => true,
        'user_id' => true,
        'reason' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,
        'stock' => true,
        'user' => true,
    ];

    
}
