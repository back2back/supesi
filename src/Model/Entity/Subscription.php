<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subscription Entity
 *
 * @property int $id
 * @property int $tenant_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $expires_on
 * @property int $countries_id
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Country $country
 */
class Subscription extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => true,
        'created' => true,
        'modified' => true,
        'expires_on' => true,
        'countries_id' => true,
        'expires_on' => true,
        'amt' => true,
        'qty' => true,
        'tenant' => true,
        'country' => true,
    ];
}
