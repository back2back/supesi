<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Authentication\PasswordHasher\DefaultPasswordHasher;

use Authentication\IdentityInterface;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;


/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property int $tenant_id
 * @property string|null $token
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant[] $tenants
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Product[] $products
 * @property \App\Model\Entity\Stock[] $stocks
 * @property \App\Model\Entity\Team[] $teams
 */
class User extends Entity implements IdentityInterface
{
    use LogTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'tenant_id' => true,
        'f_name' => true,
        'l_name' =>true,
        'phone' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'tenants' => true,
        'orders' => true,
        'products' => true,
        'stocks' => true,
        'teams' => true,
        'tenantName' => true,
        'role' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token'
    ];

    protected function _setPassword(string $password) : ?string
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }

    protected function _getLocalizedTime(){
        $timezone = 'Africa/Nairobi';
        $created = $this->created;
        $time = $created->i18nFormat([\IntlDateFormatter::FULL, \IntlDateFormatter::SHORT], $timezone);
        
        return $time ;
    }

    
    /**
     * Authentication\IdentityInterface method
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * Authentication\IdentityInterface method
     */
    public function getOriginalData()
    {
        return $this;
    }

    public function getTenantId(){
        return $this->tenant_id;
    }

    public function getTeam(){
        $teams = TableRegistry::getTableLocator()->get('Teams');

        $me = $teams->find('all')
            ->where(['Teams.user_id' => $this->id])
            ->limit(1);
        return $me->first();
    }
}
