<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Store Entity
 *
 * @property int $id
 * @property string $name
 * @property int $tenant_id
 * @property int $user_id
 * @property string $slug
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $store_image_id
 * @property string $lat
 * @property string $lng
 * @property \Cake\I18n\FrozenTime $opening
 * @property \Cake\I18n\FrozenTime $closing
 * @property string $description
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\StoreImage[] $store_images
 * @property \App\Model\Entity\StoreCategory[] $store_categories
 * @property \App\Model\Entity\StoreProduct[] $store_products
 */
class Store extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'tenant_id' => true,
        'user_id' => true,
        'slug' => true,
        'created' => true,
        'modified' => true,
        'store_image_id' => true,
        'lat' => true,
        'lng' => true,
        'opening' => true,
        'closing' => true,
        'description' => true,
        'tenant' => true,
        'user' => true,
        'store_images' => true,
        'store_categories' => true,
        'store_products' => true,
        'app_category_id' => true,
        'is_online' => true,
    ];

    
}
