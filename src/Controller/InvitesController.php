<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Invites Controller
 *
 * @property \App\Model\Table\InvitesTable $Invites
 *
 * @method \App\Model\Entity\Invite[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InvitesController extends AppController
{
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['verifyToken']);
        
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            //'contain' => ['Users', 'Tenants'],
        ];
        $user = $this->request->getAttribute('identity');
        $query = $user->applyScope('index', $this->Invites->find());
       

        $this->set('invites', $this->paginate($query));
    }

    /**
     * View method
     *
     * @param string|null $id Invite id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $invite = $this->Invites->get($id, [
            'contain' => ['Users', 'Tenants'],
        ]);

        $this->set('invite', $invite);
    }

    public function verifyToken($token = null)
    {
        $this->Authorization->skipAuthorization();
        $query = $this->Invites->find('token', ['invite' => $token]);

        //throw 404 not found
        $invite = $query->firstOrFail();
        $this->set('invite', $invite);

        
        if ($this->request->is('post')) {
            //mark invite as used
            $added = $this->Invites->teamMemberAdd($invite, $this->request->getData('password'));
            
            if ($added) {
                $this->Flash->success(__('Account created successfully. Please login using your email and password'));
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        
        $userEntity = $this->request->getAttribute('identity')->getOriginalData();
        $invite = $this->Invites->newEmptyEntity();
        $this->Authorization->authorize($invite, 'create');
        $invite->user_id = $userEntity->id;
        $invite->tenant_id = $userEntity->tenant_id;
        if ($this->request->is('post')) {
            $invite = $this->Invites->patchEntity($invite, $this->request->getData());
            
            if ($this->Invites->save($invite)) {
                $this->Flash->success(__('The invite has been saved and sent to the email'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The person invited is a member of another team. Contact support'));
        }
        
        $this->set(compact('invite'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Invite id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $invite = $this->Invites->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($invite, 'update');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $invite = $this->Invites->patchEntity($invite, $this->request->getData());
            if ($this->Invites->save($invite)) {
                $this->Flash->success(__('The invite has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The invite could not be saved. Please, try again.'));
        }
        $users = $this->Invites->Users->find('list', ['limit' => 200]);
        $tenants = $this->Invites->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('invite', 'users', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Invite id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $invite = $this->Invites->get($id);
        if ($this->Invites->delete($invite)) {
            $this->Flash->success(__('The invite has been deleted.'));
        } else {
            $this->Flash->error(__('The invite could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
