<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;
use App\Utility\ShortRandomString;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index', 'view','cart','addToCart']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Products->find());
        

        
        
        $products = $this->paginate($query);
        
        


        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $product = $this->Products->get($id, [
            'contain' => ['Users', 'Tenants', 'Orders', 'Stocks'],
        ]);
        $this->Authorization->skipAuthorization();
        

        $image = $this->Products->ProductImages
                ->find()
                ->where(['ProductImages.product_id' => $id]);

        $this->set('product', $product);
        $this->set('image' , $image);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $product = $this->Products->newEmptyEntity();
        $this->Authorization->authorize($product, 'create');
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            $userId = $this->request->getAttribute('identity')->getOriginalData()->id;
            $tenantId = (string)$this->request->getAttribute('identity')->getOriginalData()->tenant_id;

            $product->user_id = $userId;
            $product->tenant_id = $tenantId;
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
       
        $this->set(compact('product'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addFromFactory()
    {
        $this->Authorization->skipAuthorization();
        $product = $this->Products->newEmptyEntity();
        
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            $userId = $this->request->getAttribute('identity')->getOriginalData()->id;
            $tenantId = (string)$this->request->getAttribute('identity')->getOriginalData()->tenant_id;

            $product->user_id = $userId;
            $product->tenant_id = $tenantId;
            $product->from_factory = 1;
            $product->product_factory_id = $this->request->getData('factory_id');
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $factoryOptions = $this->Products->ProductFactory->find('list', ['limit' => 200]);
       
        $this->set(compact('product','factoryOptions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($product, 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $users = $this->Products->Users->find('list', ['limit' => 200]);
        $tenants = $this->Products->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('product', 'users', 'tenants'));
    }

    public function cart(){
       
        $this->Authorization->skipAuthorization();
        
        $mSession = $this->getRequest()->getSession();
        $check = $mSession->check('guestSession');

        
        if (($check)) {

            $name = $mSession->read('guestSession');
             
             $query = $this->Products->GuestCarts
             ->find()
             ->contain(['Products'])
            ->where(['session_id' => $name]);
            $guestCart = $this->paginate($query);
        }else{
            $this->Flash->success(__('Cart is empty'));

            return $this->redirect($this->referer());
        }
       
        $this->set(compact('guestCart'));
    }
    public function addToCart($productId){
        $this->Authorization->skipAuthorization();
        //add to guest checkout
        $session = $this->getRequest()->getSession();

        $check = $session->check('guestSession');
        if (!$check) {
           # write to session
            $rand = new ShortRandomString();
            $rand = $rand->random(15);
            $session->write('guestSession', $rand);
        }

        
            #add to Cart
            $guestCart = $this->Products->GuestCarts->newEmptyEntity();
            $guestCart->session_id = $session->read('guestSession');
            $guestCart->product_id = $productId;
            $guestCart->qty = 1;

            if ($this->Products->GuestCarts->save($guestCart)) {
                //$session->write('Cart.foo',$prodArray);
                $this->Flash->success(__('Added to cart'));

                return $this->redirect($this->referer());
            }
        

        
        
        
        
    }

    
}
