<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * StoreCategories Controller
 *
 * @property \App\Model\Table\StoreCategoriesTable $StoreCategories
 *
 * @method \App\Model\Entity\StoreCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StoreCategoriesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Stores', 'Users'],
        ];
        $storeCategories = $this->paginate($this->StoreCategories);

        $this->set(compact('storeCategories'));
    }

    /**
     * View method
     *
     * @param string|null $id Store Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storeCategory = $this->StoreCategories->get($id, [
            'contain' => ['Stores', 'Users'],
        ]);

        $this->set('storeCategory', $storeCategory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $storeCategory = $this->StoreCategories->newEmptyEntity();
        if ($this->request->is('post')) {
            $storeCategory = $this->StoreCategories->patchEntity($storeCategory, $this->request->getData());
            if ($this->StoreCategories->save($storeCategory)) {
                $this->Flash->success(__('The store category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store category could not be saved. Please, try again.'));
        }
        $stores = $this->StoreCategories->Stores->find('list', ['limit' => 200]);
        $users = $this->StoreCategories->Users->find('list', ['limit' => 200]);
        $this->set(compact('storeCategory', 'stores', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Store Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storeCategory = $this->StoreCategories->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storeCategory = $this->StoreCategories->patchEntity($storeCategory, $this->request->getData());
            if ($this->StoreCategories->save($storeCategory)) {
                $this->Flash->success(__('The store category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store category could not be saved. Please, try again.'));
        }
        $stores = $this->StoreCategories->Stores->find('list', ['limit' => 200]);
        $users = $this->StoreCategories->Users->find('list', ['limit' => 200]);
        $this->set(compact('storeCategory', 'stores', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Store Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storeCategory = $this->StoreCategories->get($id);
        if ($this->StoreCategories->delete($storeCategory)) {
            $this->Flash->success(__('The store category has been deleted.'));
        } else {
            $this->Flash->error(__('The store category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
