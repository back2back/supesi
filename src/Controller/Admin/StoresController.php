<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Log\LogTrait;
use Cake\ORM\TableRegistry;
/**
 * Stores Controller
 *
 * @property \App\Model\Table\StoresTable $Stores
 *
 * @method \App\Model\Entity\Store[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StoresController extends AppController
{
    use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        $this->paginate = [
            'contain' => ['Tenants', 'Users'],
        ];
        $stores = $this->paginate($this->Stores);

        $this->set(compact('stores'));
    }

    /**
     * View method
     *
     * @param string|null $id Store id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $store = $this->Stores->get($id, [
            'contain' => [
                'Tenants', 
                'Users'
                ],
        ]);

        $this->Authorization->authorize($store);
        $image = $this->Stores->StoreImages
                ->find()
                ->where(['StoreImages.store_id' => $id])
                ->first();
        $storeProducts = TableRegistry::getTableLocator()->get('StoreProducts');
        $inStore = $storeProducts->find()
                ->where(['StoreProducts.store_id' => $id])
                ->contain(['Products']);

        $this->set(compact('store','image','inStore'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($tenantId)
    {
        $tenant = $this->Stores->Tenants->get($tenantId);
        $store = $this->Stores->newEmptyEntity();
        $this->Authorization->authorize($store, 'create');
        if ($this->request->is('post')) {
            $store->tenant_id = $tenant->id;
            $store->user_id = $tenant->user_id;
            $store = $this->Stores->patchEntity($store, $this->request->getData());
            
            if ($this->Stores->save($store)) {
                $this->Flash->success(__('The store has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store could not be saved. Please, try again.'));
        }
       
        
        $this->set(compact('store', 'tenant'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Store id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $store = $this->Stores->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($store, 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $store = $this->Stores->patchEntity($store, $this->request->getData());
            
            if ($this->Stores->save($store)) {
                $this->Flash->success(__('The store has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store could not be saved. Please, try again.'));
        }
        $tenants = $this->Stores->Tenants->find('list', ['limit' => 200]);
        $users = $this->Stores->Users->find('list', ['limit' => 200]);
        $appCategories = $this->Stores->AppCategories->find('list', ['limit' => 200]);
        $isOnline = ['Offline', 'Online'];
        $this->set(compact('store', 'tenants', 'users', 'appCategories', 'isOnline'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Store id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $store = $this->Stores->get($id);
        if ($this->Stores->delete($store)) {
            $this->Flash->success(__('The store has been deleted.'));
        } else {
            $this->Flash->error(__('The store could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
