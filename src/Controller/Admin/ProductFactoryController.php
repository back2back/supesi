<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Log\LogTrait;
/**
 * ProductFactory Controller
 *
 * @property \App\Model\Table\ProductFactoryTable $ProductFactory
 *
 * @method \App\Model\Entity\ProductFactory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductFactoryController extends AppController
{
    use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $productFactory = $this->paginate($this->ProductFactory);

        $this->set(compact('productFactory'));
    }

    /**
     * View method
     *
     * @param string|null $id Product Factory id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productFactory = $this->ProductFactory->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('productFactory', $productFactory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $productFactory = $this->ProductFactory->newEmptyEntity();
        if ($this->request->is('post')) {
            $productFactory = $this->ProductFactory->patchEntity($productFactory, $this->request->getData());
            $userId = $this->request->getAttribute('identity')->getOriginalData()->id;
            $productFactory->user_id = $userId;
            if ($this->ProductFactory->save($productFactory)) {
                $this->Flash->success(__('The product factory has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product factory could not be saved. Please, try again.'));
        }
        $users = $this->ProductFactory->Users->find('list', ['limit' => 200]);
        $this->set(compact('productFactory', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Factory id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productFactory = $this->ProductFactory->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productFactory = $this->ProductFactory->patchEntity($productFactory, $this->request->getData());
            if ($this->ProductFactory->save($productFactory)) {
                $this->Flash->success(__('The product factory has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product factory could not be saved. Please, try again.'));
        }
        $users = $this->ProductFactory->Users->find('list', ['limit' => 200]);
        $this->set(compact('productFactory', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Factory id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productFactory = $this->ProductFactory->get($id);
        if ($this->ProductFactory->delete($productFactory)) {
            $this->Flash->success(__('The product factory has been deleted.'));
        } else {
            $this->Flash->error(__('The product factory could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
