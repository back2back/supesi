<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Log\LogTrait;
use Cake\Http\Exception\NotFoundException;
/**
 * ResetTokens Controller
 *
 * @property \App\Model\Table\ResetTokensTable $ResetTokens
 *
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResetTokensController extends AppController
{
    use LogTrait;
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('adminIndex', $this->ResetTokens->find());
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $resetTokens = $this->paginate($query);

        $this->set(compact('resetTokens'));
    }

    /**
     * View method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $resetToken = $this->ResetTokens->get($id, [
            'contain' => ['Users'],
        ]);
        $this->Authorization->authorize($resetToken);

        $this->set('resetToken', $resetToken);
    }

    public function verify($token = null)
    {
        $this->Authorization->skipAuthorization();
        
        $resetToken = $this->ResetTokens->find('token', ['token' => $token ]);
        //$resetToken = $resetToken->firstorFail();
        $this->log((string) $resetToken->created);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $myData = $this->request->getData();
            $updatedPassword = $myData['password'];
            //debug($updatedPassword);
           $success = $this->ResetTokens->updateToken($myData, $updatedPassword);
            if ($success) {
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            }else{
                $this->Flash->error(__('The password could not be updated.'));
            } 
        } 
        
        
        $this->set('resetToken', $resetToken);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resetToken = $this->ResetTokens->newEmptyEntity();
        $this->Authorization->authorize($resetToken);
        if ($this->request->is('post')) {
            $resetToken = $this->ResetTokens->patchEntity($resetToken, $this->request->getData());
            if ($this->ResetTokens->save($resetToken)) {
                $this->Flash->success(__('The reset token has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reset token could not be saved. Please, try again.'));
        }
        $users = $this->ResetTokens->Users->find('list', ['limit' => 200]);
        $this->set(compact('resetToken', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $resetToken = $this->ResetTokens->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($resetToken);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resetToken = $this->ResetTokens->patchEntity($resetToken, $this->request->getData());
            if ($this->ResetTokens->save($resetToken)) {
                $this->Flash->success(__('The reset token has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reset token could not be saved. Please, try again.'));
        }
        $users = $this->ResetTokens->Users->find('list', ['limit' => 200]);
        $this->set(compact('resetToken', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resetToken = $this->ResetTokens->get($id);
        $this->Authorization->authorize($resetToken);
        if ($this->ResetTokens->delete($resetToken)) {
            $this->Flash->success(__('The reset token has been deleted.'));
        } else {
            $this->Flash->error(__('The reset token could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
