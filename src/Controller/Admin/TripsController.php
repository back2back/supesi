<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Log\LogTrait;
/**
 * Trips Controller
 *
 * @property \App\Model\Table\TripsTable $Trips
 *
 * @method \App\Model\Entity\Trip[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TripsController extends AppController
{
    use LogTrait;
    public function initialize():void
    {
        parent::initialize();
       
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('adminIndex', $this->Trips->find()->where(['Trips.user_id' => 0]));
        $trips = $query->all();
        

        $this->set(compact('trips'));
    }

    /**
     * View method
     *
     * @param string|null $id Trip id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $trip = $this->Trips->get($id, [
            'contain' => ['OrderEntries'],
        ]);
        $this->Authorization->authorize($trip);
        $this->set('trip', $trip);
        $domain = Router::url('/', true);
        $domain = $domain.'admin/trips/assign/';
        $this->set('domain', $domain);
        
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $trip = $this->Trips->newEmptyEntity();
        if ($this->request->is('post')) {
            $trip = $this->Trips->patchEntity($trip, $this->request->getData());
            if ($this->Trips->save($trip)) {
                $this->Flash->success(__('The trip has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trip could not be saved. Please, try again.'));
        }
        $users = $this->Trips->Users->find('list', ['limit' => 200]);
        $orderEntries = $this->Trips->OrderEntries->find('list', ['limit' => 200]);


        $this->set(compact('trip', 'users', 'orderEntries'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Trip id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $trip = $this->Trips->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($trip,'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $trip = $this->Trips->patchEntity($trip, $this->request->getData());
            if ($this->Trips->save($trip)) {
                $this->Flash->success(__('The trip has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trip could not be saved. Please, try again.'));
        }
        $riderTable = TableRegistry::getTableLocator()->get('Riders');
        $riders = $riderTable->find()->where( ['status' => 1])->all();
        //$users = $this->Trips->Users->find('list', ['limit' => 200]);
        $this->log(print_r($riders, true));
        $this->set(compact('trip', 'riders',));
    }

    public function assign($tripId, $userId){

        // this is a direct, we already know the rider to be assigned
        $trip = $this->Trips->get($tripId, [
        ]);
        $this->Authorization->authorize($trip,'update');

        $riderTable = TableRegistry::getTableLocator()->get('Riders');
        $query = $riderTable->find()
            ->contain(['Users'])
            ->where(['Riders.user_id' => $userId,'Riders.status' => 1])
            ->all();

        $bar = ($query->first());
       
        //important
        $trip->user_id = $bar->user->id;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->log(print_r($this->request->getData(), true));
            $trip = $this->Trips->patchEntity($trip, $this->request->getData());
            if ($this->Trips->save($trip)) {
                $this->Flash->success(__('The trip has been assigned.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trip could not be saved. Please, try again.'));
        } 

        $this->set('trip', $trip);
        $this->set('rider', $bar->user->id);

    }

    public function finish($id){
        
        $trip = $this->Trips->get($id, [
        ]);

        $this->Authorization->authorize($trip,'update');
        $trip->status = 1;
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->Trips->save($trip)) {
                $this->Flash->success(__('The trip has been ended.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trip could not be finished. Please, try again.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Trip id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $trip = $this->Trips->get($id);

        $this->Authorization->authorize($trip);

        if ($this->Trips->delete($trip)) {
            $this->Flash->success(__('The trip has been deleted.'));
        } else {
            $this->Flash->error(__('The trip could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
