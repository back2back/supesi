<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Log\LogTrait;
use Cake\Event\Event;
class DashboardsController extends AppController
{
	use LogTrait;
	public function initialize():void
    {
        parent::initialize();
        
    }
	public function index(){

		$this->Authorization->skipAuthorization();
	}

	public function add(){

	}

	public function newsLetter(){
		$this->Authorization->skipAuthorization();
	if ($this->request->is('post')) {
		
		$event = new Event('Model.Dashboards.newsLetter', $this, ['data' => $this->request->getData() ]);
        $this->getEventManager()->dispatch($event);

        return $this->redirect(['action' => 'index']);
	}
	
	}
}