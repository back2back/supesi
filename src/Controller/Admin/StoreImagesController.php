<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
/**
 * StoreImages Controller
 *
 * @property \App\Model\Table\StoreImagesTable $StoreImages
 *
 * @method \App\Model\Entity\StoreImage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StoreImagesController extends AppController
{
    public function initialize():void
    {
        parent::initialize();
        
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $storeImages = $this->paginate($this->StoreImages);

        $this->set(compact('storeImages'));
    }

    /**
     * View method
     *
     * @param string|null $id Store Image id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storeImage = $this->StoreImages->get($id, [
            'contain' => ['Users', 'Stores'],
        ]);

        $this->set('storeImage', $storeImage);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($storeId = null)
    {
        $storeImage = $this->StoreImages->newEmptyEntity();

        $this->Authorization->authorize($storeImage, 'create');

        if ($this->request->is('post')) {
            $storeImage = $this->StoreImages->patchEntity($storeImage, $this->request->getData());
            if ($this->StoreImages->save($storeImage)) {
                $this->Flash->success(__('The store image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store image could not be saved. Please, try again.'));
        }
        $users = $this->StoreImages->Users->find('list', ['limit' => 200]);
        $this->set(compact('storeImage', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Store Image id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storeImage = $this->StoreImages->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($storeImage, 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storeImage = $this->StoreImages->patchEntity($storeImage, $this->request->getData());
            if ($this->StoreImages->save($storeImage)) {
                $this->Flash->success(__('The store image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store image could not be saved. Please, try again.'));
        }
        $users = $this->StoreImages->Users->find('list', ['limit' => 200]);
        $this->set(compact('storeImage', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Store Image id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        
        $this->request->allowMethod(['post', 'delete']);
        $storeImage = $this->StoreImages->get($id);
        $this->Authorization->authorize($storeImage);
        

        if ($this->StoreImages->delete($storeImage)) {
            $this->Flash->success(__('The store image has been deleted.'));
        } else {
            $this->Flash->error(__('The store image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
