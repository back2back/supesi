<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
/**
 * Invites Controller
 *
 * @property \App\Model\Table\InvitesTable $Invites
 *
 * @method \App\Model\Entity\Invite[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InvitesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Tenants'],
        ];
        $invites = $this->paginate($this->Invites);

        $this->set(compact('invites'));
    }

    /**
     * View method
     *
     * @param string|null $id Invite id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $invite = $this->Invites->get($id, [
            'contain' => ['Users', 'Tenants'],
        ]);

        $this->set('invite', $invite);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $invite = $this->Invites->newEmptyEntity();
        if ($this->request->is('post')) {
            $invite = $this->Invites->patchEntity($invite, $this->request->getData());
            if ($this->Invites->save($invite)) {
                $this->Flash->success(__('The invite has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The invite could not be saved. Please, try again.'));
        }
        $users = $this->Invites->Users->find('list', ['limit' => 200]);
        $tenants = $this->Invites->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('invite', 'users', 'tenants'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Invite id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $invite = $this->Invites->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $invite = $this->Invites->patchEntity($invite, $this->request->getData());
            if ($this->Invites->save($invite)) {
                $this->Flash->success(__('The invite has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The invite could not be saved. Please, try again.'));
        }
        $users = $this->Invites->Users->find('list', ['limit' => 200]);
        $tenants = $this->Invites->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('invite', 'users', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Invite id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $invite = $this->Invites->get($id);
        if ($this->Invites->delete($invite)) {
            $this->Flash->success(__('The invite has been deleted.'));
        } else {
            $this->Flash->error(__('The invite could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
