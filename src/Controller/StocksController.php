<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Stocks Controller
 *
 * @property \App\Model\Table\StocksTable $Stocks
 *
 * @method \App\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StocksController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        
     

        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Stocks->find());
        

        
        
        $stocks = $this->paginate($query->contain(['Products']));
        
        


        $this->set(compact('stocks'));
    }

    

    /**
     * Edit method
     *
     * @param string|null $productId Product  id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function edit($productId = null)
    {
        $stock = $this->Stocks->find()->where(['Stocks.product_id' => $productId])->firstOrFail();
        

        $initialStock = $stock->niceCounted;
        $this->Authorization->authorize($stock, 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stock = $this->Stocks->patchEntity($stock, $this->request->getData());
            
            $addStock = ($this->request->getData('restock'));
            if (($addStock != null)) {
                $stock->counted = $initialStock + $addStock;
            }
           

            if ($this->Stocks->save($stock)) {
                $this->Flash->success(__('The stock has been added.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stock could not be saved. Please, try again.'));
        }
        
        $this->set(compact('stock'));
    }*/

    /**
     * Delete method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stock = $this->Stocks->get($id);
        if ($this->Stocks->delete($stock)) {
            $this->Flash->success(__('The stock has been deleted.'));
        } else {
            $this->Flash->error(__('The stock could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
