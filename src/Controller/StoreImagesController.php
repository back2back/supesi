<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;

/**
 * StoreImages Controller
 *
 * @property \App\Model\Table\StoreImagesTable $StoreImages
 *
 * @method \App\Model\Entity\StoreImage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StoreImagesController extends AppController
{
    use LogTrait;
    
    public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['add','index','delete']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->StoreImages->find());
        
        $storeImages = $this->paginate($query);

        $this->set(compact('storeImages'));
    }

    /**
     * View method
     *
     * @param string|null $id Store Image id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storeImage = $this->StoreImages->get($id, [
            'contain' => ['Users', 'Stores'],
        ]);

        $this->Authorization->authorize($storeImage);

        $this->set('storeImage', $storeImage);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($storeId)
    {
        $user = $this->request->getAttribute('identity');

        $storeImage = $this->StoreImages->newEmptyEntity();
        $store = $this->StoreImages->Stores->get($storeId);


        $this->Authorization->authorize($store, 'image');
        if ($this->request->is('post')) {

            $storeImage = $this->StoreImages->patchEntity($storeImage, $this->request->getData());
            $storeImage->user_id = $user->getOriginalData()->id;
            $storeImage->store_id = $store->id;

            if ($this->StoreImages->save($storeImage)) {
                $this->Flash->success(__('The store image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store image could not be saved. Please, try again.'));
        }
        
        $this->set(compact('storeImage', 'store'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Store Image id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storeImage = $this->StoreImages->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storeImage = $this->StoreImages->patchEntity($storeImage, $this->request->getData());
            if ($this->StoreImages->save($storeImage)) {
                $this->Flash->success(__('The store image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store image could not be saved. Please, try again.'));
        }
        $users = $this->StoreImages->Users->find('list', ['limit' => 200]);
        $this->set(compact('storeImage', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Store Image id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storeImage = $this->StoreImages->get($id);
        if ($this->StoreImages->delete($storeImage)) {
            $this->Flash->success(__('The store image has been deleted.'));
        } else {
            $this->Flash->error(__('The store image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
