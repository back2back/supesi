<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;
use Cake\Http\Client;

/**
 * Checkouts Controller
 *
 * @property \App\Model\Table\CheckoutsTable $Checkouts
 *
 * @method \App\Model\Entity\Checkout[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CheckoutsController extends AppController
{
    use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['add','index','mpesa','test','foo']);
    }

    public function test(){
       $this->Authorization->skipAuthorization();
       $this->autoRender = false; 

        //listen
        $passKey = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';
        $till = '174379';
        $mpesaUsername = 'BjkXdbUnYCVvZkQIMBsSI1d213Zb4Mwu';
        $mpesaPassword = 'PAqBGCpHFNGHG34I';
        $urlEndpoint = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $http = new Client();

        // Simple get
        $response = $http->get($urlEndpoint, [], [
            'auth' => ['username' => $mpesaUsername, 'password' => $mpesaPassword]]);
        $json = $response->getJson();

        $this->log((string) print_r($json, true));

    }

    public function mpesa(){
        $this->Authorization->skipAuthorization();
        $this->autoRender = false; 

        //listen
        $passKey = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';
        $till = '174379';
        $mpesaUsername = 'BjkXdbUnYCVvZkQIMBsSI1d213Zb4Mwu';
        $mpesaPassword = 'PAqBGCpHFNGHG34I';
        $timestamp = '20200712122035';
        $urlStk = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        /*$request = file_get_contents('php://input');
        if ($request === FALSE || empty($request)) {
            $this->log('I got an error reading mpesa');
            errorAndDie('Error reading POST data');
        }*/

        $password = base64_encode($till.$passKey.$timestamp);
        

        $accessToken = 'L4TfJolcygpraGxKTgehRdqUnL2l';
        $http = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json'
            ]
        ]);

        $ngrok = 'https://e50b9e55cc1e.ngrok.io/supesi/checkouts/foo';

        $data =[
            'BusinessShortCode' => $till,
            'Password' => $password,
            'Timestamp' => $timestamp,
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => 1,
            'PartyA' => 254716666309,
            'PartyB' => $till,
            'PhoneNumber' => 254716666309,
            'CallBackURL' => $ngrok,
            'AccountReference' => 'supesi',
            'TransactionDesc' => 'subs',
        ];
        $data = json_encode($data);
        
        $response = $http->post($urlStk, $data);
        $json = $response->getJson();
        $this->log((string) print_r($json, true));
    }
    public function foo(){
        $this->Authorization->skipAuthorization();
        $this->autoRender = false; 

        /*$request = file_get_contents('php://input');
        if ($request === FALSE || empty($request)) {
            $this->log('I got an error reading mpesa');
            
        }else{
            $this->log('Hurray');
        }*/
        $queryUrl = 'https://sandbox.safaricom.co.ke/mpesa/stkpushquery/v1/query';
        $accessToken = 'L4TfJolcygpraGxKTgehRdqUnL2l';
        $http = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json'
            ]
        ]);
        $till = '174379';
        $passKey = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';
        $timestamp = '20200712122035';
        $password = base64_encode($till.$passKey.$timestamp);


        $data =[
            'BusinessShortCode' => $till,
            'Password' => $password,
            'Timestamp' => $timestamp,
            'CheckoutRequestID' => 'ws_CO_120720201305341103',
        ];
        $data = json_encode($data);
        $response = $http->post($queryUrl, $data);
        $json = $response->getJson();
        $this->log((string) print_r($json, true));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();

        $identity = $this->request->getAttribute('identity');
        $mSession = $this->request->getSession();
        $check = $mSession->check('guestSession');

        $query = null;
        if ($check) {
            $session = $mSession->read('guestSession');
             $query = $this->Checkouts->cleanUp($identity, $session);
        }
       
        if (is_null($query)) {
             $checkouts = $this->paginate($this->Checkouts);
        }else{
           $checkouts = $this->paginate($query);
        }
        
        

        $this->set(compact('checkouts'));
    }

    /**
     * View method
     *
     * @param string|null $id Checkout id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $checkout = $this->Checkouts->get($id, [
            'contain' => ['Sessions', 'Users'],
        ]);

        $this->set('checkout', $checkout);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $checkout = $this->Checkouts->newEmptyEntity();
        //if ($this->request->is('post')) {
            //$checkout = $this->Checkouts->patchEntity($checkout, $this->request->getData());
            $identity = $this->request->getAttribute('identity');
            $checkout->user_id = $identity->getOriginalData()->get('id');

            $mSession = $this->request->getSession();
            $checkout->session_id = $mSession->read('guestSession');

            if ($this->Checkouts->save($checkout)) {
                $this->Flash->success(__('The checkout has been saved.'));
                $mSession->delete('guestSession');

                return $this->redirect(['action' => 'thankYou']);
            }
            $this->Flash->error(__('The checkout could not be saved. Please, try again.'));
        //}
        
        $this->set(compact('checkout'));
    }

    public function thankYou(){

        $this->Authorization->skipAuthorization();
    }

    /**
     * Edit method
     *
     * @param string|null $id Checkout id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $checkout = $this->Checkouts->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $checkout = $this->Checkouts->patchEntity($checkout, $this->request->getData());
            if ($this->Checkouts->save($checkout)) {
                $this->Flash->success(__('The checkout has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The checkout could not be saved. Please, try again.'));
        }
        $sessions = $this->Checkouts->Sessions->find('list', ['limit' => 200]);
        $users = $this->Checkouts->Users->find('list', ['limit' => 200]);
        $this->set(compact('checkout', 'sessions', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Checkout id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $checkout = $this->Checkouts->get($id);
        if ($this->Checkouts->delete($checkout)) {
            $this->Flash->success(__('The checkout has been deleted.'));
        } else {
            $this->Flash->error(__('The checkout could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
