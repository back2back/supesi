<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;
use Cake\ORM\Query;

class StoresController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index','view']);
        
    }

    public function index()
    {
    	$this->Authorization->skipAuthorization();
        
    	/*$stores = $this->Stores->find()
            ->contain('AppCategories', function(Query $q){
                return $q
                    ->group('AppCategory.name');
                ;
            });*/

        $stores = $this->Stores->find('storeCategory');
          
         

    	$this->set([
    		'stores' => $stores,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['stores', 'message']);
    }

    public function view($id = null)
    {
        $this->Authorization->skipAuthorization();
        $options = [
            'lat' => -1.322299,
            'lng' => 36.801759,
            'storeId' => $id,
        ];
    	$delivery = $this->Stores->getDelivery($options);
        $store = $this->Stores->get($id);

    	if ($store) {

    		$this->set([
    			'message' => true,
    			'store' => $store,
                'delivery' => $delivery
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'store','delivery']);
    }

    //todo
    //remove product data to table object

    public function add()
    {
    	
        $product = $this->Products->newEmptyEntity();
        $this->Authorization->authorize($product,'create');
        $this->request->allowMethod(['post', 'put']);


        $data = $this->request->getData();
        $user_id = $this->request->getAttribute('identity')->getOriginalData()->id;
        $tenant_id = (string)$this->request->getAttribute('identity')->getOriginalData()->tenant_id;
        

        
        if (array_key_exists('title', $data)) {
            $product->title = $data['title'];
        }else{
            $product->title = __('no title');
        }
        if (array_key_exists('description', $data)) {
            $product->description = $data['description'];
        }
        else{
            $product->description = 'optional';
        }
        if (array_key_exists('nice_price_in', $data)) {
            # code...
            $product->price_in = $data['nice_price_in'];
        }else{
            $product->price_in = 0;
        }
        if (array_key_exists('nice_price_out', $data)) {
            # code...
            $product->price_out = $data['nice_price_out'];
        }else{
            $product->price_out = 0;
        }
        if (array_key_exists('stock_count', $data)) {
            $product->stock_count = $data['stock_count'];
        }
        
        
        $product->user_id = $user_id;
        $product->tenant_id = $tenant_id;
        


        
        if ($this->Products->save($product)) {
            $message = true;
        }else{
            $message = false;
        }

        $this->set([
            'message' => $message,
            'product' => $product,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'product']);
    }

    public function edit($id = null){
        $product = $this->Products->get($id);
        $this->Authorization->authorize($product,'update');
        $this->request->allowMethod(['post', 'put', 'patch']);


        $data = $this->request->getData();
        $user_id = $this->request->getAttribute('identity')->getOriginalData()->id;
        $tenant_id = (string)$this->request->getAttribute('identity')->getOriginalData()->tenant_id;



        
        if (array_key_exists('title', $data)) {
            $product->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $product->description = $data['description'];
        }
        
        if (array_key_exists('nice_price_in', $data)) {
            # code...
            $product->price_in = $data['nice_price_in'];
        }
        if (array_key_exists('nice_price_out', $data)) {
            # code...
            $product->price_out = $data['nice_price_out'];
        }
        if (array_key_exists('stock_count', $data)) {
            $product->stock_count = $data['stock_count'];
        }
        
        
        $product->user_id = $user_id;
        $product->tenant_id = $tenant_id;

        if ($this->Products->save($product)) {
            $message = true;
        }else{
            $message = false;
        }

        $this->set([
            'message' => $message,
            'product' => $product,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'product']);
    }
}