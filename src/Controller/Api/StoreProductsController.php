<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;
use Cake\ORM\Query;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Collection\CollectionInterface;
use Cake\Collection\Collection;

class StoreProductsController extends ApiController
{
	use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['view','delivery']);
        
    }

    public function index(){
        $this->Authorization->skipAuthorization();
        $identity = $this->request->getAttribute('identity');
        $store = $this->StoreProducts->Stores->find()->where(['Stores.tenant_id' => $identity->getOriginalData()->tenant_id])->first();
         $storeProducts = $this->StoreProducts->find()
                ->where(['store_id' => $store->id,
                    //'is_available' => 1 
                ])
                ->contain('Products', function (Query $q){
                    return $q
                        ->contain('Stocks');
                }
            );

        $this->set([
            'storeProducts' => $storeProducts,
            'message' => true
        ]);
        $this->viewBuilder()->setOption('serialize', ['storeProducts', 'message']);
    }

    public function delivery(){

        $this->request->allowMethod(['post', 'put']);
    	$this->Authorization->skipAuthorization();
       
        $options = $this->request->getData();

        $productImages = TableRegistry::getTableLocator()->get('ProductImages');
        $cdn = Configure::read('FileStorage.cdn');


        $storeProducts = $this->StoreProducts->find()
                ->where(['store_id' => $options['storeId'],'is_available' => 1 ])
                
                
                ->contain('Products', function (Query $q) use ($productImages, $cdn){

          
                    return $q;

                });

        $collection = collection($storeProducts);
        $prod = $collection->extract('product');
        
        
        $collection = $prod->each(function($value, $key) use ($productImages, $cdn){
            $productId = $value->id;
            $data = $productImages->find()->where(['product_id' => $productId])->first();
            return $value->banner = $cdn .'/'.$data->dir.'/' .$data->name;
        });
        
        
        $delivery = $this->StoreProducts->Stores->getDelivery($options);
        
    	$this->set([
    		'storeProducts' => $storeProducts,
    		'message' => true,
            'delivery' => $delivery,
    	]);
    	$this->viewBuilder()->setOption('serialize', ['storeProducts', 'message','delivery']);
    }

}