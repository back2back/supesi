<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Event\EventInterface;
use Cake\Core\Configure;
use Cake\Log\LogTrait;

/**
 * StockRetakes Controller
 *
 * @property \App\Model\Table\StockRetakesTable $StockRetakes
 *
 * @method \App\Model\Entity\StockRetake[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StockRetakesController extends ApiController
{
	public function initialize():void
    {
        parent::initialize();
    }

    public function index(){
    	$user = $this->request->getAttribute('identity');
        $query = $user->applyScope('index', $this->StockRetakes
        	->find()
        	->order(['id' => 'DESC'])
        );

        $this->set([
    		'StockRetakes' => $query,
    		'message' => true
    	]);

        $this->viewBuilder()->setOption('serialize', ['message', 'StockRetakes']);
    }

    public function add(){

    	$this->request->allowMethod(['post', 'put']);

    	$stockRetake = $this->StockRetakes->newEmptyEntity();
        $user = $this->request->getAttribute('identity');
        $stockRetake->user_id = $user->getIdentifier();

        $whatStock = $this->StockRetakes->Stocks->get($this->request->getData('stock_id'));
        
        
        $stockRetake->stock_id = $whatStock->id;
        $this->Authorization->authorize($whatStock, 'create');
       
        
            $stockRetake = $this->StockRetakes->patchEntity($stockRetake, $this->request->getData());
            if ($this->StockRetakes->save($stockRetake)) {
                $message = true;
            }else{
            	$message = false;
            }

            $this->set([
            	'message' => $message,
            	'StockRetake' => $stockRetake,
            ]);
           
        $this->viewBuilder()->setOption('serialize', ['message', 'StockRetake']);
    

    }
}