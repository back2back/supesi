<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
/**
 * ResetTokens Controller
 *
 * @property \App\Model\Table\ResetTokensTable $ResetTokens
 *
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResetTokensController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $resetTokens = $this->paginate($this->ResetTokens);

        $this->set(compact('resetTokens'));
    }

    /**
     * View method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $resetToken = $this->ResetTokens->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('resetToken', $resetToken);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resetToken = $this->ResetTokens->newEmptyEntity();
        if ($this->request->is('post')) {
            $resetToken = $this->ResetTokens->patchEntity($resetToken, $this->request->getData());
            if ($this->ResetTokens->save($resetToken)) {
                $this->Flash->success(__('The reset token has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reset token could not be saved. Please, try again.'));
        }
        $users = $this->ResetTokens->Users->find('list', ['limit' => 200]);
        $this->set(compact('resetToken', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $resetToken = $this->ResetTokens->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resetToken = $this->ResetTokens->patchEntity($resetToken, $this->request->getData());
            if ($this->ResetTokens->save($resetToken)) {
                $this->Flash->success(__('The reset token has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reset token could not be saved. Please, try again.'));
        }
        $users = $this->ResetTokens->Users->find('list', ['limit' => 200]);
        $this->set(compact('resetToken', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resetToken = $this->ResetTokens->get($id);
        if ($this->ResetTokens->delete($resetToken)) {
            $this->Flash->success(__('The reset token has been deleted.'));
        } else {
            $this->Flash->error(__('The reset token could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
