<?php

declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;
use Cake\ORM\Query;
use Cake\Collection\Collection;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class AppCategoriesController extends ApiController{

	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index']);
        
    }

    public function index()
    {
    	$this->Authorization->skipAuthorization();

        $lat = $this->request->getQuery('lat');
        $lng = $this->request->getQuery('lng');

        $storesTable = TableRegistry::getTableLocator()->get('Stores');
        
        $near = $storesTable->getNearMe(['lat' => $lat, 'lng' =>$lng]);

        $storeImages = TableRegistry::getTableLocator()->get('StoreImages');
        $cdn = Configure::read('FileStorage.cdn');

        //mapper and reducer not called yet
        $mapper = function ($data, $key, $mapReduce) use ($cdn, $storeImages){
            if (sizeof($data->stores) > 0) {
                foreach($data->stores as $n){

                    $q = $storeImages->find()
                        ->where(['StoreImages.store_id' => $n->id])
                        ->first();
                    if ($q === null) {
                        $n->banner = null;
                    }else{
                        $n->banner = $cdn. '/'. $q->dir. '/'. $q->photo;
                    }


                    
                }
                $mapReduce->emitIntermediate($data, 'data');
            }

        };

        $reducer = function ($datas, $status, $mapReduce){
            $mapReduce->emit($datas, $status);
        };

        //$this->log((string) print_r($near, true));

        if (sizeof($near) == 0) {
           $appCategories = null;
           $message = false;
        }else{
            $appCategories = 
            $this->AppCategories
                    ->find('all')
                    ->mapReduce($mapper, $reducer)
                    ->contain('Stores', function ($q) use ($near){
                    
                    $nn = array();
                    foreach($near as $foo){
                        array_push($nn, $foo['id']);
                        
                    }
                        return $q
                         ->group('id')
                         ->where(['Stores.id IN' => $nn ])
                         ->where(['Stores.is_online' => 1])
                         ->where(['Stores.app_category_id >' => 0]);
                    });
            $message = true;
        }
    	
           

        
    	$this->set([
    		'appCategories' => $appCategories,
    		'message' => $message
    	]);
    	$this->viewBuilder()->setOption('serialize', ['appCategories', 'message']);


    }
	
}