<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;

class TeamsController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['index','view','add']);
        //$this->Authorization->skipAuthorization(['login', 'add']);
    }

    public function index()
    {
    	
    	$teams = $this->Teams->find('all');
    	$this->set([
    		'teams' => $teams,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['teams', 'message']);
    }

    public function view($id = null)
    {
    	$team = $this->Teams->get($id, [
    		//'contain' => ['Tenants']
    	]);


    	if ($team) {

    		$this->set([
    			'message' => true,
    			'team' => $team
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'team']);
    }

    public function add()
    {
    	$this->request->allowMethod(['post', 'put']);
    	$team = $this->Teams->newEntity($this->request->getData());
    	if ($this->Teams->save($team)) {
    		$message = true;
    	}else{
    		$message = false;
    	}

    	$this->set([
    		'message' => $message,
    		'team' => $team,
    	]);
    	$this->viewBuilder()->setOption('serialize', ['message', 'team']);
    }
}