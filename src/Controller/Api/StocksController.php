<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;

class StocksController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['index','view','add']);
        //$this->Authorization->skipAuthorization(['login', 'add']);
    }

    public function index()
    {
    	
    	$identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Stocks->find());
        

        
        
        $stocks = $this->paginate($query->contain(['Products']));
    	$this->set([
    		'stocks' => $stocks,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['stocks', 'message']);
    }

    public function view($id = null)
    {
    	$stock = $this->Stocks->get($id, [
    		//'contain' => ['Tenants']
    	]);


    	if ($stock) {

    		$this->set([
    			'message' => true,
    			'stock' => $stock
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'stock']);
    }

    public function add()
    {
    	$this->request->allowMethod(['post', 'put']);
    	$stock = $this->Stocks->newEntity($this->request->getData());
    	if ($this->Stocks->save($stock)) {
    		$message = true;
    	}else{
    		$message = false;
    	}

    	$this->set([
    		'message' => $message,
    		'stock' => $stock,
    	]);
    	$this->viewBuilder()->setOption('serialize', ['message', 'stock']);
    }
}