<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;


class RidersController extends ApiController
{
	use LogTrait;

	public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['login','add','customer']);
    }

    public function status(){
    	$status =  $this->request->getQuery('q');
    	
    	$user = $this->request->getAttribute('identity');

    	$rider = $this->Riders->find()->where(['Riders.user_id' => $user->id])->first();
    	$this->Authorization->authorize($rider, 'update');

    	
    	$message = $this->Riders->updateStatus($rider, $status);
    	$this->set([
            'message' => $message,
            
        ]);

        $this->viewBuilder()->setOption('serialize', ['message']);
    	//$this->Riders->updateStatus($status, $user);

    }
}