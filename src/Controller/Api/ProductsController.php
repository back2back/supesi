<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;


class ProductsController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        
    }

    public function index()
    {
    	
    	//$products = $this->Products->find('all');
        
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Products->find());
        $query->contain(['Stocks']);
        
        $products = $this->paginate($query);
    	$this->set([
    		'products' => $products,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['products', 'message']);
    }

    public function view($id = null)
    {
    	$product = $this->Products->get($id, [
    		//'contain' => ['Tenants']
    	]);


    	if ($product) {

    		$this->set([
    			'message' => true,
    			'product' => $product
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'product']);
    }

    //todo
    //remove product data to table object

    public function add()
    {
    	
        $data = $this->request->getData();
        $user_id = $this->request->getAttribute('identity')->getOriginalData()->id;
        $tenant_id = $this->request->getAttribute('identity')->getOriginalData()->tenant_id;
     
        $data = $this->request->getData();
        $product = $this->Products->newEntity($data);
        $product->user_id = $user_id;
        $product->tenant_id = $tenant_id;

        if (array_key_exists('nice_price_in', $data)) {
            # code...
            $product->price_in = $data['nice_price_in'];
        }else{
            $product->price_in = 0;
        }
        if (array_key_exists('nice_price_out', $data)) {
            # code...
            $product->price_out = $data['nice_price_out'];
        }else{
            $product->price_out = 0;
        }

        $this->Authorization->authorize($product,'create');
        $this->request->allowMethod(['post', 'put']);

        
        if ($this->Products->save($product)) {
            $message = true;
        }else{
            $message = false;
        }

        $this->set([
            'message' => $message,
            'product' => $product,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'product']);
    }

    public function edit($id = null){
        $product = $this->Products->get($id);
        $this->Authorization->authorize($product,'update');
        $this->request->allowMethod(['post', 'put', 'patch']);


        $data = $this->request->getData();
        $user_id = $this->request->getAttribute('identity')->getOriginalData()->id;
        $tenant_id = (string)$this->request->getAttribute('identity')->getOriginalData()->tenant_id;

        $data = $this->request->getData();

        if (array_key_exists('nice_price_in', $data)) {
            # code...
            $product->price_in = $data['nice_price_in'];
        }else{
            $product->price_in = 0;
        }
        if (array_key_exists('nice_price_out', $data)) {
            # code...
            $product->price_out = $data['nice_price_out'];
        }else{
            $product->price_out = 0;
        }

        $this->Products->patchEntity($product, $data);
        if ($this->Products->save($product)) {
            $message = true;
        }else{
            $message = false;
        }

        $this->set([
            'message' => $message,
            'product' => $product,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'product']);

    }


}