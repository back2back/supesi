<?php
declare(strict_types=1);

namespace App\Controller\Api;

use Cake\Log\LogTrait;
use Cake\ORM\Query;
use Cake\Collection\Collection;

/**
 * Trips Controller
 *
 * @property \App\Model\Table\TripsTable $Trips
 *
 * @method \App\Model\Entity\Trip[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TripsController extends ApiController
{
    use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        
    }

    public function deliveryOnly(){
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Trips->find());
        //for my every trip, get the order
        //$order = $query->all()->extract('order_entry_id');
        $byIndex = $query->all()->indexBy('order_entry_id');
        $entries = $byIndex->extract('order_entry_id');
       

     
        $tt = $this->Trips->OrderEntries->find()
            ->where(['OrderEntries.id IN' => $entries->toArray() ])
            ->contain(['Users', 'Trips'])
            ->all();
       
          
        $total = $tt->count();
        if ($total > 0) {
            $message = true;
        }else{
            $message = false;
        }
       
        $this->set('orderEntries' , $tt);
        
        $this->set([
            
            'message' => $message
        ]);
        $this->viewBuilder()->setOption('serialize', ['orderEntries','message']);

    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Trips->find());
        $query

            ->contain('OrderEntries', function(Query $q){
                return
                        //$q->contain(['Stores','Users']);
                $q->contain(['Stores' =>
                    [
                        'queryBuilder' => function(Query $z){
                            return $z->contain('Users');
                        }
                    ]
                    ,'Users']);
            });
        
          
        $total = $query->count();
        if ($total > 0) {
            $message = true;
        }else{
            $message = false;
        }
        $this->paginate($query);
        
        $trips = $this->paginate($query);
        $this->set([
            'trips' => $trips,
            'message' => $message
        ]);
        $this->viewBuilder()->setOption('serialize', ['trips','message']);

        
    }

    /**
     * View method
     *
     * @param string|null $id Trip id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $trip = $this->Trips->get($id, [
            //'contain' => ['Users', 'OrderEntries'],
        ]);
        $this->Authorization->authorize($trip);

        
        $this->set([
                'message' => true,
                'trip' => $trip
                
            ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'trip']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $trip = $this->Trips->newEmptyEntity();
        if ($this->request->is('post')) {
            $trip = $this->Trips->patchEntity($trip, $this->request->getData());
            if ($this->Trips->save($trip)) {
                $this->Flash->success(__('The trip has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The trip could not be saved. Please, try again.'));
        }
        $users = $this->Trips->Users->find('list', ['limit' => 200]);
        $orderEntries = $this->Trips->OrderEntries->find('list', ['limit' => 200]);
        $this->set(compact('trip', 'users', 'orderEntries'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Trip id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['post', 'put']);
        $trip = $this->Trips->get($id);

        
        $this->Authorization->authorize($trip, 'update');
        $trip = $this->Trips->patchEntity($trip, $this->request->getData());
        
        
        if ($this->Trips->save($trip)) {
            $message = true;
            
        }else{
            $message = false;
        }
        $this->set([
                'message' => $message,
                'trip' => $trip
                
            ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'trip']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Trip id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $trip = $this->Trips->get($id);
        if ($this->Trips->delete($trip)) {
            $this->Flash->success(__('The trip has been deleted.'));
        } else {
            $this->Flash->error(__('The trip could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
