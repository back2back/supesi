<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;
use Cake\Event\Event;

class OrdersController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
    }

    public function index()
    {
    	
    	$orders = $this->Orders->find('all');
    	$this->set([
    		'orders' => $orders,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['orders', 'message']);
    }

    public function view($id = null)
    {
    	$order = $this->Orders->get($id, [
    		//'contain' => ['Tenants']
    	]);

    	if ($order) {

    		$this->set([
    			'message' => true,
    			'order' => $order
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'order']);
    }

    public function add()
    {
    	$this->request->allowMethod(['post', 'put']);
    	
        $data = $this->request->getData();
        
        $identity = $this->request->getAttribute('identity');
        $this->Authorization->skipAuthorization();
        
        if (array_key_exists('mode', $data['0'])) {
            
            $worldMode = $data['0']['mode'];
        }else{
            
            $worldMode = 0;
        }

        
        $storeId = $data['0']['store_id'];
        $worldMode = $data['0']['mode'];


        

        if ($worldMode) {
            $bars = $this->Orders->worldOnlineTransact($data, $identity, $storeId);
            if ($bars) {
            $message = true;
                $orders = $bars[1];
                $orderEntry = $bars[2];
            }else{
                $message = false;
                $orders = '';
            }

            $this->set([
                'message' => $message,
                'orders' => $orders,
                'orderEntry' => $orderEntry,
            ]);
            
            $this->viewBuilder()->setOption('serialize', ['message', 'orders','orderEntry']);

             //event to process the queue
            // adjust the total as order changes
            /**/

        }else{
            $bars = $this->Orders->orderEntryTransaction($data, $identity, $storeId);
            if ($bars) {
                $message = true;
                $orders = $bars[1];
            }else{
                $message = false;
                $orders = '';
            }

            $this->set([
                'message' => $message,
                'orders' => $orders,
            ]);
            
            $this->viewBuilder()->setOption('serialize', ['message', 'orders']);
        }
        
    	


    }
}