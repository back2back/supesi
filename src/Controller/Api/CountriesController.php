<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;

class CountriesController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index','view','add']);
        //$this->Authorization->skipAuthorization(['login', 'add']);
    }

    public function index()
    {
       
        $query = $this->Countries->find('country');
    	$this->set([
    		'countries' => $query,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['countries', 'message']);
    }

    public function view($id = null)
    {
    	$country = $this->Countries->get($id, [
    		//'contain' => ['Tenants']
    	]);


    	if ($country) {

    		$this->set([
    			'message' => true,
    			'country' => $country
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'country']);
    }
}