<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends ApiController
{
    use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['login','add','customer']);
    }

    
    public function index()
    {
        $users = $this->Users->find('all');

        $this->set([
            'users' => $users,
        ]);
        $this->viewBuilder()->setOption('serialize', ['users']);
    }

    
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Tenants', 'Orders', 'Products', 'Stocks', 'Teams'],
        ]);

        $this->set([
            'user' => $user,
            "message" => true,
        ]);
        $this->viewBuilder()->setOption('serialize', ['user',"message"]);
    }

    

    //adding a tenant business owner
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEntity($this->request->getData());

        $tenant = null;
        $jwt = null;
        
        if ($this->Users->save($user)) {
            
            $message = true;
            $query = $this->Users->Tenants->find('name', ['tenantName' => $user->tenantName]);
            $query->where(['user_id' => $user->id]);
            $query->contain(['Teams']);
            $tenant = $query->first();
            $jwt = $this->Users->jwt($user->id, $tenant->id);
        } else {
            $message = false;
        }

        
        $this->set([
            'message' => $message,
            'user' => $user,
            'tenant' => $tenant,
            'jwt' => $jwt
        ]);
        $this->viewBuilder()->setOption('serialize', ['message','user','jwt','tenant']);
    }

    // an ecommerce user 
    public function customer()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEntity($this->request->getData());

        $tenant = null;
        $jwt = null;
        
        if ($this->Users->save($user)) {
            
            $message = true;
            
            $jwt = $this->Users->jwt($user->id, 0);
        } else {
            $message = false;
        }

        
        $this->set([
            'message' => $message,
            'user' => $user,
            
            'jwt' => $jwt
        ]);
        $this->viewBuilder()->setOption('serialize', ['message','user','jwt']);
    }
    

    public function profile(){
        $this->Authorization->skipAuthorization();
        $user = $this->request->getAttribute('identity');

        if ($user->tenant_id > 0) {
                $message = true;
                $jwt = $this->Users->jwt($user->id, $user->tenant_id);
                $tenant = $this->Users->Tenants->get($user->getOriginalData()->tenant_id,[
                    'contain' => 'Teams.Users'] );

                

                $this->set([
                'message' => $message,
                'jwt' => $jwt, 
                'user' => $user->getOriginalData(),
                'tenant' => $tenant, 
                
            ]);
            
            $this->viewBuilder()->setOption('serialize', ['message', 'jwt','user','tenant']);
            //return;
            }else{
                $message = true;
                $jwt = $this->Users->jwt($user->id, 0);
                    $this->set([
                    'message' => $message,
                    'jwt' => $jwt, 
                    'user' => $user->getOriginalData(),
                    
                ]);
                $this->viewBuilder()->setOption('serialize', ['message', 'jwt','user']);
            }
    }

    public function login()
    {
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        $user = $this->request->getAttribute('identity');

        
        //some users are not tenants
        if ($result->isValid()) {
            
            $message = true;
            if ($user->tenant_id > 0) {
                $jwt = $this->Users->jwt($user->id, $user->tenant_id);
                $tenant = $this->Users->Tenants->get($user->getOriginalData()->tenant_id,[
                    'contain' =>'Teams.Users']);

                $this->set([
                'message' => $message,
                'jwt' => $jwt, 
                'user' => $user->getOriginalData(),
                'tenant' => $tenant, 
            ]);
            
            $this->viewBuilder()->setOption('serialize', ['message', 'jwt','user','tenant']);
            //return;
            }else{
                $jwt = $this->Users->jwt($user->id, 0);
                    $this->set([
                    'message' => $message,
                    'jwt' => $jwt, 
                    'user' => $user->getOriginalData(),
                    
                ]);
                $this->viewBuilder()->setOption('serialize', ['message', 'jwt','user']);
            }
            
            
            
        }else{
            $this->log('foo');
            $message = false;
            $this->set(['message' => $message]);
            $this->viewBuilder()->setOption('serialize', ['message']);
            
        }
                
        
    }
   
}
