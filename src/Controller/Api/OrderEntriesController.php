<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;
use Cake\ORM\Query;
use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;


class OrderEntriesController extends ApiController
{
    use LogTrait;
	public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['interval]);
        //$this->Authorization->skipAuthorization(['login', 'add']);
    }

    public function online(){
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('staffIndex', $this->OrderEntries->find());
        $query->contain('Orders', function(Query $q){
            return $q->contain('Products');
        });
        $total = $query->count();

        if ($total > 0) {
            $message = true;
            $orderEntries = $query;
        }else{
            $message = false;
            $orderEntries = '';
        }
        $this->set([
            'orderEntries' => $orderEntries,
            'message' => $message,
            'reason' => false
        ]);

        $this->viewBuilder()->setOption('serialize', ['orderEntries', 'message']);
    }

    public function index(){
    	/*$this->paginate = [
            'contain' => ['Users','Orders'],
        ];*/
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('customerIndex', $this->OrderEntries->find());
        $query
       

        ->contain([
            'Stores',
            'Orders' => [
                'queryBuilder' => function (Query $q){
                        return $q->contain('Products');
                }
            ]
        ]);

        $last = $query->last();



        $total = $query->count();

        $this->log((string) $total);
        
        if ($total > 0) {
            $message = true;

            $tripsTable = TableRegistry::getTableLocator()->get('Trips');
            $trip = $tripsTable
                ->find()
                ->where(['Trips.order_entry_id' => $last->id])
                ->contain('Users')
                ->first();

        }else{
            $message = false;
            $trip = false;
        }

        $orderEntries = $last;

        if ($message == false) {
           $this->set([
                'orderEntry' => $orderEntries,
                'message' => $message,
                'reason' => false
            ]);

            $this->viewBuilder()->setOption('serialize', ['orderEntry', 'message', 'reason']);
        }else{
            $this->set([
            'orderEntry' => $orderEntries,
            'message' => $message,
            'trip' => $trip
            ]);

            $this->viewBuilder()->setOption('serialize', ['orderEntry', 'message', 'trip']);
        }

        
    }

    public function view($id = null)
    {
    	$orderEntry = $this->OrderEntries->get($id, [
    		//'contain' => ['Tenants']
    	]);
        $this->Authorization->authorize($orderEntry);


    	if ($orderEntry) {

    		$this->set([
    			'message' => true,
    			'orderEntry' => $orderEntry
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'orderEntry']);
    }

    public function edit($id){
        $this->request->allowMethod(['patch', 'post', 'put']);
        $identity = $this->request->getAttribute('identity');

        $orderEntry = $this->OrderEntries->get($id);
        $orderEntry->prepared_by = $identity->get('id');


        $this->Authorization->authorize($orderEntry,'update');
        $orderEntry = $this->OrderEntries->patchEntity($orderEntry, $this->request->getData());
        if ($this->OrderEntries->save($orderEntry)) {
            $message = true;
        }else{
            $message = false;
        }
        $this->set([
                'message' => $message,
                'orderEntry' => $orderEntry
            ]);
        $this->viewBuilder()->setOption('serialize', ['message','orderEntry']);
    }

    public function interval($interval = null){
    	$identity = $this->request->getAttribute('identity');
        $this->paginate = [
            'contain' => ['Users','Orders'],
        ];
        $query = $identity->applyScope('index', $this->OrderEntries->interval($interval));
        //$query = $this->OrderEntries->interval($interval);
        
        //$orderEntries = $this->paginate($query);
        $orders = $this->OrderEntries->orderValue($query);

        $da = $orders->unfold()->toList();
        $total = 0;
        $profit = 0;
        foreach ($da as $bar) {
            $total += $bar->total;
            $profit += $bar->nice_margin * $bar->qty;
        }
        

        $this->set([
    		
    		'message' => true,
    		'orderEntries' => $query,
            'total' => $total,
            'profit' => $profit
            
    	]);

    	$this->viewBuilder()->setOption('serialize', [
            
            'message', 
            'orderEntries', 
            'total',
            'profit'
        ]);
    }

    public function deliveryOnly(){
        $this->request->allowMethod(['post']);

        $identity = $this->request->getAttribute('identity');
        $this->Authorization->skipAuthorization();

        $whatData = new Collection($this->request->getData());
        $firstStop = $whatData->first();
        $lastStop = $whatData->last();
      
        //Not sure if this is correct. Setting up the init order entry to use in trips
        $anonOrderEntry = $this->OrderEntries->newEmptyEntity();
        $anonOrderEntry->user_id = $identity->getIdentifier();
        $anonOrderEntry->tenant_id = $identity->getTenantId();

        //I should modify below to not require store id for delivery only. Possible create start/end points in db
        $anonOrderEntry->store_id = 0;

        //July 2024 changed status to 3, which means it came from online and ready for dispatch
        $anonOrderEntry->status = 3;
        $anonOrderEntry->prepared_by = $identity->getIdentifier();
        $anonOrderEntry->amount = 0;
        $anonOrderEntry->end_lat = $lastStop['end_location']['lat'];
        $anonOrderEntry->end_lng = $lastStop['end_location']['lng']; 
        $anonOrderEntry->customer_id = 0;
        $anonOrderEntry->payment_type = 0;
       
       
        if ($this->OrderEntries->save($anonOrderEntry)) {
            $isTripSaved = $this->OrderEntries->Trips->multiDelivery($anonOrderEntry->id, $this->request->getData());
            $message = $isTripSaved;
        }else{
            $message = false;
        }  
        $this->set([
    		'message' => $message
    	]);

    	$this->viewBuilder()->setOption('serialize', [
            'message',
        ]);
    }
}