<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;

class InvitesController extends ApiController
{
	use LogTrait;
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index','view','add']);
        //$this->Authorization->skipAuthorization(['login', 'add']);
    }

    public function index()
    {
    	
    	$invites = $this->Invites->find('all');
    	$this->set([
    		'invites' => $invites,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['invites', 'message']);
    }

    public function view($id = null)
    {
    	$invite = $this->Invites->get($id, [
    		'contain' => ['Tenants']
    	]);


    	if ($invite) {

    		$this->set([
    			'message' => true,
    			'invite' => $invite
    		]);
    	}
    	$this->viewBuilder()->setOption('serialize', ['message', 'invite']);
    }

    public function add()
    {
    	$this->request->allowMethod(['post', 'put']);
    	$invite = $this->Invites->newEntity($this->request->getData());
    	if ($this->Invites->save($invite)) {
    		$message = true;
    	}else{
    		$message = false;
    	}

    	$this->set([
    		'message' => $message,
    		'invite' => $invite,
    	]);
    	$this->viewBuilder()->setOption('serialize', ['message', 'invite']);
    }
}