<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;


class CustomersController extends ApiController
{
	use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        
    }

    public function index()
    {
        
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->Customers->find());
        
        
        $customers = $this->paginate($query);
    	$this->set([
    		'customers' => $customers,
    		'message' => true
    	]);
    	$this->viewBuilder()->setOption('serialize', ['customers', 'message']);
    }

    public function add()
    {
    	
        $user_id = $this->request->getAttribute('identity')->getOriginalData()->id;
        $tenant_id = $this->request->getAttribute('identity')->getOriginalData()->tenant_id;
     
        
        $reqData = $this->request->getData();
        $customer = $this->Customers->newEntity($reqData);
        $customer->tenant_id = $tenant_id;
        $customer->user_id  = $user_id;

         
        $customer->store_id = $this->Customers->Stores->defaultTenantStore($tenant_id);
        
        $this->Authorization->authorize($customer,'create');
        $this->request->allowMethod(['post', 'put']);

        
        if ($this->Customers->save($customer)) {
            $message = true;
        }else{
            $message = false;
        }

        $this->set([
            'message' => $message,
            'customer' => $customer,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'customer']);
    }
}