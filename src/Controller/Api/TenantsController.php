<?php
declare(strict_types=1);

namespace App\Controller\Api;
use Cake\Log\LogTrait;

class TenantsController extends ApiController
{
	use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['query']);
        
    }

	public function query()
    {
    	$this->Authorization->skipAuthorization();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $query = $this->Tenants->find('name', ['tenantName' => $data['tenantName']]);
            //$this->log((string)print_r($data, true));

        }
        if($query->first()){
        	$message = true;
        	$tenant = $query->first();
        }else{
        	$message = false;
        	$tenant = '';
        }
        
        $this->set([
    		'message' => $message,
    		'tenant' => $tenant,
    	]);
        $this->viewBuilder()->setOption('serialize', ['message','tenant']);
    }
}



