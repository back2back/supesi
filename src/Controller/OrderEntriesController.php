<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * OrderEntries Controller
 *
 * @property \App\Model\Table\OrderEntriesTable $OrderEntries
 *
 * @method \App\Model\Entity\OrderEntry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrderEntriesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->OrderEntries->find());
        $this->paginate = [
            'contain' => ['Users', 'Tenants'],
        ];
        $orderEntries = $this->paginate($query);

        $this->set(compact('orderEntries'));
    }

    /**
     * View method
     *
     * @param string|null $id Order Entry id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderEntry = $this->OrderEntries->get($id, [
            'contain' => ['Users', 'Tenants', 'Orders'],
        ]);
        $this->Authorization->authorize($orderEntry);

        $this->set('orderEntry', $orderEntry);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orderEntry = $this->OrderEntries->newEmptyEntity();
        if ($this->request->is('post')) {
            $orderEntry = $this->OrderEntries->patchEntity($orderEntry, $this->request->getData());
            if ($this->OrderEntries->save($orderEntry)) {
                $this->Flash->success(__('The order entry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order entry could not be saved. Please, try again.'));
        }
        $users = $this->OrderEntries->Users->find('list', ['limit' => 200]);
        $tenants = $this->OrderEntries->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('orderEntry', 'users', 'tenants'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Entry id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderEntry = $this->OrderEntries->get($id, [
            'contain' => ['orders'],
        ]);
        $this->Authorization->authorize($orderEntry, 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderEntry = $this->OrderEntries->patchEntity($orderEntry, $this->request->getData());
            if ($this->OrderEntries->save($orderEntry)) {
                $this->Flash->success(__('The order entry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order entry could not be saved. Please, try again.'));
        }
        $users = $this->OrderEntries->Users->find('list', ['limit' => 200]);
        $tenants = $this->OrderEntries->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('orderEntry', 'users', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Entry id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderEntry = $this->OrderEntries->get($id);

        $this->Authorization->authorize($orderEntry);
        
        if ($this->OrderEntries->delete($orderEntry)) {
            $this->Flash->success(__('The order entry has been deleted.'));
        } else {
            $this->Flash->error(__('The order entry could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function interval($interval = null){
        $identity = $this->request->getAttribute('identity');
        $query = $identity->applyScope('index', $this->OrderEntries->interval($interval));
        $this->paginate = [
            'contain' => ['Users', 'Tenants'],
        ];
        $orderEntries = $this->paginate($query);

        $this->set(compact('orderEntries'));
    }

    public function delivery(){
        $this->Authorization->skipAuthorization();
        $session = $this->request->getSession();
        $jwt = $session->read('Config.jwt');
        $this->set('jwt', $jwt);
    }
}
