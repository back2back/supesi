<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;

/**
 * ProductImages Controller
 *
 * @property \App\Model\Table\ProductImagesTable $ProductImages
 *
 * @method \App\Model\Entity\ProductImage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductImagesController extends AppController
{
    use LogTrait;
    
    public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['add','index','delete']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $user = $this->request->getAttribute('identity');
        $query = $user->applyScope('index', $this->ProductImages->find());
        $this->paginate = [
            'contain' => ['Products'],
        ];
        $productImages = $this->paginate($query);

        $this->set(compact('productImages'));
    }

    /**
     * View method
     *
     * @param string|null $id Product Image id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->Authorization->skipAuthorization();
        $productImage = $this->ProductImages->get($id, [
            'contain' => ['Products'],
        ]);

        $this->set('productImage', $productImage);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($productId)
    {
        $user = $this->request->getAttribute('identity');
        
        $product = $this->ProductImages->Products->get($productId);
        
        $productImage = $this->ProductImages->newEmptyEntity();

        //only tenants  owner can add images 
        $this->Authorization->authorize($product, 'image');
        if ($this->request->is('post')) {
            $productImage = $this->ProductImages->patchEntity($productImage, $this->request->getData());

            $productImage->product_id = $productId;
            $productImage->user_id = $user->getOriginalData()->id;
            

            if ($this->ProductImages->save($productImage)) {
                $this->Flash->success(__('The product image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product image could not be saved. Please, try again.'));
        }
        
        $this->set(compact('productImage', 'product'));
    }



    /**
     * Delete method
     *
     * @param string|null $id Product Image id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        
        $this->request->allowMethod(['post', 'delete']);
        $productImage = $this->ProductImages->get($id);

        $this->Authorization->authorize($productImage);
        
        if ($this->ProductImages->delete($productImage)) {
            $this->Flash->success(__('The product image has been deleted.'));
        } else {
            $this->Flash->error(__('The product image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
