<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;
use Cake\Http\Cookie\Cookie;
use Cake\Http\Cookie\CookieCollection;
use Cake\I18n\FrozenTime;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    use LogTrait;
    
    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['login','add', 'logout','addCustomer','loginCustomer']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $user = $this->request->getAttribute('identity');

        $query = $user->applyScope('index', $this->Users->find());
 
        $this->set('users', $this->paginate($query));
        //$this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Tenants', 'Products', 'Stocks', 'Teams'],
        ]);
        //$this->Users->testFirebase($id);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            //debug($this->request->getData('Tenant.name'));
            $user->tenantName = ($this->request->getData('Tenant.name'));
            //debug($user);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Your account has been created. Please login'));

                return $this->redirect(['controller' => 'Users','action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        $identity = $this->request->getAttribute('identity');
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        // If the user is logged in send them away.
        if ($result->isValid()) {

            //all users need jwts for some js work
            $jwt = $this->Users->jwt($identity->getIdentifier(), 0);
            /* $cookie1 = (new Cookie('jwt'))
                ->withValue($jwt)
                ->withExpiry(new FrozenTime('+10 minute')); */

            //$this->response = $this->response->withCookie($cookie1);
            //$this->log(print_r($this->response, true));


            // Create a new collection
            //$cookies = new CookieCollection([$cookie1]);
            //$cookies = $cookies->add($cookie1);
            //$this->log(print_r($cookies, true));

            $session = $this->request->getSession();
            $session->write('Config.jwt', $jwt);

            //if is admin
            if ($identity->getOriginalData()->is_admin) {
                return $this->redirect(['action' => 'index', 'prefix' => 'admin']);
            }
            if ($identity->getOriginalData()->tenant_id > 0) {
                return $this->redirect(['action' => 'dashboard']);
            }
            //$target = $this->Authentication->getLoginRedirect() ?? '/products';
            //return $this->redirect($target);
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error('Invalid username or password');
        }
    }

    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $this->Authentication->logout();
        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }

    public function loginCustomer(){
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        

        if ($result->isValid()) {
            
            
            return $this->redirect(['controller' => 'Checkouts']);
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error('Invalid username or password');
        }
    }

    public function addCustomer(){

        
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            //debug($user);
            if ($this->Users->save($user)) {
                $result = $this->Authentication->getResult();
                if ($result->isValid()) {
                    $this->Flash->success(__('Your account has been created. Please login'));

                    return $this->redirect(['controller' => 'Checkouts','action' => 'index']);
                }
                $this->Flash->success(__('Your account has been created. Please login'));

                return $this->redirect(['controller' => 'Checkouts','action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function dashboard(){
        $this->Authorization->skipAuthorization(); 
        $session = $this->request->getSession();
        $jwt =    $session->read('Config.jwt', $jwt);
        $this->set('jwt', $jwt);
         
    }
}
