<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;

/**
 * StoreProducts Controller
 *
 * @property \App\Model\Table\StoreProductsTable $StoreProducts
 *
 * @method \App\Model\Entity\StoreProduct[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StoreProductsController extends AppController
{

    use LogTrait;
    
    public function initialize():void
    {
        parent::initialize();
        //$this->Authentication->allowUnauthenticated(['add','index','delete']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $user = $this->request->getAttribute('identity');

        $query = $user->applyScope('index', $this->StoreProducts->find());
        $this->paginate = [
            'contain' => ['Products', 'Stores', 'Users'],
        ];
        $storeProducts = $this->paginate($query);

        $this->set(compact('storeProducts'));
    }

    /**
     * View method
     *
     * @param string|null $id Store Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storeProduct = $this->StoreProducts->get($id, [
            'contain' => ['Products', 'Stores', 'Users'],
        ]);

        $this->set('storeProduct', $storeProduct);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $storeProduct = $this->StoreProducts->newEmptyEntity();
        if ($this->request->is('post')) {
            $storeProduct = $this->StoreProducts->patchEntity($storeProduct, $this->request->getData());
            if ($this->StoreProducts->save($storeProduct)) {
                $this->Flash->success(__('The store product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store product could not be saved. Please, try again.'));
        }
        $products = $this->StoreProducts->Products->find('list', ['limit' => 200]);
        $stores = $this->StoreProducts->Stores->find('list', ['limit' => 200]);
        $users = $this->StoreProducts->Users->find('list', ['limit' => 200]);
        $this->set(compact('storeProduct', 'products', 'stores', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Store Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storeProduct = $this->StoreProducts->get($id, [
            'contain' => [],
        ]);
        $this->Authorization->authorize($storeProduct, 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storeProduct = $this->StoreProducts->patchEntity($storeProduct, $this->request->getData());
            if ($this->StoreProducts->save($storeProduct)) {
                $this->Flash->success(__('The store product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store product could not be saved. Please, try again.'));
        }
        $isOnline = ['Offline', 'Online'];
        $this->set(compact('storeProduct', 'isOnline'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Store Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storeProduct = $this->StoreProducts->get($id);
        if ($this->StoreProducts->delete($storeProduct)) {
            $this->Flash->success(__('The store product has been deleted.'));
        } else {
            $this->Flash->error(__('The store product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
