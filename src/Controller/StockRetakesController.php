<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Event\EventInterface;
use Cake\Core\Configure;
use Cake\Log\LogTrait;

/**
 * StockRetakes Controller
 *
 * @property \App\Model\Table\StockRetakesTable $StockRetakes
 *
 * @method \App\Model\Entity\StockRetake[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StockRetakesController extends AppController
{

    use LogTrait;
    
    public function beforeFilter(EventInterface $event){
        
        $this->Authentication->allowUnauthenticated(['view','index']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $user = $this->request->getAttribute('identity');
        $query = $user->applyScope('index', $this->StockRetakes->find());
        $this->paginate = [
            'contain' => ['Stocks', 'Users'],
        ];
        $stockRetakes = $this->paginate($query);

        $this->set(compact('stockRetakes'));
    }

    /**
     * View method
     *
     * @param string|null $id Stock Retake id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $stockRetake = $this->StockRetakes->get($id, [
            'contain' => ['Stocks', 'Users'],
        ]);

        $this->set('stockRetake', $stockRetake);
    }

    /**
     * Add method
     *
     *@param int|null stockId
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($stockId = null)
    {
        
        $stockRetake = $this->StockRetakes->newEmptyEntity();
        $user = $this->request->getAttribute('identity');
        $stockRetake->user_id = $user->getIdentifier();

        $whatStock = $this->StockRetakes->Stocks->get($stockId);
        
        $stockRetake->stock_id = $whatStock->id;
        $this->Authorization->authorize($whatStock, 'create');
       
        if ($this->request->is('post')) {
            $stockRetake = $this->StockRetakes->patchEntity($stockRetake, $this->request->getData());
            if ($this->StockRetakes->save($stockRetake)) {
                $this->Flash->success(__('The stock retake has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stock retake could not be saved. Please, try again.'));
        }
       
        
        $foo = Configure::read('StockRetakes');
        $this->set(compact('stockRetake','foo'));
    }

   

}
