<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Log\LogTrait;

/**
 * GuestCarts Controller
 *
 * @property \App\Model\Table\GuestCartsTable $GuestCarts
 *
 * @method \App\Model\Entity\GuestCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GuestCartsController extends AppController
{
    use LogTrait;

    public function initialize():void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index', 'view','cart','addToCart']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sessions', 'Products'],
        ];
        $guestCarts = $this->paginate($this->GuestCarts);

        $this->set(compact('guestCarts'));
    }

    /**
     * View method
     *
     * @param string|null $id Guest Cart id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $guestCart = $this->GuestCarts->get($id, [
            'contain' => ['Sessions', 'Products'],
        ]);

        $this->set('guestCart', $guestCart);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $guestCart = $this->GuestCarts->newEmptyEntity();
        if ($this->request->is('post')) {
            $guestCart = $this->GuestCarts->patchEntity($guestCart, $this->request->getData());
            if ($this->GuestCarts->save($guestCart)) {
                $this->Flash->success(__('The guest cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The guest cart could not be saved. Please, try again.'));
        }
        $sessions = $this->GuestCarts->Sessions->find('list', ['limit' => 200]);
        $products = $this->GuestCarts->Products->find('list', ['limit' => 200]);
        $this->set(compact('guestCart', 'sessions', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Guest Cart id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $guestCart = $this->GuestCarts->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $guestCart = $this->GuestCarts->patchEntity($guestCart, $this->request->getData());
            if ($this->GuestCarts->save($guestCart)) {
                $this->Flash->success(__('The guest cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The guest cart could not be saved. Please, try again.'));
        }
        $sessions = $this->GuestCarts->Sessions->find('list', ['limit' => 200]);
        $products = $this->GuestCarts->Products->find('list', ['limit' => 200]);
        $this->set(compact('guestCart', 'sessions', 'products'));
    }

    
}
