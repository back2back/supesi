<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * AppCategories Controller
 *
 * @property \App\Model\Table\AppCategoriesTable $AppCategories
 *
 * @method \App\Model\Entity\AppCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AppCategoriesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $appCategories = $this->paginate($this->AppCategories);

        $this->set(compact('appCategories'));
    }

    /**
     * View method
     *
     * @param string|null $id App Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $appCategory = $this->AppCategories->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('appCategory', $appCategory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $appCategory = $this->AppCategories->newEmptyEntity();
        if ($this->request->is('post')) {
            $appCategory = $this->AppCategories->patchEntity($appCategory, $this->request->getData());
            if ($this->AppCategories->save($appCategory)) {
                $this->Flash->success(__('The app category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The app category could not be saved. Please, try again.'));
        }
        $users = $this->AppCategories->Users->find('list', ['limit' => 200]);
        $this->set(compact('appCategory', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id App Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $appCategory = $this->AppCategories->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $appCategory = $this->AppCategories->patchEntity($appCategory, $this->request->getData());
            if ($this->AppCategories->save($appCategory)) {
                $this->Flash->success(__('The app category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The app category could not be saved. Please, try again.'));
        }
        $users = $this->AppCategories->Users->find('list', ['limit' => 200]);
        $this->set(compact('appCategory', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id App Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $appCategory = $this->AppCategories->get($id);
        if ($this->AppCategories->delete($appCategory)) {
            $this->Flash->success(__('The app category has been deleted.'));
        } else {
            $this->Flash->error(__('The app category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
