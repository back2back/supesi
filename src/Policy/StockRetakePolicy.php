<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\StockRetake;
use App\Model\Entity\Stock;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use App\AuthRules\AuthRules;

/**
 * StockRetake policy
 */
class StockRetakePolicy
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create StockRetake
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StockRetake $stockRetake
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Stock $stock)
    {
        
        
    }

    /**
     * Check if $user can update StockRetake
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StockRetake $stockRetake
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, StockRetake $stockRetake)
    {
    }

    /**
     * Check if $user can delete StockRetake
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StockRetake $stockRetake
     * @return bool
     */
    public function canDelete(IdentityInterface $user, StockRetake $stockRetake)
    {
    }

    /**
     * Check if $user can view StockRetake
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StockRetake $stockRetake
     * @return bool
     */
    public function canView(IdentityInterface $user, StockRetake $stockRetake)
    {
    }
}
