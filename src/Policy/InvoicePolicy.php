<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Invoice;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use App\AuthRules\AuthRules;

/**
 * Invoice policy
 */
class InvoicePolicy
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Invoice
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invoice $invoice
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Invoice $invoice)
    {
        $auth = new AuthRules();
        $foo = $auth->teamRole($user);
        if ($foo === 1) {
            return true;
        }else{
            return false;
        } 
    }

    /**
     * Check if $user can update Invoice
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invoice $invoice
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Invoice $invoice)
    {
        if ($user->id == $invoice->user_id) {
            return true;
        }
    }

    /**
     * Check if $user can delete Invoice
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invoice $invoice
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Invoice $invoice)
    {
    }

    /**
     * Check if $user can view Invoice
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invoice $invoice
     * @return bool
     */
    public function canView(IdentityInterface $user, Invoice $invoice)
    {
    }
}
