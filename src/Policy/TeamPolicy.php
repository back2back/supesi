<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Team;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;


/**
 * Team policy
 */
class TeamPolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Team
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Team $team
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Team $team)
    {
    }

    /**
     * Check if $user can update Team
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Team $team
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Team $team)
    {
        return $this->orgRole($user, $team);
    }

    /**
     * Check if $user can delete Team
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Team $team
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Team $team)
    {
    }

    /**
     * Check if $user can view Team
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Team $team
     * @return bool
     */
    public function canView(IdentityInterface $user, Team $team)
    {
        //with a role only owner can view
        return $this->orgRole($user, $team);
      
        
    }

    public function orgRole($user, $team){
        $teamTenantId = $team->tenant_id;
        
        if ($teamTenantId == $user->tenant_id) {
            //check if I'm owner
            if ($user->getTeam()->org_role === 1) {
                return true;
            }
            
        }        
        return false;
    }

}
