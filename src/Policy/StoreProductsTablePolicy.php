<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\StoreProductsTable;
use Authorization\IdentityInterface;

/**
 * StoreProducts policy
 */
class StoreProductsTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['Stores.user_id' => $user->getOriginalData()->id]);
	}

	public function scopeAdminIndex($user, $query){
		return $query;
	}
}
