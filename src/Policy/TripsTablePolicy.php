<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\TripsTable;
use Authorization\IdentityInterface;

/**
 * TripsTable policy
 */
class TripsTablePolicy
{
	public function scopeAdminIndex($user, $query)
	{
		// make this query return trips in groups
		return $query
			->where(['Trips.status' => 0])
			->contain(['OrderEntries']);
			
	}

	public function scopeIndex($user, $query){
		$userId = $user->getOriginalData()->id;
		//modify to ensure user is a rider
		return $query->where(
			['Trips.user_id' => $userId,'Trips.status' => 0],		
		);
	}
}
