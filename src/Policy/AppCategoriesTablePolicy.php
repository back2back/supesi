<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\AppCategoriesTable;
use Authorization\IdentityInterface;

/**
 * AppCategories policy
 */
class AppCategoriesTablePolicy
{
	public function scopeAdminIndex($user, $query)
	{
		return $query;
	}
}
