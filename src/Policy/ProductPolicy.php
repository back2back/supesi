<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Product;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use App\AuthRules\AuthRules;

/**
 * Product policy
 */
class ProductPolicy implements BeforePolicyInterface
{
    
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Product
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Product $product
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Product $product)
    {
        //owner can create products
        //the product object does not exist
       
        $auth = new AuthRules();
        $foo = $auth->teamRole($user);
        if ($foo === 1) {
            return true;
        }else{
            return false;
        }       
    }

    /**
     * Check if $user can update Product
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Product $product
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Product $product)
    {
        //return $this->orgRole($user, $product);
        $auth = new AuthRules();
        return $auth->onResource($user, $product);

    }

    /**
     * Check if $user can delete Product
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Product $product
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Product $product)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $product);
    }

    /**
     * Check if $user can view Product
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Product $product
     * @return bool
     */
    public function canView(IdentityInterface $user, Product $product)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $product);
    }
    
    public function canImage(IdentityInterface $user, Product $product){
        $auth = new AuthRules();
        return $auth->onResource($user, $product);
    }

    
}
