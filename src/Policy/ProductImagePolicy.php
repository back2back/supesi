<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\ProductImage;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use App\AuthRules\AuthRules;
use Cake\Log\LogTrait;

/**
 * ProductImage policy
 */
class ProductImagePolicy implements BeforePolicyInterface
{
    use LogTrait;
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create ProductImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ProductImage $productImage
     * @return bool
     */
    public function canCreate(IdentityInterface $user, ProductImage $productImage)
    {
        $this->log((string) $productImage->user_id);
        $auth = new AuthRules();
        //return $auth->onResource($user, $productImage);
        $foo = $auth->teamRole($user);
        if ($foo === 1) {
            return true;
        }else{
            return false;
        }
    }

   

    /**
     * Check if $user can delete ProductImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ProductImage $productImage
     * @return bool
     */
    public function canDelete(IdentityInterface $user, ProductImage $productImage)
    {
        if ($user->id === $productImage->user_id) {
            return true;
        }
    }

    /**
     * Check if $user can view ProductImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ProductImage $productImage
     * @return bool
     */
    public function canView(IdentityInterface $user, ProductImage $productImage)
    {
    }


}
