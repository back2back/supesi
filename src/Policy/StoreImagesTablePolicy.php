<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\StoreImagesTable;
use Authorization\IdentityInterface;

/**
 * StoreImages policy
 */
class StoreImagesTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['user_id' => $user->getOriginalData()->id]);
	}
}
