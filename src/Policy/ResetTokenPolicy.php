<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\ResetToken;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * ResetToken policy
 */
class ResetTokenPolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    
    /**
     * Check if $user can create ResetToken
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ResetToken $resetToken
     * @return bool
     */
    public function canCreate(IdentityInterface $user, ResetToken $resetToken)
    {
        //everyone can create
        return true;
    }

    /**
     * Check if $user can update ResetToken
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ResetToken $resetToken
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, ResetToken $resetToken)
    {
        // i can update my token
    }

    /**
     * Check if $user can delete ResetToken
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ResetToken $resetToken
     * @return bool
     */
    public function canDelete(IdentityInterface $user, ResetToken $resetToken)
    {
    }

    /**
     * Check if $user can view ResetToken
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\ResetToken $resetToken
     * @return bool
     */
    public function canView(IdentityInterface $user, ResetToken $resetToken)
    {
        return true;
    }

    
}
