<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\OrderEntriesTable;
use Authorization\IdentityInterface;

/**
 * OrderEntries policy
 */
class OrderEntriesTablePolicy
{
	public function scopeIndex($user, $query){
		return $query
			->where([
				'OrderEntries.tenant_id' => $user->getOriginalData()->get('tenant_id'),
				//'OrderEntries.status' => 1,
			]);
	}
	public function scopeCustomerIndex($user, $query){
		return $query
			->where([
				'OrderEntries.user_id' => $user->getOriginalData()->get('id'),
				
			]);
	}
	public function scopeStaffIndex($user, $query){
		return $query
			->where([
				'OrderEntries.status' => 1,
				//'OrderEntries.user_id' => $user->getOriginalData()->get('id'),
				
			]);
	}

	public function scopeAdminIndex($user, $query)
	{
		return $query
			->where([
				'OrderEntries.status' => 1,
			]);
	}
}
