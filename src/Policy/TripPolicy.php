<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Trip;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use Cake\Log\LogTrait;

/**
 * Trip policy
 */
class TripPolicy implements BeforePolicyInterface
{

    use LogTrait;
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            if ($resource->status == 0) {
               return true;
            }
            return false;
        }
                
    }

    /**
     * Check if $user can create Trip
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Trip $trip
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Trip $trip)
    {
    }

    /**
     * Check if $user can update Trip
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Trip $trip
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Trip $trip)
    {

        // only riders car edit the trip, i.e claim a trip in pool
        $userRole = $user->getOriginalData()->role;
        
        if ($userRole == 1 && $trip->status == 0) {
            return true;
        }else{
            return false;
        }

    }

    /**
     * Check if $user can delete Trip
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Trip $trip
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Trip $trip)
    {
    }

    /**
     * Check if $user can view Trip
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Trip $trip
     * @return bool
     */
    public function canView(IdentityInterface $user, Trip $trip)
    {
    }
}
