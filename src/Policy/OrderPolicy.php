<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Order;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Order policy
 */
class OrderPolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Order
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Order $order
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Order $order)
    {
    }

    /**
     * Check if $user can update Order
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Order $order
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Order $order)
    {
    }

    /**
     * Check if $user can delete Order
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Order $order
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Order $order)
    {
    }

    /**
     * Check if $user can view Order
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Order $order
     * @return bool
     */
    public function canView(IdentityInterface $user, Order $order)
    {
    }

    
}
