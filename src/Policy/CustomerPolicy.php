<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Customer;
use Authorization\IdentityInterface;

/**
 * Customer policy
 */
class CustomerPolicy
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Customer
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Customer $customer
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Customer $customer)
    {
        //any tenant can create customer with the store id
        return true;
    }

    /**
     * Check if $user can update Customer
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Customer $customer
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Customer $customer)
    {
    }

    /**
     * Check if $user can delete Customer
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Customer $customer
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Customer $customer)
    {
    }

    /**
     * Check if $user can view Customer
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Customer $customer
     * @return bool
     */
    public function canView(IdentityInterface $user, Customer $customer)
    {
    }
}
