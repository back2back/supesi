<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\StockRetakesTable;
use Authorization\IdentityInterface;

/**
 * StockRetakes policy
 */
class StockRetakesTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['StockRetakes.user_id' => $user->getOriginalData()->get('id')]);
	}
}
