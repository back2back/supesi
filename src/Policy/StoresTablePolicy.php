<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\StoresTable;
use Authorization\IdentityInterface;

/**
 * Stores policy
 */
class StoresTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['Stores.tenant_id' => $user->getOriginalData()->get('tenant_id')]);
	}

	public function scopeAdminIndex($user, $query){
		return $query;
	}
}
