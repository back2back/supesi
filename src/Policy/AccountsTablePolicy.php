<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\AccountsTable;
use Authorization\IdentityInterface;

/**
 * Accounts policy
 */
class AccountsTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['user_id' => $user->getOriginalData()->get('id')]);
	}
	public function scopeAdminIndex($user, $query){
		return $query;
	}
}
