<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\CustomersTable;
use Authorization\IdentityInterface;

/**
 * Customers policy
 */
class CustomersTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['Customers.tenant_id' => $user->getOriginalData()->tenant_id]);
	}
	public function scopeAdminIndex($user, $query){
		return $query;
	}
}
