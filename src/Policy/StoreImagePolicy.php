<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\StoreImage;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * StoreImage policy
 */
class StoreImagePolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create StoreImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreImage $storeImage
     * @return bool
     */
    public function canCreate(IdentityInterface $user, StoreImage $storeImage)
    {
    }

    /**
     * Check if $user can update StoreImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreImage $storeImage
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, StoreImage $storeImage)
    {
    }

    /**
     * Check if $user can delete StoreImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreImage $storeImage
     * @return bool
     */
    public function canDelete(IdentityInterface $user, StoreImage $storeImage)
    {
        return true;
    }

    /**
     * Check if $user can view StoreImage
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreImage $storeImage
     * @return bool
     */
    public function canView(IdentityInterface $user, StoreImage $storeImage)
    {
        return true;
    }
}
