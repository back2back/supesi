<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Account;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Account policy
 */
class AccountPolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Account
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Account $account
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Account $account)
    {
    }

    /**
     * Check if $user can update Account
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Account $account
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Account $account)
    {
    }

    /**
     * Check if $user can delete Account
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Account $account
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Account $account)
    {
    }

    /**
     * Check if $user can view Account
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Account $account
     * @return bool
     */
    public function canView(IdentityInterface $user, Account $account)
    {
    }
}
