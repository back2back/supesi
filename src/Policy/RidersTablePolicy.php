<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\RidersTable;
use Authorization\IdentityInterface;

/**
 * Riders policy
 */
class RidersTablePolicy
{
	public function scopeAdminIndex($user, $query)
	{
		return $query;
	}
}
