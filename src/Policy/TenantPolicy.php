<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Tenant;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Tenant policy
 */
class TenantPolicy implements BeforePolicyInterface
{
    
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Tenant
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Tenant $tenant
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Tenant $tenant)
    {
    }

    /**
     * Check if $user can update Tenant
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Tenant $tenant
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Tenant $tenant)
    {
    }

    /**
     * Check if $user can delete Tenant
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Tenant $tenant
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Tenant $tenant)
    {
    }

    /**
     * Check if $user can view Tenant
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Tenant $tenant
     * @return bool
     */
    public function canView(IdentityInterface $user, Tenant $tenant)
    {
    }
}
