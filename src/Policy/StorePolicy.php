<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Store;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use App\AuthRules\AuthRules;

/**
 * Store policy
 */
class StorePolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Store
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Store $store
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Store $store)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $store);
    }

    /**
     * Check if $user can update Store
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Store $store
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Store $store)
    {
    }

    /**
     * Check if $user can delete Store
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Store $store
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Store $store)
    {
    }

    /**
     * Check if $user can view Store
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Store $store
     * @return bool
     */
    public function canView(IdentityInterface $user, Store $store)
    {
        return true;
    }
    public function canImage(IdentityInterface $user, Store $store){
        $auth = new AuthRules();
        return $auth->onResource($user, $store);
    }
}
