<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Subscription;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Subscription policy
 */
class SubscriptionPolicy implements BeforePolicyInterface
{
    
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Subscription
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Subscription $subscription
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Subscription $subscription)
    {
    }

    /**
     * Check if $user can update Subscription
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Subscription $subscription
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Subscription $subscription)
    {
    }

    /**
     * Check if $user can delete Subscription
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Subscription $subscription
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Subscription $subscription)
    {
    }

    /**
     * Check if $user can view Subscription
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Subscription $subscription
     * @return bool
     */
    public function canView(IdentityInterface $user, Subscription $subscription)
    {
    }
}
