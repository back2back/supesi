<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\StoreProduct;
use Authorization\IdentityInterface;

/**
 * StoreProduct policy
 */
class StoreProductPolicy
{
    /**
     * Check if $user can create StoreProduct
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreProduct $storeProduct
     * @return bool
     */
    public function canCreate(IdentityInterface $user, StoreProduct $storeProduct)
    {
    }

    /**
     * Check if $user can update StoreProduct
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreProduct $storeProduct
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, StoreProduct $storeProduct)
    {
        // for now let the owner update. Later maybe restrict to admins
        if ($user->id == $storeProduct->user_id) {
            return true;
        }
    }

    /**
     * Check if $user can delete StoreProduct
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreProduct $storeProduct
     * @return bool
     */
    public function canDelete(IdentityInterface $user, StoreProduct $storeProduct)
    {
    }

    /**
     * Check if $user can view StoreProduct
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\StoreProduct $storeProduct
     * @return bool
     */
    public function canView(IdentityInterface $user, StoreProduct $storeProduct)
    {
    }
}
