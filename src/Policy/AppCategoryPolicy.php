<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\AppCategory;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * AppCategory policy
 */
class AppCategoryPolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }

    /**
     * Check if $user can create AppCategory
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\AppCategory $appCategory
     * @return bool
     */
    public function canCreate(IdentityInterface $user, AppCategory $appCategory)
    {
    }

    /**
     * Check if $user can update AppCategory
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\AppCategory $appCategory
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, AppCategory $appCategory)
    {
    }

    /**
     * Check if $user can delete AppCategory
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\AppCategory $appCategory
     * @return bool
     */
    public function canDelete(IdentityInterface $user, AppCategory $appCategory)
    {
    }

    /**
     * Check if $user can view AppCategory
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\AppCategory $appCategory
     * @return bool
     */
    public function canView(IdentityInterface $user, AppCategory $appCategory)
    {
        return true;
    }
}
