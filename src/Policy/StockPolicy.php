<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Stock;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Stock policy
 */
class StockPolicy implements BeforePolicyInterface
{
    
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Stock
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Stock $stock
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Stock $stock)
    {
        if ($user->id == $stock->user_id) {
           return true;
        }else{
            return false;
        }
    }

    /**
     * Check if $user can update Stock
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Stock $stock
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Stock $stock)
    {
        //for now only user who created stock can edit
        if ($user->id == $stock->user_id) {
           return true;
        }
    }

    /**
     * Check if $user can delete Stock
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Stock $stock
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Stock $stock)
    {
    }

    /**
     * Check if $user can view Stock
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Stock $stock
     * @return bool
     */
    public function canView(IdentityInterface $user, Stock $stock)
    {
    }
}
