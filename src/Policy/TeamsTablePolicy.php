<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\TeamsTable;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Teams policy
 */
class TeamsTablePolicy implements BeforePolicyInterface
{
	public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        // fall through
    }

	public function scopeIndex($user, $query){
		return $query->where(['Teams.tenant_id' => $user->getOriginalData()->get('tenant_id')]);
	}

    public function scopeAdminIndex($user, $query)
    {
        $query->group('Teams.tenant_id');
        return $query;
    }
}
