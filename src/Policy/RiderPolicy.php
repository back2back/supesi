<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Rider;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * Rider policy
 */
class RiderPolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Rider
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Rider $rider
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Rider $rider)
    {
    }

    /**
     * Check if $user can update Rider
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Rider $rider
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Rider $rider)
    {
        if ($user->id == $rider->user_id) {
            return true;
        }
    }

    /**
     * Check if $user can delete Rider
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Rider $rider
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Rider $rider)
    {
    }

    /**
     * Check if $user can view Rider
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Rider $rider
     * @return bool
     */
    public function canView(IdentityInterface $user, Rider $rider)
    {
    }
}
