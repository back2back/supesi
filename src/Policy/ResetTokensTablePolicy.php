<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\ResetTokensTable;
use Authorization\IdentityInterface;

/**
 * ResetTokens policy
 */
class ResetTokensTablePolicy
{
	public function scopeIndex($user, $query){
		//only view my tokens
		return $query->where(['ResetTokens.user_id' => $user->getOriginalData()->get('id')]);
	}
	public function scopeAdminIndex($user, $query)
	{
		//admin gets all data in table
		return $query;
	}

	
	
}
