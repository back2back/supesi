<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\ProductsTable;
use Authorization\IdentityInterface;

/**
 * Products policy
 */
class ProductsTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['Products.tenant_id' => $user->getOriginalData()->get('tenant_id')]);
	}
}
