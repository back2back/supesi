<?php
declare(strict_types=1);

namespace App\Policy;

use App\AuthRules\AuthRules;
use App\Model\Entity\OrderEntry;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;

/**
 * OrderEntry policy
 */
class OrderEntryPolicy implements BeforePolicyInterface
{
    

    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create OrderEntry
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\OrderEntry $orderEntry
     * @return bool
     */
    public function canCreate(IdentityInterface $user, OrderEntry $orderEntry)
    {
        
       
    }

    /**
     * Check if $user can update OrderEntry
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\OrderEntry $orderEntry
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, OrderEntry $orderEntry)
    {
        //tenants can update
        if ($user->tenant_id == $orderEntry->tenant_id) {
            return true;
        }else{
            return false;
        }
        //$auth = new AuthRules();
        //return $auth->onResource($user, $orderEntry);
    }

    /**
     * Check if $user can delete OrderEntry
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\OrderEntry $orderEntry
     * @return bool
     */
    public function canDelete(IdentityInterface $user, OrderEntry $orderEntry)
    {
    }

    /**
     * Check if $user can view OrderEntry
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\OrderEntry $orderEntry
     * @return bool
     */
    public function canView(IdentityInterface $user, OrderEntry $orderEntry)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $orderEntry);
    }
}
