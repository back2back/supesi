<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\StocksTable;
use Authorization\IdentityInterface;

/**
 * Stocks policy
 */
class StocksTablePolicy
{
	public function scopeAdminIndex($user, $query)
	{
		return $query;
	}
	public function scopeIndex($user, $query)
	{
		return $query->where(['Stocks.tenant_id' => $user->getOriginaldata()->tenant_id]);
	}

	public function scopeCounted($user, $query){
		return 
		$query->where(
			[
				'Stocks.counted > ' => 0,
				'Stocks.tenant_id' => $user->getOriginaldata()->tenant_id
			]);
		
	}
}
