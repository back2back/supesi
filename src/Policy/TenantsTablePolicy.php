<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\TenantsTable;
use Authorization\IdentityInterface;

/**
 * Tenants policy
 */
class TenantsTablePolicy
{
	public function scopeAdminIndex($user, $query)
	{
		return $query;
	}
}
