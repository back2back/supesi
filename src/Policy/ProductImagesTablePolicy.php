<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\ProductImagesTable;
use Authorization\IdentityInterface;

/**
 * ProductImages policy
 */
class ProductImagesTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['ProductImages.user_id' => $user->getOriginalData()->get('id')]);
	}
}
