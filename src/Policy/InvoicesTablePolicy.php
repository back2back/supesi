<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\InvoicesTable;
use Authorization\IdentityInterface;

/**
 * Invoices policy
 */
class InvoicesTablePolicy
{
    public function scopeIndex($user, $query){
        return $query->where(['Invoices.user_id' => $user->getOriginalData()->get('id')]);
    }
}
