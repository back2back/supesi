<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\InvitesTable;
use Authorization\IdentityInterface;

/**
 * Invites policy
 */
class InvitesTablePolicy
{
	public function scopeIndex($user, $query){
		return $query->where(['user_id' => $user->getOriginalData()->get('id')]);
	}
}
