<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Invite;
use Authorization\IdentityInterface;
use Authorization\Policy\BeforePolicyInterface;
use App\AuthRules\AuthRules;

/**
 * Invite policy
 */
class InvitePolicy implements BeforePolicyInterface
{
    public function before($user, $resource, $action)
    {
        if ($user->getOriginalData()->is_admin) {
            return true;
        }
        
    }
    /**
     * Check if $user can create Invite
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invite $invite
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Invite $invite)
    {
        $auth = new AuthRules();
        $foo = $auth->teamRole($user);
        if ($foo === 1) {
            return true;
        }else{
            return false;
        } 
    }

    /**
     * Check if $user can update Invite
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invite $invite
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Invite $invite)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $invite);
    }

    /**
     * Check if $user can delete Invite
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invite $invite
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Invite $invite)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $invite);
    }

    /**
     * Check if $user can view Invite
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Invite $invite
     * @return bool
     */
    public function canView(IdentityInterface $user, Invite $invite)
    {
        $auth = new AuthRules();
        return $auth->onResource($user, $invite);
    }
}
