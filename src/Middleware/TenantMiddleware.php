<?php

namespace App\Middleware;

use Cake\Http\Cookie\Cookie;
use Cake\I18n\FrozenTime;
use Cake\I18n\FrozenDate;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;

use Cake\Log\LogTrait;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Controller\Controller;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

class TenantMiddleware implements MiddlewareInterface
{
    use LogTrait;

 
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {
        
        $identity = $request
            ->getAttribute('identity');

        
        //if we are logged in
        if ($identity != null) {
            $isAdmin = $identity->is_admin;

            //admins don't check subscription
            if ($isAdmin) {
                return $handler->handle($request);;
            }

            $guest = $identity->getOriginalData()->get('tenant_id');
            if ($guest == 0) {
                return $handler->handle($request);
            }

            $tenantsTable = TableRegistry::getTableLocator()->get('Tenants');
            $data = $tenantsTable->get($identity->getOriginalData()->get('tenant_id'), [
                'contain' => 'Subscriptions']);

            $now = FrozenTime::now();
            

            $time = ($data->subscription->expires_on);
            if ($now->gt($time)) {
            
                
                $request = $request->withAttribute('expired', true);
               
                $responseA = 
                $handler->handle($request);
                    
                   
                return $responseA;   
                
                         
            }
      
            return $handler->handle($request);
                
        }
        return $handler->handle($request);

        
    }
}