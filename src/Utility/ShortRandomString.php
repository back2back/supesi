<?php
namespace App\Utility;

/**
 * 
 */
class ShortRandomString 
{
	
	public function random($length){

	//length should be shorter than chars len 15 chars 26
       
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz@#$&*";
        $size = strlen( $chars );

        if ($size < $length) {
            return;
        }

        $str = '';
        for( $i = 0; $i < $length; $i++ ) {  
            $str .= $chars[ rand( 0, $size - 1 ) ];  
            
        }

        return base64_encode($str);
	}
}