<?php
namespace App\Utility;

use Cake\Log\LogTrait;
use Cake\Http\Client;

class TripCostRounder{

use LogTrait;

public function roundOff($amount){

		return round($amount, -1);
	}
}