<?php
namespace App\Utility;

use Cake\Log\LogTrait;
use Cake\Http\Client;

class TripCalculator{

use LogTrait;

CONST MIN_FARE = 130;
CONST RATE_PER_M = 0.015;

public function getFare(
	$startLat,
	$startLng,
	$endLat,
	$endLng
	){

	
	//define('RATE_M', '0.015');
	//define('BASE_FARE', '130');
	$RATE_M = 0.015;
	$BASE_FARE = 130;
	

	$http = new Client();
	$mapsUrl = 'https://maps.googleapis.com/maps/api/directions/json';
	$json = $http->get($mapsUrl, 
		[
			'origin' => $startLat.','.$startLng,
			'destination' => $endLat.','.$endLng,
			'key' => 'AIzaSyB6XSYMWmOrhDyjx2jc3HvZ2GOa7iJOeB0'
		],
		
	);
	
	$json = $json->getStringBody();
	
	
	$foobar = json_decode($json);
	$routes = $foobar->routes;
	$status = $foobar->status;

	if ($status ==='OK') {
		
		$distance = '';
		$time = '';
		foreach ($routes as $value) {
			$legs = $value->legs;

			foreach($legs as $babes){
				$distance = $babes->distance;
				$time = $babes->duration;
			}
		}


		
		$beforeBase = $distance->value* $RATE_M;
		$afterBase = round($beforeBase, -1) + $BASE_FARE;

		$this->log((string) $afterBase);
		
		return [true, $distance->value, $afterBase];
	}
	
	return [false];
	

	}

}