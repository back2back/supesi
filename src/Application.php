<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App;

use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\BaseApplication;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;

use Authentication\AuthenticationService;
use Authentication\AuthenticationServiceInterface;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Middleware\AuthenticationMiddleware;
use Psr\Http\Message\ServerRequestInterface;

use Authorization\AuthorizationService;
use Authorization\AuthorizationServiceInterface;
use Authorization\AuthorizationServiceProviderInterface;
use Authorization\Middleware\AuthorizationMiddleware;
use Authorization\Policy\OrmResolver;
use Psr\Http\Message\ResponseInterface;

use Cake\Utility\Security;

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Http\Middleware\BodyParserMiddleware;

use App\Middleware\TenantMiddleware;
use App\Middleware\TimezoneMiddleware;


/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication implements AuthenticationServiceProviderInterface, AuthorizationServiceProviderInterface
{
    /**
     * Load all the application configuration and bootstrap logic.
     *
     * @return void
     */
    public function bootstrap(): void
    {
        $this->addPlugin('Josegonzalez/Upload');

        $this->addPlugin('Migrations');
        $this->addPlugin('Mailgun');

        // Call parent to load bootstrap from files.
        parent::bootstrap();

        if (PHP_SAPI === 'cli') {
            $this->bootstrapCli();
        }

        // Load more plugins here
        $this->addPlugin('Authentication');
        $this->addPlugin('Authorization');
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {
        
        $csrf = new CsrfProtectionMiddleware();

        
        $csrf->skipCheckCallback(function ($request) {
            
            if ($request->getParam('prefix') === 'Api') {
                return true;
            }
        });

        $middlewareQueue
            
            ->add(new ErrorHandlerMiddleware(Configure::read('Error')))
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime'),
            ]))
            ->add(new RoutingMiddleware($this))
            ->add($csrf)
            ->add(new BodyParserMiddleware())
            ->add(new AuthenticationMiddleware($this))
            //valid tenant data middleware
            ->add(new TenantMiddleware())

           
           
           

            ->add(new AuthorizationMiddleware($this));

        return $middlewareQueue;
    }

    /**
     * Bootrapping for CLI application.
     *
     * That is when running commands.
     *
     * @return void
     */
    protected function bootstrapCli(): void
    {
        try {
            $this->addPlugin('Bake');
        } catch (MissingPluginException $e) {
            // Do not halt if the plugin is missing
        }

        $this->addPlugin('Migrations');
        

        // Load more plugins here
    }

    public function getAuthenticationService(ServerRequestInterface $request) : AuthenticationServiceInterface
    {

        $service = new AuthenticationService();

        if ($request->getParam('prefix') !== 'Api') {
            $service->setConfig([
                'unauthenticatedRedirect' => '/users/login',
                'queryParam' => 'redirect',
            ]);
        }

        $fields = [
            'username' => 'email',
            'password' => 'password'
        ];

        // Load the authenticators, you want session first
        $service->loadAuthenticator('Authentication.Session');
        $service->loadAuthenticator('Authentication.Form', [
            'fields' => $fields,
            //'loginUrl' => array('/users/login', '/api/users/login')
        ]);

        //Testing jwt
        $service->loadAuthenticator('Authentication.Jwt', [
            'returnPayload' => false,
            'secretKey' => Security::getSalt()]);

        // Load identifiers
        $service->loadIdentifier('Authentication.Password', compact('fields'));
        $service->loadIdentifier('Authentication.JwtSubject');

        return $service;
    }

    public function getAuthorizationService(ServerRequestInterface $request): AuthorizationServiceInterface
    {
        $resolver = new OrmResolver();

        return new AuthorizationService($resolver);
    }


}
