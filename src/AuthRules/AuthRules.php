<?php

namespace App\AuthRules;

class AuthRules{

	public function teamRole($user){
        //more roles to be defined
        return $user->getTeam()->org_role;
    }
	
	public function onResource($user, $resource){
        $resourceTenantId = $resource->tenant_id;
        
        if ($resourceTenantId == $user->tenant_id) {
            //check if I'm owner
            if ($this->teamRole($user)) {
                return true;
            }
            
        }        
        return false;
    }
}