<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;
//use Cake\Mailer\Email;
use Mailgun\Mailer\MailgunMailer;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Pheanstalk\Pheanstalk;
use Mailgun\Mailer\MailgunTrait;
use Cake\Mailer\Mailer;


class EmailListener implements EventListenerInterface
{
    use LogTrait;
    use MailgunTrait;
	public function implementedEvents():array
    {

        return [
            'Model.Email.ResetToken' => 'sendEmailResetToken',
            'Model.Checkout.alertTenant' => 'notifyTenant',
            'Model.OrderEntries.online' => 'onlineOrder',
            'Model.Stocks.lowStock' => 'lowStock', 
            'Model.Dashboards.newsLetter' => 'newsLetter',       

        ];
    }

    public function emailHandler($userEntity, $emailFrom){
        
        $email = new MailgunMailer();
        
        $email
            ->setEmailFormat('html')
            ->setFrom([Configure::read('adminEmail') => $emailFrom])

            ->setTo($userEntity->email);
            


        return $email;
    }

   
    public function sendEmailResetToken($event){
        $entity = $event->getData('reset');
        $fooToken = $event->getData('token');
        
        $domain = Router::url('/', true);
        $resetUrl = $domain.'reset-tokens/verify/'.$fooToken;

        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $queryUser = $usersTable->findByEmail($entity->email);
        
        $userData = $queryUser->first();

        $from = ('Supesi');
        $email = $this->emailHandler($userData, $from);
        $email
            ->setViewVars(['url' => $resetUrl , 'userData' => $userData])
            ->setSubject(__('Password recovery'))
            ->viewBuilder()
                ->setTemplate('reset_email');
            
        $email->deliver();

    }
    public function lowStock($event){
        $stock = $event->getData('stock');

        //get
        $tenantsTable = TableRegistry::getTableLocator()->get('Tenants');
        $tenantData = $tenantsTable->get($stock->tenant_id, [
            'contain' => 'Users']
        );

        $productsTable = TableRegistry::getTableLocator()->get('Products');
        $productData = $productsTable->get($stock->product_id);

        
        $from = ('Supesi');
        $email = $this->emailHandler($tenantData->user, $from);
        $email
            ->setViewVars(['userData' => $tenantData->user, 'stockData' => $stock, 'productData' => $productData])
            ->setSubject(__('Low Stock').' '. $productData->title )
            ->viewBuilder()
                ->setTemplate('low_stock');

        $email->deliver();
    }

    public function notifyTenant($event){
        $checkoutEntity = $event->getData('checkout');
        

        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $userData = $usersTable->get($checkoutEntity->product->user_id);

        $tableGuestCart = TableRegistry::getTableLocator()->get('GuestCarts');
        $query = $tableGuestCart->find('all')
            ->contain(['Products'])
            ->where(['session_id' => $checkoutEntity->session_id]);
            

        $from = ('Supesi');
        $email = $this->emailHandler($userData, $from);
        $email
            ->setTo(['muasyaleon@gmail.com',])
            ->setViewVars(['checkoutData' => $checkoutEntity, 'cartItems' => $query])
            ->setSubject(__('You got an order'))

            ->viewBuilder()
                ->setTemplate('tenant_new_order');

        $email->deliver();
    }

    
    public function onlineOrder($event){

       

        $orderEntry = $event->getData('orderEntry');
        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $userData = $usersTable->find()->where(['Users.tenant_id' => $orderEntry->tenant_id])->firstOrFail();
        

        $from = ('Supesi');
        $email = $this->emailHandler($userData, $from);
        
        $email
         ->setBCc(['oneappke@gmail.com'])
            ->setViewVars(['order' => $orderEntry, 'user' => $userData])
            ->setSubject(__('Order received'))
            ->viewBuilder()
                ->setTemplate('online_order_placed');
        
        $debug = Configure::read('debug');

        if (!$debug) {
            # code...
            $email->deliver();
        }else{
            $this->log((string) $orderEntry['id']. ' ' . 'Email sending ok');
        }
        
        
       


    }

    public function newsLetter($event){
        $debug = Configure::read('debug');

        if ($debug) {
            $list = 'test@mg.supesi.com';
        }else{
            $list = 'news@mg.supesi.com';
        }
        $data = $event->getData('data');
        $email = new MailgunMailer();
        $email->setFrom(['admin@supesi.com' => 'Supesi updates'])
        ->setTo($list)
        ->setSubject($data['subject'])
        ->deliver($data['body']);
    }



    
}