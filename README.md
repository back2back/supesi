## New deployment
Install docker in ubuntu droplet.
Clone the repo and composer install --no-dev

## Deployment

Split ansible playbooks into two
Run the 2nd and let it fail, fix the git clone issue and run again
Login to the server generate ssh keys and copy the public key to gitlab repo
Composer install creates app_local.php file on server. Update it with :
	1: Debug should be false
	2: Database and other sensitive data
	3: Possibly delete it completely

Document Root

Cd into sites-available, change to repo folder
Set the correct overrides for apache2.conf

## Installation

1. Check out middleware. Tenants are run through middleware.
2. Default country is Kenya. To investigate how this affects timezone specific actions
3. Subsctiptions is set to a default of 1200/= kes. 
4. Trial period set default is 14 days.